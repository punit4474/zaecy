<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colours extends Model
{
    protected $fillable = [
        'name', 'colour_code', 'description','status','language_id'
    ];

    protected $table = 'colours';

}
