<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageCategory extends Model
{
    protected $fillable = [
        'name', 'description', 'status','language_id','collection_id'
    ];

    protected $table = 'page_categories';

    public function CollectionData()
    {
        return $this->belongsTo('App\Models\Collection', 'collection_id');
    }
}
