<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'discounts';

    protected $fillable = ['name','discount_code','discount_type','discount_value','discount_start_date','discount_end_date','max_use_value'];
}
