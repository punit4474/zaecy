<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'image', 'discription','status','language_id','collection_id'
    ];

    protected $table = 'categories';

    public function CollectionData()
    {
        return $this->belongsTo('App\Models\Collection', 'collection_id');
    }
}
