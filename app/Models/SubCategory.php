<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = [
        'name', 'image', 'discription','status','language_id','category_id','collection_id'
    ];

    protected $table = 'sub_categories';

    public function CategoryData()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function CollectionData()
    {
        return $this->belongsTo('App\Models\Collection', 'collection_id');
    }
}
