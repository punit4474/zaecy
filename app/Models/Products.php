<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'language_id', 'collection_id', 'category_id','subcategory_id','product_id','product_name','brand_name','gender','material_composition','material_type','country_region','model_fit',
        'fabric_finish','fit','style','nack_style','sleeve_style','price','discount_type','discount_value','hashtag','status','parent_id','product_quotation','product_description','similar_products','recommended_products'
    ];

    protected $table = 'products';

    public function CollectionData()
    {
        return $this->belongsTo('App\Models\Collection', 'collection_id');
    }

    public function CategoryData()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function SubcategoryData()
    {
        return $this->belongsTo('App\Models\SubCategory', 'subcategory_id');
    }
    public function FitData()
    {
        return $this->belongsTo('App\Models\Fit', 'fit');
    }
    public function StyleData()
    {
        return $this->belongsTo('App\Models\Style', 'style');
    }
    public function NackStyleData()
    {
        return $this->belongsTo('App\Models\NackStyle', 'nack_style');
    }
    public function SleeveStyleData()
    {
        return $this->belongsTo('App\Models\SleeveType', 'sleeve_style');
    }
}
