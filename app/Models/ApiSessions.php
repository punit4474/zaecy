<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiSessions extends Model
{
    protected $fillable = ['session_id','user_id','user_role','active','login_time'];

    protected $table = 'api_sessions';
}
