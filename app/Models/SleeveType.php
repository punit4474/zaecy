<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SleeveType extends Model
{
    protected $fillable = [
        'name', 'description', 'status','language_id'
    ];

    protected $table = 'sleeve_types';
}
