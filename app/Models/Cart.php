<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'carts';

    protected $fillable = ['user_id','product_id','colour_id','product_sku','variant_sku_id','size','qty'];

    public function ProductData()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }
    public function ColourData()
    {
        return $this->belongsTo('App\Models\ProductColours', 'colour_id');
    }

}
