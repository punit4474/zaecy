<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPageCategory extends Model
{
    protected $table = 'product_page_categories';

    protected $fillable = ['product_id','page_category_id'];
}
