<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'name', 'description', 'url','type','image','status','language_id','category_id'
    ];

    protected $table = 'sliders';

    public function CollectionData()
    {
        return $this->belongsTo('App\Models\Collection', 'category_id');
    }
}
