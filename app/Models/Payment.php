<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = ['order_id','payment_method','payment_amount','payment_id','payment_status','refund_id','payment_details','refund_details'];
}
