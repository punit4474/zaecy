<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    protected $fillable = [
        'product_id','colour_id','size','qty','sku'
    ];

    protected $table = 'product_sizes';
}
