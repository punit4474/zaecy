<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NackStyle extends Model
{
    protected $fillable = [
        'name', 'description', 'status','language_id'
    ];

    protected $table = 'nack_styles';
}
