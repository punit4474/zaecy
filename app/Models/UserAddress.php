<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = [
        'user_id', 'address_type', 'address_line1','address_line2','city','postal_code','default','address_name'
    ];

    protected $table = 'user_addresses';
}
