<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    protected $fillable = ['user_id','order_id','total_amount','discount_code','discount_amount','payment_id','order_status','airway_tracking_number','address_name','address1','address2','city','postal_code','return_id'];

    public function userData()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
