<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fit extends Model
{
    protected $fillable = [
        'name', 'description', 'status','language_id'
    ];

    protected $table = 'fits';
}
