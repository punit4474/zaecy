<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColours extends Model
{
    protected $fillable = [
        'product_id','colour_name','colour_code','colour_image','product_quotation','product_description','model_fit'
    ];

    protected $table = 'product_colours';


}
