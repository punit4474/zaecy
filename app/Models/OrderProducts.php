<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    protected $table = 'order_products';

    protected $fillable = ['order_id','product_id','product_sku','product_name','product_colour','product_size','product_size_sku','qty','price'];

    public function productData()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }
}
