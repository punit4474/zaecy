<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWishList extends Model
{
    protected $table = 'user_wish_lists';

    protected $fillable = ['user_id','product_id','language_id'];
}
