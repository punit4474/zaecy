<?php

namespace App\Helpers;

use App\Models\Orders;
use Auth;
use URL;
use Mail;
use File;
use Carbon\Carbon;
use App\Models\ApiSessions;
use App\Models\Notification;
use App\Models\User;


class BaseFunction
{

    /**
     * Generate Api Token
     * @param $userId
     * @return bool|mixed|string
     */
    public static function setSessionData($userId, $user_role)
    {
        if (empty($userId)) {
            return "User id is empty.";
        } else {
            /*  FIND USER ID IN API SESSION AVAILABE OR NOT  */
            $getApiSessionData = ApiSessions::where('user_id', $userId)->first();
            if ($getApiSessionData) {
                if ($getApiSessionData->delete()) {
                    $apiSession = new ApiSessions();
                    /*  SET SESSION DATA  */
                    $sessionData = [];
                    $sessionData['session_id'] = encrypt(rand(1000, 999999999));
                    $sessionData['user_id'] = $userId;
                    $sessionData['user_role'] = $user_role;
                    $sessionData['login_time'] = \Carbon\Carbon::now();
                    $sessionData['active'] = 1;
                    $apiSession->fill($sessionData);
                    if ($apiSession->save()) {
                        return $apiSession->session_id;
                    } else {
                        return FALSE;
                    }
                } else {
                    return FALSE;
                }
            } else {
                $apiSession = new ApiSessions();
                /*  SET SESSION DATA  */
                $sessionData = [];
                $sessionData['session_id'] = encrypt(rand(1000, 999999999));
                $sessionData['user_id'] = $userId;
                $sessionData['user_role'] = $user_role;
                $sessionData['login_time'] = \Carbon\Carbon::now();
                $sessionData['active'] = 1;
                $apiSession->fill($sessionData);
                if ($apiSession->save()) {
                    return $apiSession->session_id;
                } else {
                    return FALSE;
                }
            }
        }
    }

    /**
     * Check token is valid or not
     * @param $sessionId
     * @return array
     */
    public static function checkApisSession($sessionId)
    {
        $checkSessionExist = ApiSessions::where('session_id', $sessionId)->first();
        if ($checkSessionExist) {
            $sGetUserDataessionData = [];
            $sessionData['id'] = ($checkSessionExist->id) ? $checkSessionExist->id : '';
            $sessionData['session_id'] = ($checkSessionExist->session_id) ? $checkSessionExist->session_id : '';
            $sessionData['user_id'] = ($checkSessionExist->user_id) ? $checkSessionExist->user_id : '';
            $sessionData['user_role'] = ($checkSessionExist->user_role) ? $checkSessionExist->user_role : '';
            $sessionData['active'] = ($checkSessionExist->active) ? $checkSessionExist->active : '';
            $sessionData['login_time'] = ($checkSessionExist->login_time) ? $checkSessionExist->login_time : '';
            return $sessionData;
        } else {
            return array();
        }
    }

    /**
     * Get Random Code
     * @param $limit
     * @return false|string
     */
    public static function random_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }

    public static function findDistace($lat1, $lon1, $lat2, $lon2)
    {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return $miles;
    }

    public static function addNotification($title, $description, $type, $user_id, $sender_id, $event_id, $group_id)
    {

        $data['title'] = $title;
        $data['description'] = $description;
        $data['type'] = $type;
        $data['user_id'] = $user_id;
        $data['sender_id'] = $sender_id;
        $data['event_id'] = $event_id;
        $data['group_id'] = $group_id;

        $notification = new Notification();
        $notification->fill($data);
        if ($notification->save()) {
            return true;
        }
    }

    public static function finalPrice($amount, $type, $value)
    {

        $final_price = $amount;
        if ($type == 'amount') {
            $final_price = $amount - $value;
        } elseif ($type == 'percentage') {
            $percentage = ($amount * $value) / 100;
            $final_price = $amount - $percentage;
        }

        return round($final_price, 0);
    }

    public static function orderNumber()
    {
        $getAppointment = Orders::orderBy('id', 'DESC')->value('id');

        if (empty($getAppointment)) {
            $orderId = 1001;
        } else {
            $orderId = $getAppointment + 1;
        }

        return $orderId.rand(100000,999999);

    }

    public static function createPaymentSession($orderid, $amount)
    {
        $url = "https://rakbankpay.gateway.mastercard.com/form/version/61/merchant/TESTZAECYFZLLCR/session";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{
              "apiOperation": "CREATE_CHECKOUT_SESSION",
              "interaction": {
                  "operation": "PURCHASE"
              },
              "order": {
                  "currency": "USD",
                   "id": "' . $orderid . '" ,
                  "amount": ' . $amount . '
                }
            }';

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = json_decode(curl_exec($curl));
        curl_close($curl);

        return $resp;
    }

    public static function sendSMS($mobile, $otp)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://smartmessaging.etisalat.ae:5676/login/user/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_FAILONERROR => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{"username":"sawan058","password":"Sawan@123#"}',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = json_decode(curl_exec($curl));
        if (curl_errno($curl)) {
            $error_msg = curl_error($curl);
            echo "<pre>";
            print_r($error_msg);
            die;
        }
        curl_close($curl);
//        971501256556
        $token = $response->token;
        $mobile = \BaseFunction::makeMobileNo($mobile);

        $curl = curl_init();
        $value = [
        
                  "msgCategory"=> "4.5",
                  "contentType"=> "3.1",
                  "senderAddr"=> "ZAECY",
                  "dndCategory"=> "Campaign",
                  "priority"=> 1,
                  "dr"=>"1",
                  "clientTxnId"=> 112346965,
                  "recipient"=>"971".$mobile,
                  "msg"=>"User Verification OTP is ".$otp
                ];

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://smartmessaging.etisalat.ae:5676/campaigns/submissions/sms/nb',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FAILONERROR => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($value),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $token
            ),
        ));

        $response1 = curl_exec($curl);

        if (curl_errno($curl)) {
            $error_msg = curl_error($curl);
            echo "<pre>";
            print_r($error_msg);
            die;
        }

        curl_close($curl);

//        print_r($response1);
       return true;

    }

    public static function paymentCheck($orderNo){
        $username = 'TESTZAECYFZLLCR';
        $password = 'c25f6e6fc72175df3528f1cd0cde3541';
        $url = "https://rakbankpay.gateway.mastercard.com/api/rest/version/47/merchant/TESTZAECYFZLLCR/order/" . $orderNo;
        $authBasics = base64_encode("merchant.TESTZAECYFZLLCR:c25f6e6fc72175df3528f1cd0cde3541");;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/json",
            "Authorization: Basic " . $authBasics,
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = json_decode(curl_exec($curl));
        curl_close($curl);
        return $resp;
//        echo "<pre>";
//        print_r($resp);
//        die;
    }

    public static function makeMobileNo($mobile) {
        $finalMobileNo = '';
        if (substr($mobile, 0, 4) === '+971'):
            $makedMobileNo = substr($mobile, 4);
            if (substr($makedMobileNo, 0, 1) != '0'):
                $finalMobileNo = '971' . $makedMobileNo;
            else:
                $finalMobileNo = $makedMobileNo;
            endif;
        elseif (substr($mobile, 0, 1) === '0') :
            $makedMobileNo = substr($mobile, 1);
            if (substr($makedMobileNo, 0, 1) != '0'):
                $finalMobileNo = '971' . $makedMobileNo;
            else:
                $finalMobileNo = $makedMobileNo;
            endif;
        elseif (substr($mobile, 0, 3) === '971'):
            $makedMobileNo = substr($mobile, 3);
            if (substr($makedMobileNo, 0, 1) != '0'):
                $finalMobileNo = '971' . $makedMobileNo;
            else:
                $finalMobileNo = $makedMobileNo;
            endif;
        else:
            $finalMobileNo = '971' . $mobile;
        endif;
        return $finalMobileNo;
    }


}
