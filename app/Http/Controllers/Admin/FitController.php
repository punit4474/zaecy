<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Fit;
use URL;
use Validator;
use Mail;
use File;
use Session;

class FitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Fit::get();

        return view('Admin.Fit.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Fit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();

        $fit = new Fit();
        $fit->fill($data);
        if($fit->save()){
            Session::flash('message','<div class="alert alert-success">Fits added Successfully.!! </div>');
            return redirect('zaecy-admin/fits');
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Fit::where('id',$id)->first();
        return view('Admin.Fit.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        $update = Fit::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">Fit updated Successfully.!! </div>');
            return redirect('zaecy-admin/fits');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Fit::where('id',$id)->delete();

        if($delete){
            Session::flash('message','<div class="alert alert-danger">Fit removed Successfully.!! </div>');
            return redirect('zaecy-admin/fits');
        }
    }

    /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = Fit::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = Fit::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; Fit status updated successfully. </div>');
            return redirect('zaecy-admin/fits');
        }
    }
}
