<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\Payment;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class DashboardController extends Controller
{
    public function index(){
        $totalTransaction = Payment::count();
        $totalUnitSold = OrderProducts::sum('qty');
        $totalRevenue = Payment::where('payment_status','success')->sum('payment_amount');
        $basket = Orders::count();
        $customer = User::leftjoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.role_id', 2)->count();
        $average = 0;
        if($basket != 0){
            $average = $totalUnitSold/$basket;
        }

        $mostRecentSold = Orders::orderBy('id','DESC')->limit(5)->get();
        foreach ($mostRecentSold as $row){
            $row->unit = OrderProducts::where('order_id',$row->id)->sum('qty');
            $row->transaction = Payment::where('order_id',$row->id)->value('payment_id');
        }

        $topProduct = OrderProducts::leftjoin('orders','orders.id','=','order_products.order_id')
            ->select('order_products.*',DB::raw("SUM(order_products.product_id) as product_count"))
            ->where('orders.order_status','delivered')
            ->groupBy('order_products.product_id')->get()->toArray();
        $topProducts = [];
        foreach ($topProduct as $row){
            $getCategory = Products::where('id',$row['product_id'])->value('category_id');
            $row['category'] = Category::where('id',$getCategory)->value('name');
            $topProducts[] = $row;
        }


        array_multisort(array_column($topProducts, 'product_count'), SORT_DESC, $topProducts);
//        dd($topProducts);


        return view('Admin.dashboard',compact('totalTransaction','totalUnitSold','totalRevenue','average','mostRecentSold','topProducts','customer'));
    }
}
