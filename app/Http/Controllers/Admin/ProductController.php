<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PageCategory;
use App\Models\ProductPageCategory;
use Illuminate\Http\Request;
use App\Models\ProductImage;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\ProductColours;
use App\Models\Collection;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Colours;
use App\Models\Fit;
use App\Models\NackStyle;
use App\Models\Style;
use App\Models\SleeveType;
use URL;
use Validator;
use Mail;
use File;
use Session;
use PHPExcel;
use PHPExcel_IOFactory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Products::get();

        foreach ($data as $row) {
            $productImage = ProductImage::where('product_id', $row->id)->first();
            if (!empty($productImage)) {

                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $productImage['image'];
            } else {
                $row->image = '';
            }
            $c = [];
            $colour = ProductColours::where('product_id', $row->id)->select('colour_name')->get();
            foreach ($colour as $cd) {
                $c[] = $cd['colour_name'];
            }

            $row->color = implode(',', $c);
        }


        return view('Admin.Products.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collection = ['' => 'Select Collection'] + Collection::where('status', 'active')->pluck('name', 'id')->all();
        $category = ['' => 'Select Category'];
        $subcategory = ['' => 'Select Sub Category'];
        $fit = ['' => 'Select FIt'] + Fit::where('status', 'active')->pluck('name', 'id')->all();
        $style = ['' => 'Select Style'] + Style::where('status', 'active')->pluck('name', 'id')->all();
        $nackStyle = ['' => 'Select Nack Style'] + NackStyle::where('status', 'active')->pluck('name', 'id')->all();
        $sleeveStyle = ['' => 'Select Sleeve Style'] + SleeveType::where('status', 'active')->pluck('name', 'id')->all();
        $pageCategory = PageCategory::where('status', 'active')->pluck('name', 'id')->all();

        return view('Admin.Products.create', compact('collection', 'category', 'subcategory', 'fit', 'style', 'nackStyle', 'sleeveStyle', 'pageCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'language_id' => 'required',
            'collection_id' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'product_id' => 'required',
            'product_name' => 'required',
            'brand_name' => 'required',
            'price' => 'required',
            // 'product_url' => 'required|unique:products',
        ]);

        $data = $request->all();
        $data = $request->except('product_quotation','product_description');
        // dd($data);
        $data['status'] = $request['status'];
        $newLanguage_id = 2;
        if ($request['language_id'] == 2) {
            $newLanguage_id = 1;
        }

        $checkProductId = Products::where('language_id', $newLanguage_id)->where('product_id', $request['product_id'])->first();
        if (!empty($checkProductId)) {
            $data['parent_id'] = $checkProductId['id'];
        }
        if (!empty($request['similar_products'])) {

            $data['similar_products'] = implode(',', $request['similar_products']);
        }
        if (!empty($request['recommended_products'])) {

            $data['recommended_products'] = implode(',', $request['recommended_products']);
        }

        $product = new Products();
        $product->fill($data);
        // dd($data);  

        if ($product->save()) {
            $colour_name = $request['colour_name'];
            // dd($colour_name);
            $colour_image = $request['colour_image'];
            $product_quotation = $request['product_quotation'];
            $product_description = $request['product_description'];
            $page_category = $request['page_category'];
            $product_image = $request['product_image'];
            $product_video = $request['product_video'];
            $model_fit = $request['model_fit'];
            $xs = $request['xs'];
            $s = $request['s'];
            $m = $request['m'];
            $l = $request['l'];
            $xl = $request['xl'];
            $xxl = $request['xxl'];
        
         

            for ($i = 0; $i < count($colour_name); $i++) {
                $cProduct['colour_name'] = $colour_name[$i];
                $cProduct['product_quotations'] = $product_quotation[$i];
                $cProduct['product_descriptions'] = $product_description[$i];
                $cProduct['model_fit'] = $model_fit[$i];
                // dd($cProduct);
                if (!empty($colour_image[$i])) {
                    $extension = $colour_image[$i]->getClientOriginalExtension();

                    $destinationpath = 'storage/app/public/product';

                    $filename = 'product-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                    $colour_image[$i]->move($destinationpath, $filename);
                    $cProduct['colour_image'] = $filename;
                }

                $cProduct['product_id'] = $product->id;

                $addColour = new ProductColours();
                $addColour->fill($cProduct);
                $addColour->save();
                if ($addColour->save()) {

                    if (!empty($product_image[$i])) {
                        foreach ($product_image[$i] as $item) {
                            if (!empty($item)) {
                                $extension = $item->getClientOriginalExtension();

                                $destinationpath = 'storage/app/public/product';

                                $filename = 'product-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                                $item->move($destinationpath, $filename);

                                $barImage['image'] = $filename;
                                $barImage['type'] = 'image';
                                $barImage['product_id'] = $product->id;
                                $barImage['colour_id'] = $addColour->id;

                                $product_img = new ProductImage();
                                $product_img->fill($barImage);
                                $product_img->save();
                            }

                        }
                    }

                    $cSize = ['xs', 's', 'm', 'l', 'xl', 'xxl'];

                    foreach ($cSize as $j) {
                        $sProduct['product_id'] = $product->id;
                        $sProduct['colour_id'] = $addColour->id;
                        $sProduct['size'] = $j;

                        if ($j == 'xs') {
                            $sProduct['qty'] = $xs[$i];
                        } elseif ($j == 's') {
                            $sProduct['qty'] = $s[$i];
                        } elseif ($j == 'm') {
                            $sProduct['qty'] = $m[$i];
                        } elseif ($j == 'l') {
                            $sProduct['qty'] = $l[$i];
                        } elseif ($j == 'xl') {
                            $sProduct['qty'] = $xl[$i];
                        } elseif ($j == 'xxl') {
                            $sProduct['qty'] = $xxl[$i];
                        }
                        $sProduct['sku'] = str_replace(' ', '-', strtolower($product['product_id'])) . '-' . str_replace(' ', '-', strtolower($colour_name[$i])) . '-' . $j;
                        $sizeValue = new ProductSize();
                        $sizeValue->fill($sProduct);
                        $sizeValue->save();
                    }

                }

            }

            if (!empty($page_category) && $request['page_category']) {
                foreach ($page_category as $v) {
                    $pageCategory['product_id'] = $product->id;
                    $pageCategory['page_category_id'] = $v;

                    $upC = new ProductPageCategory();
                    $upC->fill($pageCategory);
                    $upC->save();
                }

            }

            if (!empty($product_video)) {
                $extension = $product_video->getClientOriginalExtension();

                $destinationpath = 'storage/app/public/product/video';

                $filename = 'product-video-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                $product_video->move($destinationpath, $filename);

                $vProduct['type'] = 'video';
                $vProduct['image'] = $filename;
                $vProduct['product_id'] = $product->id;
                $videoPr = new ProductImage();
                $videoPr->fill($vProduct);
                $videoPr->save();
            }


            Session::flash('message', '<div class="alert alert-success">Product added Successfully.!! </div>');
            return redirect('zaecy-admin/products');
        }



    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Products::where('id', $id)->first();
        $collection = ['' => 'Select Collection'] + Collection::where('language_id', $data['language_id'])->where('status', 'active')->pluck('name', 'id')->all();
        $category = ['' => 'Select Category'] + Category::where('language_id', $data['language_id'])
                ->where('collection_id', $data['collection_id'])
                ->where('status', 'active')->pluck('name', 'id')->all();
        $subcategory = ['' => 'Select Sub Category'] + SubCategory::where('language_id', $data['language_id'])
                ->where('category_id', $data['category_id'])
                ->where('status', 'active')
                ->pluck('name', 'id')->all();

        $fit = ['' => 'Select FIt'] + Fit::where('language_id', $data['language_id'])->where('status', 'active')->pluck('name', 'id')->all();
        $style = ['' => 'Select Style'] + Style::where('language_id', $data['language_id'])->where('status', 'active')->pluck('name', 'id')->all();
        $nackStyle = ['' => 'Select Nack Style'] + NackStyle::where('language_id', $data['language_id'])->where('status', 'active')->pluck('name', 'id')->all();
        $sleeveStyle = ['' => 'Select Sleeve Style'] + SleeveType::where('language_id', $data['language_id'])->where('status', 'active')->pluck('name', 'id')->all();
        $pageCategory = PageCategory::where('language_id', $data['language_id'])->where('status', 'active')->pluck('name', 'id')->all();
        $selectedPageCategory = ProductPageCategory::where('product_id', $data['id'])->pluck('page_category_id')->all();
        $simillarProduct = Products::where('language_id', $data['language_id'])->where('status', 'active')->pluck('product_id', 'id')->all();

        $productColour = ProductColours::where('product_id', $data['id'])->get();
        foreach ($productColour as $row) {
            $row->product_images = ProductImage::where('product_id', $row->product_id)->where('colour_id', $row->id)->get();
            $row->product_size = ProductSize::where('product_id', $row->product_id)->where('colour_id', $row->id)->get();
        }
        $data['video'] = ProductImage::where('product_id', $data['id'])->where('type', 'video')->value('image');

        return view('Admin.Products.edit', compact('collection', 'category', 'subcategory', 'fit', 'style', 'nackStyle', 'sleeveStyle', 'pageCategory', 'data', 'selectedPageCategory', 'simillarProduct', 'productColour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'language_id' => 'required',
            'collection_id' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'product_id' => 'required',
            'product_name' => 'required',
            'brand_name' => 'required',
            'price' => 'required',
        ]);

        $data = $request->all();
        $data = $request->except('_token', '_method', 'product_id', 'colour_name', 'xs', 's', 'm', 'l', 'xl', 'xxl', 'pro_id', 'page_category','product_image','colour_image','product_video','product_quotation','product_description');

        $product_id = $request['product_id'];

        if (!empty($request['similar_products'])) {

            $data['similar_products'] = implode(',', $request['similar_products']);
        }
        if (!empty($request['recommended_products'])) {

            $data['recommended_products'] = implode(',', $request['recommended_products']);
        }

        $updateProduct = Products::where('id', $id)->update($data);
        if ($updateProduct) {
            $product_id = $request['product_id'];
            $colour_name = $request['colour_name'];
            $colour_image = $request['colour_image'];
            $page_category = $request['page_category'];
            $product_image = $request['product_image'];
            $product_video = $request['product_video'];
            $product_quotation = $request['product_quotation'];
            $product_description = $request['product_description'];
             $model_fit = $request['model_fit'];
            $xs = $request['xs'];
            $s = $request['s'];
            $m = $request['m'];
            $l = $request['l'];
            $xl = $request['xl'];
            $xxl = $request['xxl'];

            for ($j = 0; $j < count($product_id); $j++) {
                $cProduct['colour_name'] = $colour_name[$j];
                $cProduct['product_quotation'] = $product_quotation[$j];
                $cProduct['product_description'] = $product_description[$j];
                $cProduct['model_fit'] = $model_fit[$j];
                if (!empty($colour_image[$j])) {
                    $oldimage = ProductColours::where('product_id', $id)->where('id', $product_id[$j])->value('colour_image');

                    if (!empty($oldimage)) {

                        File::delete('storage/app/public/colour/' . $oldimage);
                    }

                    $extension = $colour_image[$j]->getClientOriginalExtension();

                    $destinationpath = 'storage/app/public/colour';

                    $filename = 'product-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                    $colour_image[$j]->move($destinationpath, $filename);
                    $cProduct['colour_image'] = $filename;
                }
                $cProduct['product_id'] = $id;
                $updateColour = ProductColours::where('id', $product_id[$j])->update($cProduct);
                if ($updateColour) {
                    $product = Products::where('id', $id)->first();
                    if (!empty($product_image[$j])) {
                        foreach ($product_image[$j] as $item) {
                            if (!empty($item)) {
                                $extension = $item->getClientOriginalExtension();

                                $destinationpath = 'storage/app/public/product';

                                $filename = 'product-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                                $item->move($destinationpath, $filename);

                                $barImage['image'] = $filename;
                                $barImage['type'] = 'image';
                                $barImage['product_id'] = $id;
                                $barImage['colour_id'] = $product_id[$j];

                                $product_img = new ProductImage();
                                $product_img->fill($barImage);
                                $product_img->save();
                            }

                        }
                    }

                    $cSize = ['xs', 's', 'm', 'l', 'xl', 'xxl'];

                    foreach ($cSize as $p) {
                        $sProduct['product_id'] = $id;
                        $sProduct['colour_id'] = $product_id[$j];
                        $sProduct['size'] = $p;

                        if ($p == 'xs') {
                            $sProduct['qty'] = $xs[$j];
                        } elseif ($p == 's') {
                            $sProduct['qty'] = $s[$j];
                        } elseif ($p == 'm') {
                            $sProduct['qty'] = $m[$j];
                        } elseif ($p == 'l') {
                            $sProduct['qty'] = $l[$j];
                        } elseif ($p == 'xl') {
                            $sProduct['qty'] = $xl[$j];
                        } elseif ($p == 'xxl') {
                            $sProduct['qty'] = $xxl[$j];
                        }

                        $sProduct['sku'] = str_replace(' ', '-', strtolower($product['product_id'])) . '-' . str_replace(' ', '-', strtolower($colour_name[$j])) . '-' . $p;
                        $sizeValue = ProductSize::where('product_id', $id)->where('colour_id', $product_id[$j])->where('size', $p)->update($sProduct);

                    }
                }
            }

            for ($i = count($product_id); $i < count($colour_name); $i++) {


                $cProduct['colour_name'] = $colour_name[$i];
                $cProduct['product_quotation'] = $product_quotation[$i];
                $cProduct['product_description'] = $product_description[$i];
                $cProduct['model_fit'] = $model_fit[$i];
                
                if (!empty($colour_image[$i])) {
                    $extension = $colour_image[$i]->getClientOriginalExtension();

                    $destinationpath = storage_path('app/public/colour');

                    $filename = 'product-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                    $colour_image[$i]->move($destinationpath, $filename);
                    $cProduct['colour_image'] = $filename;
                }

                $cProduct['product_id'] = $product->id;
                $addColour = new ProductColours();
                $addColour->fill($cProduct);
                if ($addColour->save()) {

                    if (!empty($product_image[$i])) {
                        foreach ($product_image[$i] as $item) {
                            if (!empty($item)) {
                                $extension = $item->getClientOriginalExtension();

                                $destinationpath = storage_path('app/public/product');

                                $filename = 'product-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                                $item->move($destinationpath, $filename);

                                $barImage['image'] = $filename;
                                $barImage['type'] = 'image';
                                $barImage['product_id'] = $product->id;
                                $barImage['colour_id'] = $addColour->id;

                                $product_img = new ProductImage();
                                $product_img->fill($barImage);
                                $product_img->save();
                            }

                        }
                    }

                    $cSize = ['xs', 's', 'm', 'l', 'xl', 'xxl'];

                    foreach ($cSize as $j) {
                        $sProduct['product_id'] = $product->id;
                        $sProduct['colour_id'] = $addColour->id;
                        $sProduct['size'] = $j;

                        if ($j == 'xs') {
                            $sProduct['qty'] = $xs[$i];
                        } elseif ($j == 's') {
                            $sProduct['qty'] = $s[$i];
                        } elseif ($j == 'm') {
                            $sProduct['qty'] = $m[$i];
                        } elseif ($j == 'l') {
                            $sProduct['qty'] = $l[$i];
                        } elseif ($j == 'xl') {
                            $sProduct['qty'] = $xl[$i];
                        } elseif ($j == 'xxl') {
                            $sProduct['qty'] = $xxl[$i];
                        }
                        $sProduct['sku'] = str_replace(' ', '-', strtolower($product['product_id'])) . '-' . str_replace(' ', '-', strtolower($colour_name[$i])) . '-' . $j;
                        $sizeValue = new ProductSize();
                        $sizeValue->fill($sProduct);
                        $sizeValue->save();
                    }

                }

            }

            if (!empty($page_category) && $request['page_category']) {
                $remove = ProductPageCategory::where('product_id', $id)->delete();
                foreach ($page_category as $v) {
                    $pageCategory['product_id'] = $product->id;
                    $pageCategory['page_category_id'] = $v;

                    $upC = new ProductPageCategory();
                    $upC->fill($pageCategory);
                    $upC->save();
                }

            }

            if (!empty($product_video)) {

                $oldimage = ProductImage::where('product_id', $id)->where('type', 'video')->value('image');

                if (!empty($oldimage)) {

                    File::delete('storage/app/public/product/video/' . $oldimage);
                }

                $extension = $product_video->getClientOriginalExtension();

                $destinationpath = 'storage/app/public/product/video';

                $filename = 'product-video-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                $product_video->move($destinationpath, $filename);

                $vProduct['type'] = 'video';
                $vProduct['image'] = $filename;
                $vProduct['product_id'] = $product->id;
                $videoPr = new ProductImage();
                $videoPr->fill($vProduct);
                $videoPr->save();
            }

            Session::flash('message', '<div class="alert alert-success">Product Updated Successfully.!! </div>');
            return redirect('zaecy-admin/products');

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $size = ProductSize::where('product_id',$id)->delete();
        $colour = ProductColours::where('product_id',$id)->delete();
        $image = ProductImage::where('product_id',$id)->delete();
        $product = Products::where('id',$id)->delete();
        if($product){
            Session::flash('message', '<div class="alert alert-danger">Product Removed Successfully.!! </div>');
            return redirect('zaecy-admin/products');
        }
    }

    public function getCategory(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'collection_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Category::where('language_id', $request['language_id'])->where('collection_id', $request['collection_id'])->where('status', 'active')->get();

        foreach ($data as $row) {
            $row['image'] = URL::to(config('app.asset_directory') . 'storage/app/public/category/' . $row['image']);
        }

        return $data = ['status' => 'success', 'data' => $data];
    }

    public function getSubcategory(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'category_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = SubCategory::where('language_id', $request['language_id'])->where('category_id', $request['category_id'])->where('status', 'active')->get();

        foreach ($data as $row) {
            $row['image'] = URL::to(config('app.asset_directory') . 'storage/app/public/subcategory/' . $row['image']);
        }

        return $data = ['status' => 'success', 'data' => $data];
    }

    public function getCollection(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Collection::where('language_id', $request['language_id'])->where('status', 'active')->get();

        foreach ($data as $row) {
            $row['image'] = URL::to(config('app.asset_directory') . 'storage/app/public/collection/' . $row['image']);
        }

        return $data = ['status' => 'success', 'data' => $data];

    }

    public function getFit(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Fit::where('language_id', $request['language_id'])->where('status', 'active')->get();

        return $data = ['status' => 'success', 'data' => $data];
    }

    public function getStyle(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Style::where('language_id', $request['language_id'])->where('status', 'active')->get();

        return $data = ['status' => 'success', 'data' => $data];
    }

    public function getNackStyle(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = NackStyle::where('language_id', $request['language_id'])->where('status', 'active')->get();

        return $data = ['status' => 'success', 'data' => $data];
    }

    public function getSleeveStyle(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = SleeveType::where('language_id', $request['language_id'])->where('status', 'active')->get();

        return $data = ['status' => 'success', 'data' => $data];
    }

    public function getProduct(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('language_id', $request['language_id'])->where('status', 'active')->get();
        return $data = ['status' => 'success', 'data' => $data];
    }

    public function removeProduct(Request $request)
    {
        $rule = [
            'product_id' => 'required',
            'colour_id' => 'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        $getImages = ProductImage::where('product_id', $request['product_id'])->where('colour_id', $request['colour_id'])->get();
        foreach ($getImages as $row) {
            if (!empty($row->image)) {

                File::delete('storage/app/public/product/' . $row->image);
            }

        }
        $removeImage = ProductImage::where('product_id', $request['product_id'])->where('colour_id', $request['colour_id'])->delete();

        $oldimage = ProductColours::where('product_id', $request['product_id'])->where('id', $request['colour_id'])->value('colour_image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/colour/' . $oldimage);
        }
        $removeSize = ProductSize::where('product_id', $request['product_id'])->where('colour_id', $request['colour_id'])->delete();

        $removeData = ProductColours::where('product_id', $request['product_id'])->where('id', $request['colour_id'])->delete();

        if ($removeData) {
            return $data = ['status' => 'success', 'data' => []];
        } else {
            return $data = ['status' => 'error', 'data' => []];
        }

    }


    public function import()
    {
        return view('Admin.Products.import');
    }


    public function importData(Request $request)
    {
        $data = request()->file('image');

        $csv = array();
        $excelReader = PHPExcel_IOFactory::createReaderForFile($data);
        $excelReader->setReadDataOnly();
        $loadSheets = array('Sheet1');
        $excelReader->setLoadSheetsOnly($loadSheets);
        $excelObj = $excelReader->load($data);

        $excelObj->getActiveSheet()->toArray(null, true, true, true);

        $worksheetNames = $excelObj->getSheetNames($data);
        $return = array();

        foreach ($worksheetNames as $key => $sheetName) {
            $excelObj->setActiveSheetIndexByName($sheetName);
            $return[$sheetName] = $excelObj->getActiveSheet()->toArray(null, true, true, true);
        }

        $data = $return['Worksheet'];
        $i = 1;
        $productSku = '';
        $language = '';

        foreach ($data as $j => $row) {
            if ($j != $i) {

                $productData['language_id'] = $row['A'];
                $productData['collection_id'] = $row['B'];
                $productData['category_id'] = $row['C'];
                $productData['subcategory_id'] = $row['D'];
                $productData['product_id'] = $row['E'];
                $productData['product_name'] = $row['F'];
                $productData['brand_name'] = $row['G'];
                $productData['gender'] = $row['H'];
                $productData['material_composition'] = $row['I'];
                $productData['material_type'] = $row['J'];
                $productData['country_region'] = $row['K'];
                $productData['model_fit'] = $row['L'];
                $productData['weight'] = $row['M'];
                $productData['fit'] = $row['N'];
                $productData['style'] = $row['O'];
                $productData['nack_style'] = $row['P'];
                $productData['sleeve_style'] = $row['Q'];
                $productData['price'] = $row['R'];
                $productData['discount_type'] = $row['S'];
                $productData['discount_value'] = $row['T'];
                $productData['hashtag'] = $row['U'];
                $productData['status'] = 'active';
                $productData['product_quotation'] = $row['V'];
                $productData['product_description'] = $row['W'];
                if (!empty($row['X'])) {
                    $getId = Products::where('product_id', $row['X'])->value('id');
                    $productData['parent_id'] = $getId;
                }


                if ($productSku != $row['E']) {
                    $productSku = $row['E'];
                    $product = new Products();
                    $product->fill($productData);
                    $product->save();
                    $id = $product->id;
                } else {
                    $id = $product->id;
                }

                $colourData['product_id'] = $id;
                $colourData['colour_name'] = $row['Y'];
                $colourData['colour_image'] = $row['Z'];
                $colourData['product_quotation'] = $row['V'];
                $colourData['product_description'] = $row['W'];
                $colourData['model_fit'] = $row['L'];
                $storeColour = new ProductColours();
                $storeColour->fill($colourData);
                $storeColour->save();

                if (!empty($row['AA'])) {
                    $images = explode(',', $row['AA']);
                    foreach ($images as $item) {
                        $imageData['product_id'] = $id;
                        $imageData['image'] = $item;
                        $imageData['type'] = 'image';
                        $imageData['colour_id'] = $storeColour->id;
                        $simage = new ProductImage();
                        $simage->fill($imageData);
                        $simage->save();
                    }
                }


                $xsData['product_id'] = $id;
                $xsData['colour_id'] = $storeColour->id;
                $xsData['size'] = 'xs';
                $xsData['sku'] = str_replace(' ', '-', strtolower($row['E'])) . '-' . str_replace(' ', '-', strtolower($row['Y'])) . '-xs';
                $xsData['qty'] = $row['AB'];
                $xs = new ProductSize();
                $xs->fill($xsData);
                $xs->save();

                $sData['product_id'] = $id;
                $sData['colour_id'] = $storeColour->id;
                $sData['size'] = 's';
                $sData['sku'] = str_replace(' ', '-', strtolower($row['E'])) . '-' . str_replace(' ', '-', strtolower($row['Y'])) . '-s';
                $sData['qty'] = $row['AC'];
                $s = new ProductSize();
                $s->fill($sData);
                $s->save();

                $mData['product_id'] = $id;
                $mData['colour_id'] = $storeColour->id;
                $mData['size'] = 'm';
                $mData['sku'] = str_replace(' ', '-', strtolower($row['E'])) . '-' . str_replace(' ', '-', strtolower($row['Y'])) . '-m';
                $mData['qty'] = $row['AD'];
                $m = new ProductSize();
                $m->fill($mData);
                $m->save();

                $lData['product_id'] = $id;
                $lData['colour_id'] = $storeColour->id;
                $lData['size'] = 'l';
                $lData['sku'] = str_replace(' ', '-', strtolower($row['E'])) . '-' . str_replace(' ', '-', strtolower($row['Y'])) . '-l';
                $lData['qty'] = $row['AE'];
                $l = new ProductSize();
                $l->fill($lData);
                $l->save();

                $xlData['product_id'] = $id;
                $xlData['colour_id'] = $storeColour->id;
                $xlData['size'] = 'xl';
                $xlData['sku'] = str_replace(' ', '-', strtolower($row['E'])) . '-' . str_replace(' ', '-', strtolower($row['Y'])) . '-xl';
                $xlData['qty'] = $row['AF'];
                $xl = new ProductSize();
                $xl->fill($xlData);
                $xl->save();

                $xxlData['product_id'] = $id;
                $xxlData['colour_id'] = $storeColour->id;
                $xxlData['size'] = 'xxl';
                $xxlData['sku'] = str_replace(' ', '-', strtolower($row['E'])) . '-' . str_replace(' ', '-', strtolower($row['Y'])) . '-xxl';
                $xxlData['qty'] = $row['AG'];
                $xxl = new ProductSize();
                $xxl->fill($xxlData);
                $xxl->save();

                if (!empty($row['AH'])) {
                    $videoData['product_id'] = $id;
                    $videoData['image'] = $row['AH'];
                    $videoData['type'] = 'video';
                    $videoData['colour_id'] = null;
                    $vimage = new ProductImage();
                    $vimage->fill($videoData);
                    $vimage->save();
                }

            }

        }

        return redirect('zaecy-admin/products');

    }

    public function removeProductImage($id,$image_id){
        $oldimage = ProductImage::where('product_id', $id)->where('id', $image_id)->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/product/' . $oldimage);
        }

        $remove = ProductImage::where('product_id', $id)->where('id', $image_id)->delete();
        if($remove){
            return redirect('zaecy-admin/products/'.$id.'/edit');
        }
    }

    public function removeMultipleProducts(Request $request)
    {
        
        foreach ($request->product_id as $key) {
            $getImages = ProductImage::where('product_id', $request['product_id'])->where('colour_id', $request['colour_id'])->get();

            foreach ($getImages as $row) {
                if (!empty($row->image)) {

                    File::delete('storage/app/public/product/' . $row->image);
                }

            }
            
            $removeImage = ProductImage::where('product_id', $request['product_id'])->where('colour_id', $request['colour_id'])->delete();

            $oldimage = ProductColours::where('product_id', $request['product_id'])->where('id', $request['colour_id'])->value('colour_image');

            if (!empty($oldimage)) {

                File::delete('storage/app/public/colour/' . $oldimage);
            }
            $removeSize = ProductSize::where('product_id', $request['product_id'])->where('colour_id', $request['colour_id'])->delete();

            $removeData = ProductColours::where('product_id', $request['product_id'])->where('id', $request['colour_id'])->delete();
        }

        $product = Products::whereIn('id',$request->product_id)->delete();

        if ($product) {
            return $data = ['status' => 'success', 'data' => []];
        } else {
            return $data = ['status' => 'error', 'data' => []];
        }

    }
}
