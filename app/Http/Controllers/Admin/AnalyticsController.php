<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrderProducts;
use App\Models\Orders;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    public function index(Request $request)
    {
        $type = 'day';
        if (isset($request['start']) && isset($request['last'])) {
            if (\Carbon\Carbon::parse($request['start'])->isSameDay(\Carbon\Carbon::parse($request['last']))) {
                $type = 'hours';
            }
        }

        if (isset($request['start'])) {
            $lastmonth = \Carbon\Carbon::parse($request['start'])->format('Y-m-d');
            $start = \Carbon\Carbon::parse($request['start'])->format('F d, Y');
        } else {
            $lastmonth = \Carbon\Carbon::now()->subMonth(1)->format('Y-m-d');
            $start = \Carbon\Carbon::parse($lastmonth)->format('F d, Y');
        }

        if (isset($request['last'])) {
            $today = \Carbon\Carbon::parse($request['last'])->addDay(1)->format('Y-m-d');
            $end = \Carbon\Carbon::parse($request['last'])->format('F d, Y');
        } else {
            $today = \Carbon\Carbon::now()->addDay(1)->format('Y-m-d');
            $end = \Carbon\Carbon::now()->format('F d, Y');
        }


        /**
         * Total Order Sell Data Start
         */
        $orders = Orders::where('order_status', '!=', 'cancel')->get();
        $totalSale = [];
        $ord = [];
        foreach ($orders as $row) {
            if (\Carbon\Carbon::now()->between($lastmonth, $today)) {
                if ($type == 'day') {
                    $row->date = \Carbon\Carbon::parse($row->created_at)->format('Y-m-d');
                } elseif ($type == 'hours') {
                    $row->date = \Carbon\Carbon::parse($row->created_at)->format('h a');
                }
                $row->unit = OrderProducts::where('order_id', $row->id)->sum('qty');
                $ord[$row->date][] = $row;
            }
        }


        foreach ($ord as $key => $value) {
            if ($type == 'day') {
                $totalSale[] = array('date' => \Carbon\Carbon::parse($key)->format('d M'), 'total' => array_sum(array_column($value, 'total_amount')));
            } elseif ($type == 'hours') {
                $totalSale[] = array('date' => \Carbon\Carbon::parse($key)->format('h:i a'), 'total' => array_sum(array_column($value, 'total_amount')));
            }
        }

        array_multisort(array_column($totalSale, 'date'), SORT_ASC, $totalSale);
        $totalAmount = array_sum(array_column($totalSale, 'total'));

        /**
         * Total Order Sell Data End
         */
        /**
         * Total Orders Count Start
         */
        $totalOrder = [];
        foreach ($ord as $key => $value) {
            if ($type == 'day') {
                $totalOrder[] = array('date' => \Carbon\Carbon::parse($key)->format('d M'), 'total' => count($value));
            } elseif ($type == 'hours') {
                $totalOrder[] = array('date' => \Carbon\Carbon::parse($key)->format('h:i a'), 'total' => count($value));
            }
        }
        array_multisort(array_column($totalOrder, 'date'), SORT_ASC, $totalOrder);
        $totalOrderValue = array_sum(array_column($totalOrder, 'total'));
        /**
         * Total Orders Count End
         */

        /**
         * Total Average Sale Start
         */
        $totalAverageSale = [];
        foreach ($ord as $key => $value) {
            if ($type == 'day') {
                $totalAverageSale[] = array('date' => \Carbon\Carbon::parse($key)->format('d M'), 'total' => number_format(array_sum(array_column($value, 'total_amount')) / count($value), 0));
            } elseif ($type == 'hours') {
                $totalAverageSale[] = array('date' => \Carbon\Carbon::parse($key)->format('h:i a'), 'total' => number_format(array_sum(array_column($value, 'total_amount')) / count($value), 0));
            }
        }
        array_multisort(array_column($totalAverageSale, 'date'), SORT_ASC, $totalAverageSale);
        $totalAverageSaleValue = array_sum(array_column($totalAverageSale, 'total'));
        /**
         * Total Average Sale End
         */

        /**
         * Average Basket Size Start
         */
        $averageBasket = [];
        foreach ($ord as $key => $value) {
            if ($type == 'day') {
            $averageBasket[] = array('date' => \Carbon\Carbon::parse($key)->format('d M'), 'total' => number_format(array_sum(array_column($value, 'unit')) / count($value), 1));
            } elseif ($type == 'hours') {
                $averageBasket[] = array('date' => \Carbon\Carbon::parse($key)->format('h:i a'), 'total' => number_format(array_sum(array_column($value, 'unit')) / count($value), 1));
            }
        }
        array_multisort(array_column($averageBasket, 'date'), SORT_ASC, $averageBasket);
        $totalAverageBasketValue = array_sum(array_column($averageBasket, 'total'));
        /**
         * Average Basket Size End
         */


        return view('Admin.Analytics.index', compact('start', 'end', 'totalSale', 'totalOrder', 'totalAverageSale', 'averageBasket', 'totalAmount', 'totalOrderValue', 'totalAverageSaleValue', 'totalAverageBasketValue'));
    }
}
