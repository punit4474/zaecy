<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Collection;
use URL;
use Validator;
use Mail;
use File;
use Session;


class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Collection::get();

        return view('Admin.Collections.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Collections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'language_id'=>'required'
        ]);

        $data = $request->all();
        if ($request->file('image')) {

            $file = $request->file('image');
            $filename = 'collection-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/collection', $filename);
            $data['image'] = $filename;
        }

        $collection = new Collection();
        $collection->fill($data);
        if($collection->save()){
            Session::flash('message','<div class="alert alert-success">Collection Added Successfully.!! </div>');
            return redirect('zaecy-admin/collections');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Collection::where('id',$id)->first();
        return view('Admin.Collections.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'language_id'=>'required'
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        if ($request->file('image')) {

            $oldimage = Collection::where('id', $id)->value('image');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/collection/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'collection-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/collection', $filename);
            $data['image'] = $filename;
        }
        $collection = Collection::where('id',$id)->update($data);

        if($collection){
            Session::flash('message','<div class="alert alert-success">Collection Updated Successfully.!! </div>');
            return redirect('zaecy-admin/collections');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldimage = Collection::where('id', $id)->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/collection/' . $oldimage);
        }
        $data = Collection::where('id',$id)->delete();

        if($data){
            Session::flash('message','<div class="alert alert-danger">Collection Removed Successfully.!! </div>');
            return redirect('zaecy-admin/collections');
        }
    }

    /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = Collection::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = Collection::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; Collection status updated successfully. </div>');
            return redirect('zaecy-admin/collections');
        }
    }
}
