<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Orders;
use Illuminate\Http\Request;
use URL;
use Validator;
use Mail;
use File;
use Session;


class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =Discount::get();
        foreach ($data as $row){
            $row->used  = Orders::where('discount_code',$row->discount_code)->where('order_status','!=','cancel')->count();
        }
        $totalOrder = Orders::count();
        $totalRevenue = Orders::where('order_status','delivered')->sum('total_amount');
        $totalFulfilled = Orders::where('order_status','delivered')->count();
        $totalUnFulfilled = Orders::whereIn('order_status',['packing','confirm'])->count();
        return view('Admin.Discount.index',compact('data','totalOrder','totalRevenue','totalUnFulfilled','totalFulfilled'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Discount.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name'=>'required',
           'discount_code'=>'required',
           'discount_type'=>'required',
           'discount_value'=>'required',
           'discount_start_date'=>'required',
           'discount_end_date'=>'required',
           'max_use_value'=>'required|numeric',
        ]);

        $data = $request->all();

        $discount = new Discount();
        $discount->fill($data);
        if($discount->save()){
            Session::flash('message','<div class="alert alert-success">Discount added Successfully.!! </div>');
            return redirect('zaecy-admin/discount');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Discount::where('id',$id)->first();
        return view('Admin.Discount.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'discount_code'=>'required',
            'discount_type'=>'required',
            'discount_value'=>'required',
            'discount_start_date'=>'required',
            'discount_end_date'=>'required',
            'max_use_value'=>'required|numeric',
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        $update = Discount::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">Discount updated Successfully.!! </div>');
            return redirect('zaecy-admin/discount');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Discount::where('id',$id)->delete();

        if($delete){
            Session::flash('message','<div class="alert alert-danger">Discount removed Successfully.!! </div>');
            return redirect('zaecy-admin/discount');
        }
    }

}
