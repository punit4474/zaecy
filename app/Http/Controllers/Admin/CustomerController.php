<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\Role;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use URL;
use Validator;
use Mail;
use File;
use Session;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $type = $request['filter'];

        if (!empty($type)) {
            $datas = User::leftjoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.role_id', 2)->get()->toArray();
            $data = [];
            foreach ($datas as $row) {
                $row['orders'] = Orders::where('user_id', $row['id'])->count();
                $row['revenue'] = Orders::where('user_id', $row['id'])->where('order_status', 'delivered')->sum('total_amount');
                $row['date'] = Orders::where('user_id', $row['id'])->where('order_status', 'delivered')->orderBy('created_at', 'DESC')->value('created_at');
                $row['order_date'] = Orders::where('user_id', $row['id'])->whereNotIn('order_status', ['cancel', 'return'])->orderBy('updated_at', 'DESC')->value('updated_at');
                $row['user_date'] = \Carbon\Carbon::parse($row['created_at'])->format('Y-m-d');
                $row['product_qty'] = Orders::leftjoin('order_products','order_products.order_id','=','orders.id')->where('orders.user_id', $row['id'])->sum('order_products.qty');
                $row['address'] = UserAddress::where('user_id',$row['id'])->where('default','yes')->first();
                $data[] = $row;
            }

            $distanceVal = [];
            if ($type == 'last_update_new') {

                array_multisort(array_column($data, 'order_date'), SORT_DESC, $data);
            }

            if ($type == 'last_update_old') {
                array_multisort(array_column($data, 'order_date'), SORT_ASC, $data);
            }

            if ($type == 'high') {
                array_multisort(array_column($data, 'revenue'), SORT_DESC, $data);
            }

            if ($type == 'low') {
                array_multisort(array_column($data, 'revenue'), SORT_ASC, $data);
            }

            if ($type == 'order_high') {
                array_multisort(array_column($data, 'orders'), SORT_DESC, $data);
            }

            if ($type == 'order_low') {
                array_multisort(array_column($data, 'orders'), SORT_ASC, $data);
            }

            if ($type == 'last_order_new') {
                array_multisort(array_column($data, 'date'), SORT_DESC, $data);
            }

            if ($type == 'last_order_old') {
                array_multisort(array_column($data, 'date'), SORT_ASC, $data);
            }

            if ($type == 'date_new_customer') {
                array_multisort(array_column($data, 'user_date'), SORT_DESC, $data);
            }

            if ($type == 'date_old_customer') {

                array_multisort(array_column($data, 'user_date'), SORT_ASC, $data);
            }
        } else {
            $datas = User::leftjoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.role_id', 2)->get()->toArray();
            $data = [];
            foreach ($datas as $row) {
                $row['orders'] = Orders::where('user_id', $row['id'])->count();
                $row['revenue'] = Orders::where('user_id', $row['id'])->where('order_status', 'delivered')->sum('total_amount');
                $row['date'] = Orders::where('user_id', $row['id'])->where('order_status', 'delivered')->orderBy('created_at', 'DESC')->value('created_at');
                $row['order_date'] = Orders::where('user_id', $row['id'])->whereNotIn('order_status', ['cancel', 'return'])->orderBy('updated_at', 'DESC')->value('updated_at');
                $row['user_date'] = \Carbon\Carbon::parse($row['created_at'])->format('Y-m-d');
                $row['product_qty'] = Orders::leftjoin('order_products','order_products.order_id','=','orders.id')->where('orders.user_id', $row['id'])->sum('order_products.qty');
                $row['address'] = UserAddress::where('user_id',$row['id'])->where('default','yes')->first();
                $data[] = $row;
            }

        }

        return view('Admin.Customer.index', compact('data','type'));
    }

    public function create()
    {
        return view('Admin.Customer.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'address_name' => 'required',
            'address_line1' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'address_type' => 'required',
        ]);

        $data = $request->all();
        $data['status'] = 'active';
        $data['otp_verify'] = 'yes';
        $data['user_role'] = '2';
        $user = new User();
        $user->fill($data);
        if ($user->save()) {
            $clientrole = Role::where('id', 2)->first();
            $user->attachRole($clientrole);

            $address['address_name'] = $request['address_name'];
            $address['address_line1'] = $request['address_line1'];
            $address['address_line2'] = $request['address_line2'];
            $address['city'] = $request['city'];
            $address['postal_code'] = $request['postal_code'];
            $address['address_type'] = $request['address_type'];

            $userAddress = new UserAddress();
            $userAddress->fill($address);
            $userAddress->save();

            Session::flash('message', '<div class="alert alert-success">Customer Added Successfully.!! </div>');
            return redirect('zaecy-admin/customers');
        }
    }

    public function filterData(Request $request)
    {


        return (view('Admin.Customer.view', compact('data'))->render());
    }
}
