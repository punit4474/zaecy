<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use App\Models\PageCategory;
use Illuminate\Http\Request;
use URL;
use Validator;
use Mail;
use File;
use Session;

class PageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PageCategory::get();

        return view('Admin.PageCategory.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collection = ['' =>'Select Collection'] + Collection::where('status','active')->pluck('name','id')->all();
        return view('Admin.PageCategory.create',compact('collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();

        $fit = new PageCategory();
        $fit->fill($data);
        if($fit->save()){
            Session::flash('message','<div class="alert alert-success">Page Category added Successfully.!! </div>');
            return redirect('zaecy-admin/page-category');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PageCategory::where('id',$id)->first();
        $collection = ['' =>'Select Collection'] + Collection::where('status','active')->pluck('name','id')->all();
        return view('Admin.PageCategory.edit',compact('data','collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        $update = PageCategory::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">PageCategory updated Successfully.!! </div>');
            return redirect('zaecy-admin/page-category');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = PageCategory::where('id',$id)->delete();

        if($delete){
            Session::flash('message','<div class="alert alert-danger">PageCategory removed Successfully.!! </div>');
            return redirect('zaecy-admin/page-category');
        }
    }

    /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = PageCategory::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = PageCategory::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; PageCategory status updated successfully. </div>');
            return redirect('zaecy-admin/page-category');
        }
    }
}
