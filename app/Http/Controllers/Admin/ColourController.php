<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Colours;
use URL;
use Validator;
use Mail;
use File;
use Session;

class ColourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Colours::get();
        return view('Admin.Colours.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Colours.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'colour_code'=>'required',
            'language_id'=>'required',
        ]);

        $data = $request->all();

        $colours = new Colours();
        $colours->fill($data);
        if($colours->save()){
            Session::flash('message','<div class="alert alert-success">Colours added Successfully.!! </div>');
            return redirect('zaecy-admin/colours');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Colours::where('id',$id)->first();
        return view('Admin.Colours.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'colour_code'=>'required',
            'language_id'=>'required',
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        $update = Colours::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">Colours updated Successfully.!! </div>');
            return redirect('zaecy-admin/colours');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Colours::where('id', $id)->delete();

        if($data){
            Session::flash('message','<div class="alert alert-danger">Colours Removed Successfully.!! </div>');
            return redirect('zaecy-admin/colours');
        }
    }
}
