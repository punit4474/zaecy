<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Collection;
use App\Models\Category;
use URL;
use Validator;
use Mail;
use File;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Category::get();

        return view('Admin.Category.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collection = ['' =>'Select Collection'] + Collection::where('status','active')->pluck('name','id')->all();
        return view('Admin.Category.create',compact('collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'language_id'=>'required',
            'collection_id'=>'required',
        ]);

        $data = $request->all();
        if ($request->file('image')) {

            $file = $request->file('image');
            $filename = 'category-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/category', $filename);
            $data['image'] = $filename;
        }

        $category = new Category();
        $category->fill($data);
        if($category->save()){
            Session::flash('message','<div class="alert alert-success">Category Added Successfully.!! </div>');
            return redirect('zaecy-admin/category');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::where('id',$id)->first();
        $collection = ['' =>'Select Collection'] + Collection::where('status','active')->where('language_id',$data['language_id'])->pluck('name','id')->all();
        return view('Admin.Category.edit',compact('collection','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'language_id'=>'required',
            'collection_id'=>'required',
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        if ($request->file('image')) {

            $oldimage = Category::where('id', $id)->value('image');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/category/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'category-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/category', $filename);
            $data['image'] = $filename;
        }

        $update = Category::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">Category Updated Successfully.!! </div>');
            return redirect('zaecy-admin/category');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldimage = Category::where('id', $id)->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/category/' . $oldimage);
        }
        $data = Category::where('id', $id)->delete();

        if($data){
            Session::flash('message','<div class="alert alert-danger">Category Removed Successfully.!! </div>');
            return redirect('zaecy-admin/category');
        }
    }

     /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = Category::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = Category::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; Category status updated successfully. </div>');
            return redirect('zaecy-admin/category');
        }
    }
}
