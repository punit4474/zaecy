<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Style;
use URL;
use Validator;
use Mail;
use File;
use Session;


class StyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Style::get();

        return view('Admin.Style.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Style.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();

        $fit = new Style();
        $fit->fill($data);
        if($fit->save()){
            Session::flash('message','<div class="alert alert-success">Style added Successfully.!! </div>');
            return redirect('zaecy-admin/style');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Style::where('id',$id)->first();
        return view('Admin.Style.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        $update = Style::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">Style updated Successfully.!! </div>');
            return redirect('zaecy-admin/style');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Style::where('id',$id)->delete();

        if($delete){
            Session::flash('message','<div class="alert alert-danger">Style removed Successfully.!! </div>');
            return redirect('zaecy-admin/style');
        }
    }

     /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = Style::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = Style::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; Style status updated successfully. </div>');
            return redirect('zaecy-admin/style');
        }
    }
}
