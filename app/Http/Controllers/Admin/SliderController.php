<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Category;
use URL;
use Validator;
use Mail;
use File;
use Session;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Slider::get();
        return view('Admin.Slider.index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = ['' =>'Select Collection'] + Collection::where('status','active')->pluck('name','id')->all();
        return view('Admin.Slider.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=>'required',
            'image'=>'required',
            'type'=>'required',
            'language_id'=>'required',
        ]);


        $data = $request->all();

        if ($request->file('image')) {
            $file = $request->file('image');
            $filename = 'slider-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/slider', $filename);
            $data['image'] = $filename;
        }

        $slider = new Slider();
        $slider->fill($data);
        if($slider->save()){
            Session::flash('message','<div class="alert alert-success">Slider added Successfully.!! </div>');
            return redirect('zaecy-admin/slider');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Slider::where('id',$id)->first();
        $category = ['' =>'Select Collection'] + Collection::where('status','active')->pluck('name','id')->all();

        return view('Admin.Slider.edit',compact('data','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'type'=>'required',
            'language_id'=>'required',
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        if ($request->file('image')) {

            $oldimage = Slider::where('id', $id)->value('image');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/slider/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'slider-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/slider', $filename);
            $data['image'] = $filename;
        }

        $update = Slider::where('id',$id)->update($data);

        if($update){
            Session::flash('message','<div class="alert alert-success">Slider updated Successfully.!! </div>');
            return redirect('zaecy-admin/slider');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldimage = Slider::where('id', $id)->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/slider/' . $oldimage);
        }
        $data = Slider::where('id', $id)->delete();

        if($data){
            Session::flash('message','<div class="alert alert-danger">Slider Removed Successfully.!! </div>');
            return redirect('zaecy-admin/slider');
        }
    }

    /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = Slider::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = Slider::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; Slider status updated successfully. </div>');
            return redirect('zaecy-admin/slider');
        }
    }
}
