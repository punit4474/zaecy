<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SleeveType;
use URL;
use Validator;
use Mail;
use File;
use Session;

class SleeveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SleeveType::get();

        return view('Admin.SleeveType.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.SleeveType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();

        $fit = new SleeveType();
        $fit->fill($data);
        if($fit->save()){
            Session::flash('message','<div class="alert alert-success">SleeveType added Successfully.!! </div>');
            return redirect('zaecy-admin/sleeve-type');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SleeveType::where('id',$id)->first();
        return view('Admin.SleeveType.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        $update = SleeveType::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">SleeveType updated Successfully.!! </div>');
            return redirect('zaecy-admin/sleeve-type');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = SleeveType::where('id',$id)->delete();

        if($delete){
            Session::flash('message','<div class="alert alert-danger">SleeveType removed Successfully.!! </div>');
            return redirect('zaecy-admin/sleeve-type');
        }
    }

    /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = SleeveType::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = SleeveType::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; SleeveType status updated successfully. </div>');
            return redirect('zaecy-admin/sleeve-type');
        }
    }
}
