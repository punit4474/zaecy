<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NackStyle;
use URL;
use Validator;
use Mail;
use File;
use Session;

class NackStyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = NackStyle::get();

        return view('Admin.NackStyle.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.NackStyle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();

        $fit = new NackStyle();
        $fit->fill($data);
        if($fit->save()){
            Session::flash('message','<div class="alert alert-success">NackStyle added Successfully.!! </div>');
            return redirect('zaecy-admin/nack-style');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = NackStyle::where('id',$id)->first();
        return view('Admin.NackStyle.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        $update = NackStyle::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">NackStyle updated Successfully.!! </div>');
            return redirect('zaecy-admin/nack-style');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = NackStyle::where('id',$id)->delete();

        if($delete){
            Session::flash('message','<div class="alert alert-danger">NackStyle removed Successfully.!! </div>');
            return redirect('zaecy-admin/nack-style');
        }
    }

    /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = NackStyle::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = NackStyle::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; NackStyle status updated successfully. </div>');
            return redirect('zaecy-admin/nack-style');
        }
    }
}
