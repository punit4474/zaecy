<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use URL;
use Validator;
use Mail;
use File;
use Session;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SubCategory::get();

        return view('Admin.SubCategory.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collection = ['' => 'Select Collection'] + Collection::where('status', 'active')->pluck('name', 'id')->all();
        $category = ['' => 'Select Category'];
        return view('Admin.SubCategory.create',compact('category','collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'language_id'=>'required',
            'category_id'=>'required',
        ]);

        $data = $request->all();
        if ($request->file('image')) {

            $file = $request->file('image');
            $filename = 'subcategory-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/subcategory', $filename);
            $data['image'] = $filename;
        }

        $category = new SubCategory();
        $category->fill($data);
        if($category->save()){
            Session::flash('message','<div class="alert alert-success">SubCategory Added Successfully.!! </div>');
            return redirect('zaecy-admin/subcategory');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SubCategory::where('id',$id)->first();
        $collection = ['' => 'Select Collection'] + Collection::where('status', 'active')->pluck('name', 'id')->all();

        $category = ['' =>'Select Category'] + Category::where('status','active')->where('collection_id', $data['collection_id'])->where('language_id',$data['language_id'])->pluck('name','id')->all();
        return view('Admin.SubCategory.edit',compact('category','data','collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'language_id'=>'required',
            'category_id'=>'required',
        ]);

        $data = $request->all();
        $data = $request->except('_token','_method');

        if ($request->file('image')) {

            $oldimage = SubCategory::where('id', $id)->value('image');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/subcategory/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'subcategory-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/subcategory', $filename);
            $data['image'] = $filename;
        }

        $update = SubCategory::where('id',$id)->update($data);
        if($update){
            Session::flash('message','<div class="alert alert-success">SubCategory Updated Successfully.!! </div>');
            return redirect('zaecy-admin/subcategory');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldimage = SubCategory::where('id', $id)->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/subcategory/' . $oldimage);
        }
        $data = SubCategory::where('id', $id)->delete();

        if($data){
            Session::flash('message','<div class="alert alert-danger">SubCategory Removed Successfully.!! </div>');
            return redirect('zaecy-admin/subcategory');
        }
    }

     /**
     * Change Status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatus($id){
        $getStatus = SubCategory::where('id',$id)->value('status');

        if($getStatus == 'active'){
            $newStatus['status'] = 'inactive';
        } else {
            $newStatus['status'] = 'active';
        }

        $update = SubCategory::where('id',$id)->update($newStatus);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong>&nbsp; SubCategory status updated successfully. </div>');
            return redirect('zaecy-admin/subcategory');
        }
    }

    public function getCategory(Request $request){
        $rule = [
            'language_id'=>'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Category::where('language_id',$request['language_id'])->where('status','active')->get();

        foreach($data as $row){
            $row['image'] = URL::to(config('app.asset_directory') .'storage/app/public/category/'.$row['image']);
        }

        return $data = ['status' =>'success','data'=>$data];
    }
}
