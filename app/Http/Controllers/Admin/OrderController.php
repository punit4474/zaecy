<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\Payment;
use App\Models\ProductColours;
use App\Models\ProductImage;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;
use URL;
use Validator;
use Mail;
use File;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request['filter'];
        $totalOrder = Orders::count();
        $totalRevenue = Orders::where('order_status','delivered')->sum('total_amount');
        $totalFulfilled = Orders::where('order_status','delivered')->count();
        $totalUnFulfilled = Orders::where('order_status','!=','delivered')->count();
        $datas = Orders::get()->toArray();
        $data = [];
        foreach ($datas as $row){
            $row['name'] = User::where('id',$row['user_id'])->value('name');
            $row['payment_status'] = Payment::where('order_id',$row['id'])->value('payment_status');
            $row['qty'] = OrderProducts::where('order_id',$row['id'])->sum('qty');
            $data[] = $row;
        }

        if (!empty($type)) {
            if ($type == 'order_desc') {

                array_multisort(array_column($data, 'order_id'), SORT_DESC, $data);
            }
            if ($type == 'order_asc') {

                array_multisort(array_column($data, 'order_id'), SORT_ASC, $data);
            }
            if ($type == 'high') {

                array_multisort(array_column($data, 'total_amount'), SORT_DESC, $data);
            }
            if ($type == 'low') {

                array_multisort(array_column($data, 'total_amount'), SORT_ASC, $data);
            }
        }


        return view('Admin.Order.index',compact('data','totalOrder','totalFulfilled','totalRevenue','totalUnFulfilled','type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer =  [''=>'Select Customer'] + User::leftjoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.role_id', 2)->pluck('name','id')->all();
        $product = ['' => 'Select Products']+ Products::where('status','active')->pluck('product_name','id')->all();

        return view('Admin.Order.create',compact('customer','product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Orders::where('id',$id)->first();
        $products = OrderProducts::where('order_id', $data->id)->get();
        foreach ($products as $item) {
            $item->colour = ProductColours::where('product_id', $item->product_id)->where('id', $item->product_colour)->value('colour_name');
            $image = ProductImage::where('product_id', $item->product_id)->where('colour_id', $item->product_colour)->value('image');
            if (!empty($image)) {
                $item->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $item->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
        }
        $data->product = $products;
        return view('Admin.Order.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data = $request->except('_method','_token');

        $update = Orders::where('id',$id)->update($data);

        if($update){
            return redirect('zaecy-admin/orders/'.$id.'/edit');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('zaecy-admin/orders');
    }
}
