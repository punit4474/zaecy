<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Collection;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Subscriber;
use URL;
use Validator;
use Mail;
use File;

class CategoryController extends Controller
{
    public function getCollection(Request $request)
    {
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Collection::where('language_id',$request['language_id'])->where('status','active')->get();

        foreach($data as $row){
            if($row->image != ''){

                $row['image'] = config('app.asset_url').'/storage/app/public/collection/'.$row['image'];
            } else {
                $row['image'] = config('app.asset_url').'/storage/app/public/default/default_user.png';
            }
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collection get Successful', 'ResponseData' => $data], 200);
    }

    public function getCategory(Request $request)
    {
        $rule = [
            'language_id'=>'required',
            'collection_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Category::where('language_id',$request['language_id'])->where('collection_id',$request['collection_id'])->where('status','active')->get();

        foreach($data as $row){
            if($row->image != ''){
                $row['image'] = config('app.asset_url').'/storage/app/public/category/'.$row['image'];
            } else {
                $row['image'] = config('app.asset_url').'/storage/app/public/default/default_user.png';
            }

        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Category get Successful', 'ResponseData' => $data], 200);
    }

    public function getSubcategory(Request $request)
    {
        $rule = [
            'language_id'=>'required',
            'category_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = SubCategory::where('language_id',$request['language_id'])->where('category_id',$request['category_id'])->where('status','active')->get();

        foreach($data as $row){
            if($row->image != ''){
                $row['image'] = config('app.asset_url').'/storage/app/public/subcategory/'.$row['image'];
            } else {
                $row['image'] = config('app.asset_url').'/storage/app/public/default/default_user.png';
            }

        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'SubCategory get Successful', 'ResponseData' => $data], 200);
    }

    public function newsLetter(Request $request)
    {
        $rule = [
            'email'=>'required',
        ];

        $message = [
            'email.required' => 'email is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();

        $subscribe = new Subscriber();
        $subscribe->fill($data);
        if($subscribe->save()){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Newsletter Subscribe Successful', 'ResponseData' => $subscribe], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => []], 200);
        }

    }

    public function getAllCollection(Request $request){
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Collection::where('status','active')->where('language_id',$request['language_id'])->get();

        foreach ($data as $row){
            $row->category = Category::where('collection_id',$row->id)->where('language_id',$request['language_id'])->get();
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Get All Collection Successful', 'ResponseData' => $data], 200);


    }

    public function getHeaderCollection(Request $request){
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Collection::where('status','active')->where('language_id',$request['language_id'])->get();

        foreach ($data as $row){
            $row->category = Category::where('collection_id',$row->id)->where('language_id',$request['language_id'])->get();
            foreach ($row->category as $item){
                $item->subcategory = SubCategory::where('category_id',$item->id)->where('language_id',$request['language_id'])->get();
            }
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Get All Collection Successful', 'ResponseData' => $data], 200);
    }

    public function getSubcategoryCollection(Request $request){
        $rule = [
            'language_id'=>'required',
            'collection_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $getCategory = Category::where('collection_id',$request['collection_id'])->where('status','active')->pluck('id')->all();

        $data = SubCategory::where('language_id',$request['language_id'])->whereIn('category_id',$getCategory)->where('status','active')->get();

        foreach($data as $row){
            if($row->image != ''){
                $row['image'] = config('app.asset_url').'/storage/app/public/subcategory/'.$row['image'];
            } else {
                $row['image'] = config('app.asset_url').'/storage/app/public/default/default_user.png';
            }

        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'SubCategory get Successful', 'ResponseData' => $data], 200);
    }
}
