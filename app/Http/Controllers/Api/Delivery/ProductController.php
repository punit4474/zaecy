<?php

namespace App\Http\Controllers\Api\Delivery;

use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\ProductSize;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $data = ProductSize::leftjoin('products','products.id','=','product_sizes.product_id')
                ->leftjoin('product_colours','product_colours.id','=','product_sizes.colour_id')
                ->leftjoin('categories','categories.id','=','products.category_id')
                ->where('products.language_id',1)
                ->select('product_sizes.*','products.product_name','product_colours.colour_name','products.price','products.category_id','categories.name as category_name')
                ->paginate(50);

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product get Successful', 'ResponseData' => $data], 200);
    }
}
