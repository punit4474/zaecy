<?php

namespace App\Http\Controllers\Api\Delivery;

use App\Http\Controllers\Controller;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\ProductSize;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        $data = Orders::where('order_status','confirm')->paginate(20);
        foreach ($data as $row){
            $row->tax_amount = 0;
            $row->payment_method = 'card';
            $row->country_code = '971';
            $row->email = @$row->userData->email;
            $row->name = @$row->userData->name;
            $row->mobile_number = @$row->userData->phone_number;
            $row->order_amount  = $row->total_amount;
            $orderData = OrderProducts::where('order_id',$row->id)->get();
            foreach ($orderData as $rows){
                $rows->tax_amount = 0;
                $rows->discount_amount = 0;
                $rows->line_amount = $rows->qty * $rows->price;
            }
            $row->order_data = $orderData;
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Orders get Successful', 'ResponseData' => $data], 200);
    }

    public function cancelOrder(){
        $data = Orders::where('order_status','cancel')->select('id','updated_at','total_amount','order_status')->paginate(50);

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Cancel Orders get Successful', 'ResponseData' => $data], 200);
    }

    public function returnOrder(){
        $data = Orders::where('order_status','return')->select('id','updated_at','total_amount','order_status')->paginate(50);
        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Return Orders get Successful', 'ResponseData' => $data], 200);
    }

    public function awbTracking(Request $request){
        $rule = [
            'order_id'=>'required',
        ];

        $message = [
            'order_id.required' => 'Order id is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }


    }

    public function stockUpdate(Request $request){
        $data = $request['product'];

        foreach ($data as $row){
            $datas['sku'] = $row['sku'];
            $datas['size'] = $row['sizeCode'];
            $datas['qty'] = $row['qty'];

            $updateQty = ProductSize::where('sku',$row['sku'])->update($datas);
        }
        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Qty updated Successful', 'ResponseData' => []], 200);
    }
}
