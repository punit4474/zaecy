<?php

namespace App\Http\Controllers\Api\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductImage;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\ProductColours;
use App\Models\Collection;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Colours;
use URL;
use Validator;
use Mail;
use File;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('language_id',$request['language_id'])->get();

        foreach($data as $row){
            $productImage = ProductImage::where('product_id',$row->id)->get();
            foreach($productImage as $item){
                $item->image =URL::to('storage/app/public/product/'.$item->image);
            }
            $row->image = $productImage;
            $row->sizes = ProductSize::where('product_id',$row->id)->get();
            $row->colours = ProductColours::where('product_id',$row->id)->get();
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product get Successful', 'ResponseData' => $data], 200);
    }

    public function getCollection(Request $request)
    {
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Collection::where('language_id',$request['language_id'])->where('status','active')->get();

        foreach($data as $row){
            $row['image'] = URL::to('storage/app/public/collection/'.$row['image']);
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collection get Successful', 'ResponseData' => $data], 200);
    }

    public function getCategory(Request $request)
    {
        $rule = [
            'language_id'=>'required',
            'collection_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Category::where('language_id',$request['language_id'])->where('collection_id',$request['collection_id'])->where('status','active')->get();

        foreach($data as $row){
            $row['image'] = URL::to('storage/app/public/category/'.$row['image']);  
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Category get Successful', 'ResponseData' => $data], 200);
    }

    public function getSubcategory(Request $request)
    {
        $rule = [
            'language_id'=>'required',
            'category_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = SubCategory::where('language_id',$request['language_id'])->where('category_id',$request['category_id'])->where('status','active')->get();

        foreach($data as $row){
            $row['image'] = URL::to('storage/app/public/subcategory/'.$row['image']);  
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'SubCategory get Successful', 'ResponseData' => $data], 200);
    }

    public function create(Request $request)
    {
        $rule = [
            'product_id'=>'required',
            'collection_id'=>'required',
            'category_id'=>'required',
            'subcategory_id'=>'required',
            'language_id'=>'required',
            'product_name'=>'required',
            'brand_name'=>'required',
            'material_composition'=>'required',
            'material_type'=>'required',
            // 'country_region'=>'required',
            // 'model_fit'=>'required',
            // 'weight'=>'required',
            // 'fit'=>'required',
            // 'style'=>'required',
            // 'nack_style'=>'required',
            // 'sleeve_style'=>'required',
            'price'=>'required',
            // 'discount_type'=>'required',
            // 'discount_value'=>'required',
            'hashtag'=>'required',
            'images.*'=>'required',
            // 'video'=>'required',
            'size.*'=>'required'
        ];
        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
        $product = new Products();
        $product->fill($data);
        if($product->save()){
            $images = $request['images'];

            if(!empty($images)){
                foreach ($images as $item) {
                    if (!empty($item)) {
                        $extension = $item->getClientOriginalExtension();

                        $destinationpath = storage_path('app/public/product');

                        $filename = 'product-' . uniqid() . '-' . rand(1, 9999) . '.' . $extension;

                        $item->move($destinationpath, $filename);

                        $barImage['image'] = $filename;
                        $barImage['type'] = 'image';
                        $barImage['product_id'] = $product->id;

                        $product_img = new ProductImage();
                        $product_img->fill($barImage);
                        $product_img->save();
                    }

                }
            }

            $video = $request['video'];

            if ($request->file('video')) {

                $fileVideo = $request->file('video');
                $filenameVideo = 'video-' . uniqid() . '.' . $fileVideo->getClientOriginalExtension();
                $fileVideo->move('storage/app/public/product/video', $filenameVideo);
                $data['image'] = $filenameVideo;

                $barVideo['image'] = $filenameVideo;
                $barVideo['type'] = 'video';
                $barVideo['product_id'] = $product->id;

                $product_video = new ProductImage();
                $product_video->fill($barVideo);
                $product_video->save();
            }  

            $colour = $request['colour'];

            if(!empty($colour)){
                foreach($colour as $row){
                    $code = Colours::where('id',$row['colour'])->value('colour_code');
                    $c['product_id'] = $product->id;
                    $c['colour'] = $row['colour'];
                    $c['qty'] = $row['qty'];
                    $c['colour_code'] = $code;

                    $colourData = new ProductColours();
                    $colourData->fill($c);
                    $colourData->save();
                }
            }


        }
    }
}
