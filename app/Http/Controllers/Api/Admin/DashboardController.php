<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $totalTransaction = '300';
        $totalUnitSold = '20';
        $averageBasketSide = '45.21';
        $totalRevenue = '4500';

        $topProduct = [];
        $mostRecentlySold = [];

        $data['totalTransaction'] = $totalTransaction;
        $data['totalUnitSold'] = $totalUnitSold;
        $data['averageBasketSide'] = $averageBasketSide;
        $data['totalRevenue'] = $totalRevenue;
        $data['topProduct'] = $topProduct;
        $data['mostRecentlySold'] = $mostRecentlySold;

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Successful', 'ResponseData' => $data], 200);
    }
}
