<?php

namespace App\Http\Controllers\Api\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\ApiSessions;
use App\Models\User;
use Hash;
use Validator;
use URL;
use Mail;
use File;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $rule = [
            'email' => 'required',
            'password' => 'required'
        ];

        $message = [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required'
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = User::where('phone_number', $request['email'])->where('status','active')->first();

        if(empty($user)){
            $user = User::where('email', $request['email'])->where('status','active')->first();
        }

        if (!empty($user)) {

            if (Hash::check($request['password'], $user->password)) {

                if($user->user_role != 1){
                    return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Invalid User type', 'ResponseData' => null], 400);
                }
                $token = \BaseFunction::setSessionData($user->id,$user->user_role);

                $user['login_type'] = $request['login_type'];
                $data = ['token' => $token, 'user' => $user];

                // unset($user['roles']);
                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Login Successful', 'ResponseData' => $data], 200);

            } else {
                return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Invalid Password', 'ResponseData' => null], 400);
            }

        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'User not registered', 'ResponseData' => null], 400);
        }
    }
}
