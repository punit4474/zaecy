<?php

namespace App\Http\Controllers\Api\Admin\Collection;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Collection;
use URL;
use Validator;
use Mail;
use File;

class CollectionController extends Controller
{
    public function index(Request $request)
    {
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Collection::where('language_id',$request['language_id'])->get();

        foreach($data as $row){
            $row['image'] = URL::to('storage/app/public/collection/'.$row['image']);
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collection get Successful', 'ResponseData' => $data], 200);
    }

    public function create(Request $request)
    {
        $rule = [
            'name' => 'required',
            'language_id'=>'required'
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
        if ($request->file('image')) {

            $file = $request->file('image');
            $filename = 'collection-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/collection', $filename);
            $data['image'] = $filename;
        }  

        $collection = new Collection();
        $collection->fill($data);
        if($collection->save()){
            $collection['image'] = URL::to('storage/app/public/collection/'.$collection['image']);
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collection Added Successful', 'ResponseData' => $collection], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }

    }

    public function edit(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id'=>'required'
        ];

        $message = [
            'id.required' => 'Id is required',
            'language_id'=>'language id is required'
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Collection::where('id',$request['id'])->where('language_id',$request['language_id'])->first();
        if(!empty($data['image'])){
            $data['image'] = URL::to('storage/app/public/collection/'.$data['image']);
        }
    
        if(!empty($data)){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collection get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => 'required',
            'id'=>'required',
            'language_id'=>'required',
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
        $data = $request->except('user');
        if ($request->file('image')) {

            $oldimage = Collection::where('id', $request['id'])->value('image');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/collection/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'collection-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/collection', $filename);
            $data['image'] = $filename;
        }  

        $collection = Collection::where('id',$request['id'])->where('language_id',$request['language_id'])->update($data);

        if($collection){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collection Updated Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }

    }

    public function destroy(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id' => 'required',
        ];

        $message = [
            'id.required' => 'Id is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $oldimage = Collection::where('id', $request['id'])->where('language_id',$request['language_id'])->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/collection/' . $oldimage);
        }
        $data = Collection::where('id',$request['id'])->where('language_id',$request['language_id'])->delete();

        if($data){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collection removed Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }
}
