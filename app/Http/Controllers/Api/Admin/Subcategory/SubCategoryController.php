<?php

namespace App\Http\Controllers\Api\Admin\Subcategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubCategory;
use URL;
use Validator;
use Mail;
use File;

class SubCategoryController extends Controller
{
    public function index(Request $request)
    {
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = SubCategory::where('language_id',$request['language_id'])->get();

        foreach($data as $row){
            $row['image'] = URL::to('storage/app/public/subcategory/'.$row['image']);  
            $row['category_name'] = @$row->CategoryData->name;
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'SubCategory get Successful', 'ResponseData' => $data], 200);
    }

    public function create(Request $request)
    {
        
        $rule = [
            'name' => 'required',
            'language_id'=>'required',
            'category_id'=>'required',
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
        $data['category_id'] = (int)$data['category_id'];
        if ($request->file('image')) {

            $file = $request->file('image');
            $filename = 'subcategory-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/subcategory', $filename);
            $data['image'] = $filename;
        }  

        $subcategory = new SubCategory();
        $subcategory->fill($data);
        if($subcategory->save()){
            $subcategory['image'] = URL::to('storage/app/public/subcategory/'.$subcategory['image']);
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Sub Category Added Successful', 'ResponseData' => $subcategory], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    public function edit(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id'=>'required'
        ];

        $message = [
            'id.required' => 'Id is required',
            'language_id'=>'language id is required'
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        
        $data = SubCategory::where('id',$request['id'])->where('language_id',$request['language_id'])->first();
        if(!empty($data['image'])){
            $data['image'] = URL::to('storage/app/public/subcategory/'.$data['image']);
        }
    
        if(!empty($data)){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'subcategory get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => 'required',
            'id'=>'required',
            'language_id'=>'required',
            'category_id'=>'required',
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
   
        $data = $request->except('user');
        if ($request->file('image')) {

            $oldimage = SubCategory::where('id', $request['id'])->value('image');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/subcategory/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'subcategory-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/subcategory', $filename);
            $data['image'] = $filename;
        }  

        $subcategory = SubCategory::where('id',$request['id'])->where('language_id',$request['language_id'])->update($data);

        if($subcategory){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'SubCategory Updated Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }

    }

    public function destroy(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id' => 'required',
        ];

        $message = [
            'id.required' => 'Id is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $oldimage = SubCategory::where('id', $request['id'])->where('language_id',$request['language_id'])->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/subcategory/' . $oldimage);
        }
        $data = SubCategory::where('id',$request['id'])->where('language_id',$request['language_id'])->delete();

        if($data){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'SubCategory removed Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }
}
