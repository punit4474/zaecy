<?php

namespace App\Http\Controllers\Api\Admin\Colours;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Colours;
use URL;
use Validator;
use Mail;
use File;


class ColoursController extends Controller
{
    public function index(Request $request)
    {
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }


        $data = Colours::where('language_id',$request['language_id'])->get();

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Colours Get Successful', 'ResponseData' => $data], 200);
    }

    public function create(Request $request)
    {
        $rule = [
            'name' => 'required',
            'language_id'=>'required',
            'colour_code'=>'required',
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();

        $colour = new Colours();
        $colour->fill($data);
        if($colour->save()){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Colour Added Successful', 'ResponseData' => $colour], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    public function edit(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id'=>'required'
        ];

        $message = [
            'id.required' => 'Id is required',
            'language_id'=>'language id is required'
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Colours::where('id',$request['id'])->where('language_id',$request['language_id'])->first();

        if(!empty($data)){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Colour get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => 'required',
            'language_id'=>'required',
            'colour_code'=>'required',
            'id'=>'required',
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
        $data = $request->except('user');

        $colour = Colours::where('id',$request['id'])->where('language_id',$request['language_id'])->update($data);
        
        if($colour){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Colour Updated Successful', 'ResponseData' => []], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    public function destroy(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id' => 'required',
        ];

        $message = [
            'id.required' => 'Id is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $delete = Colours::where('id',$request['id'])->where('language_id',$request['language_id'])->delete();

        if($delete){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Colours removed Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }
}
