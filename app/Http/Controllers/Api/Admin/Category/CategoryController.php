<?php

namespace App\Http\Controllers\Api\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use URL;
use Validator;
use Mail;
use File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rule = [
            'language_id'=>'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Category::where('language_id',$request['language_id'])->get();

        foreach($data as $row){
            $row['image'] = URL::to('storage/app/public/category/'.$row['image']);  
            $row['collection_name'] = @$row->CollectionData->name;
        }

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Category get Successful', 'ResponseData' => $data], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rule = [
            'name' => 'required',
            'language_id'=>'required',
            'collection_id'=>'required',
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
        if ($request->file('image')) {

            $file = $request->file('image');
            $filename = 'category-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/category', $filename);
            $data['image'] = $filename;
        }  

        $category = new Category();
        $category->fill($data);
        if($category->save()){
            $category['image'] = URL::to('storage/app/public/category/'.$category['image']);
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Category Added Successful', 'ResponseData' => $category], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id'=>'required'
        ];

        $message = [
            'id.required' => 'Id is required',
            'language_id'=>'language id is required'
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        
        $data = Category::where('id',$request['id'])->where('language_id',$request['language_id'])->first();
        if(!empty($data['image'])){
            $data['image'] = URL::to('storage/app/public/category/'.$data['image']);
        }
    
        if(!empty($data)){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Category get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rule = [
            'name' => 'required',
            'id'=>'required',
            'language_id'=>'required',
            'collection_id'=>'required',
        ];

        $message = [
            'name.required' => 'Name is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
   
        $data = $request->except('user');
        if ($request->file('image')) {

            $oldimage = Category::where('id', $request['id'])->value('image');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/category/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'category-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/category', $filename);
            $data['image'] = $filename;
        }  

        $category = Category::where('id',$request['id'])->where('language_id',$request['language_id'])->update($data);

        if($category){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Category Updated Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rule = [
            'id' => 'required',
            'language_id' => 'required',
        ];

        $message = [
            'id.required' => 'Id is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $oldimage = Category::where('id', $request['id'])->where('language_id',$request['language_id'])->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/category/' . $oldimage);
        }
        $data = Category::where('id',$request['id'])->where('language_id',$request['language_id'])->delete();

        if($data){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Category removed Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went Wrong', 'ResponseData' => null], 400);
        }
    }
}
