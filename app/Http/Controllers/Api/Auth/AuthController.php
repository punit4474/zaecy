<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\ApiSessions;
use App\Models\User;
use Hash;
use Validator;
use URL;
use Mail;
use File;

class AuthController extends Controller
{
	// Login Api
    public function login(Request $request)
    {
    	$rule = [
            'login_type' => 'required',
        ];

        $message = [
            'login_type.required' => 'Login type is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

	    if ($validate->fails()) {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
	    }

	    if($request['login_type'] == 'normal'){

	        $rule = [
	            'email' => 'required',
	            // 'password' => 'required'
	        ];

	        $message = [
	            'email.required' => 'Email / Phone Number is required',
	            'password.required' => 'Password is required'
	        ];

	        $validate = Validator::make($request->all(), $rule, $message);

	        if ($validate->fails()) {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
	        }

	        $user = User::where('phone_number', $request['email'])->where('status','active')->first();
			if(!empty($user)){
				$checkOTP = User::where('phone_number',$request['email'])->first();

				if(!empty($checkOTP)){
					$forgot_otp = rand(100000,999999);
					$updateUser = User::where('phone_number',$request['email'])->update(['otp'=>$forgot_otp]);
                    \BaseFunction::sendSMS($request['email'],$forgot_otp);

					return response()->json(['ResponseCode' => 1, 'ResponseText' => 'OTP Sent Successfully', 'ResponseData' => ['otp'=>$forgot_otp]], 200);
				} else {
					return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Mobile number not matched our records Try again.', 'ResponseData' => []], 400);
				}
			}
	        if(empty($user)){
	        	$user = User::where('email', $request['email'])->where('status','active')->first();
	        }

	        if (!empty($user)) {

	            if (Hash::check($request['password'], $user->password)) {
	                $token = \BaseFunction::setSessionData($user->id,$user->user_role);

			        $user['login_type'] = $request['login_type'];
	                $data = ['token' => $token, 'user' => $user];

	                // unset($user['roles']);
	                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Login Successful', 'ResponseData' => $data], 200);

	            } else {
	                return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Invalid Password', 'ResponseData' => null], 400);
	            }

	        } else {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'User not registered', 'ResponseData' => null], 400);
	        }
    	} elseif($request['login_type'] == 'social'){

    		$rule = [
            'social_type' => 'required',
            'social_id' => 'required',
	        ];

	        $message = [

	            'social_type.required' => 'Social media type is required',
	            'social_id.required' => 'Gender is required',
	        ];

	        $validate = Validator::make($request->all(), $rule, $message);

	        if ($validate->fails()) {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
	        }

	       	$user = User::where('social_type',$request['social_type'])->where('social_id',$request['social_id'])->where('status','active')->first();

	       	if (!empty($user)) {
	                $token = \BaseFunction::setSessionData($user->id,$user->user_role);

			        $user['login_type'] = $request['login_type'];
	                $data = ['token' => $token, 'user' => $user];
	                // unset($user['roles']);
	                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Login Successful', 'ResponseData' => $data], 200);

	        } else {
	            return response()->json(['ResponseCode' => 3, 'ResponseText' => 'User not registered', 'ResponseData' => null], 400);
	        }

    	}

    }
    // Register Api
    public function Register(Request $request)
    {
    	$rule = [
            'login_type' => 'required',
        ];

        $message = [
            'login_type.required' => 'Login type is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

	    if ($validate->fails()) {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
	    }

    	if($request['login_type'] == 'normal'){
    		$rule = [
            'name' => 'required',
            'phone_number' => 'required|unique:users',
            'password' => 'required|min:6',
	        ];

	        $message = [
	            'name.required' => 'Name is required',
	            'phone_number.required' => 'Phone Number is required',
	            'password.required' => 'password is required',
	        ];

	        $validate = Validator::make($request->all(), $rule, $message);

	        if ($validate->fails()) {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
	        }

	        $data = $request->all();
	        $data['otp'] = rand(100000,999999);
	        $data['status'] = 'active';
	        $data['user_role'] = 2;
	        $data['password'] = Hash::make($request['password']);


	        $user = new User();
	        $user->fill($data);
	        if($user->save()){

	        	$clientrole = Role::where('id', 2)->first();
            	$user->attachRole($clientrole);

	        	// $token = \BaseFunction::setSessionData($user->id,2);
                \BaseFunction::sendSMS($request['phone_number'],$data['otp']);

		       $user['login_type'] = $request['login_type'];
			   $data = ['user' => $user];

	        	return response()->json(['ResponseCode' => 1, 'ResponseText' => "Registration Successfully", 'ResponseData' => $data], 200);

	        }
    	} elseif($request['login_type'] == 'social'){
    		$rule = [
            'name' => 'required',
            'social_type' => 'required',
            'social_id' => 'required',
	        ];

	        $message = [

	            'name.required' => 'Name is required',
	            // 'phone_number.required' => 'Phone Number is required',
	        ];

	        $validate = Validator::make($request->all(), $rule, $message);

	        if ($validate->fails()) {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
	        }

	        $data = $request->all();
	        $data['status'] = 'active';
	        $data['otp_verify'] = 'yes';
	        $data['status'] = 'active';
	        $data['user_role'] = 2;


	        $user = new User();
	        $user->fill($data);
	        if($user->save()){
	        	$clientrole = Role::where('id', 2)->first();
            	$user->attachRole($clientrole);

	        	$token = \BaseFunction::setSessionData($user->id,2);

		        $user['login_type'] = $request['login_type'];
	            $data = ['token' => $token, 'user' => $user];

	        	return response()->json(['ResponseCode' => 1, 'ResponseText' => "Registration Successfully", 'ResponseData' => $data], 200);
	        }
    	}

    }

    // Verify OTP Api
    public function verifyOTP(Request $request)
    {
    		$rule = [
            'phone_number' => 'required',
            'verification_code' => 'required',
	        ];

	        $message = [
	            'phone_number.required' => 'Phone Number is required',
	            'verification_code.required' => 'Verification code is required',
	        ];

	        $validate = Validator::make($request->all(), $rule, $message);

	        if ($validate->fails()) {
	            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
	        }

	        $data = $request->all();

	        $checkOTP = User::where('phone_number',$request['phone_number'])->where('otp',$request['verification_code'])->first();

	        if(!empty($checkOTP)){
	        	$updateUser = User::where('phone_number',$request['phone_number'])->where('otp',$request['verification_code'])->update(['otp_verify'=>'yes']);

				$token = \BaseFunction::setSessionData($checkOTP->id,$checkOTP->user_role);


	            $data = ['token' => $token, 'user' => $checkOTP];

	                // unset($user['roles']);
	            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Login Successful', 'ResponseData' => $data], 200);

        	} else {
        		return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Invalid verification code', 'ResponseData' => []], 400);
        	}

	}

    // Change Password Api
    public function changePassword(Request $request)
    {

        $rule = [
            'current_password' => 'required',
            'new_password' => ['required'],
            'confirm_password' => 'required|same:new_password'
        ];
        $message = [
            'current_password.required' => 'current_password is required',
            'new_password.required' => 'password is required',
            'new_password.min' => 'password Must be 8 Characters',
            'confirm_password.required' => 'confirm password is required',

        ];
        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        try {

            $data  = request()->all();
  			$userId = $data['user']['user_id'];
            $getUser = User::where('id',$userId)->first();

            if (!Hash::check($request['current_password'], $getUser->password)) {
				return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Current password not Match our record.', 'ResponseData' => null], 400);
            }

            $newPassword = Hash::make($data['new_password']);

            $updatePassword = User::where('id',$userId)->update(['password' => $newPassword]);
            if ($updatePassword) {
                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Password Change Successfull.', 'ResponseData' => null], 200);
            }
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => null], 400);
        } catch (\Swift_TransportException $e) {
            \Log::debug($e);
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => null], 400);
        }
    }

    // Logout Api
    public function logout(Request $request)
    {
        $user = $request['user'];
        $user_id = $user['user_id'];
        ApiSessions::where('user_id', $user_id)->delete();
        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Successfully logout', 'ResponseData' => null]);
    }

    // Forgot Password

    public function forgotPassword(Request $request)
    {
    	$rule = [
	        'phone_number' => 'required',
        ];

        $message = [
            'phone_number.required' => 'Phone Number is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();

        $checkOTP = User::where('phone_number',$request['phone_number'])->first();

        if(!empty($checkOTP)){
			$forgot_otp = rand(100000,999999);
            \BaseFunction::sendSMS($request['phone_number'],$forgot_otp);
        	$updateUser = User::where('phone_number',$request['phone_number'])->update(['forgot_otp'=>$forgot_otp]);

        	return response()->json(['ResponseCode' => 1, 'ResponseText' => 'OTP Sent Successfully', 'ResponseData' => ['otp'=>$forgot_otp]], 200);
    	} else {
    		return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Mobile number not matched our records Try again.', 'ResponseData' => []], 400);
    	}
    }

	public function setNewPassword(Request $request)
	{
		$rule = [
	        'phone_number' => 'required',
	        'otp' => 'required',
			'new_password' =>'required'
        ];

        $message = [
            'phone_number.required' => 'Phone Number is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

		$data = $request->all();

        $checkOTP = User::where('phone_number',$request['phone_number'])->where('forgot_otp',$data['otp'])->first();

        if(empty($checkOTP)){
            $checkOTP = User::where('email',$request['phone_number'])->where('forgot_otp',$data['otp'])->first();

        }

		if(!empty($checkOTP)){
			$newPassword = Hash::make($data['new_password']);
			$updateData = User::where('phone_number',$request['phone_number'])->where('forgot_otp',$data['otp'])->update(['password'=>$newPassword]);

			if ($updateData) {
				$updateOTP = User::where('phone_number',$request['phone_number'])->where('forgot_otp',$data['otp'])->update(['forgot_otp'=>'']);
                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Password Change Successfull.', 'ResponseData' => null], 200);
            }
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => null], 400);

		} else {
			return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Invalid verification code or Mobile number', 'ResponseData' => []], 400);
		}



	}

	public function forgotPasswordEmail(Request $request)
	{
		$rule = [
            'email' => 'required',
        ];
        $message = [
            'email.required' => 'email is required',
        ];
        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        $email = $request['email'];

        $check_user = User::where('email', $email)->first();

        if (empty($check_user)) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Email not registered', 'ResponseData' => null], 422);
        } else {

            $forgot_otp = rand(100000,999999);

            $updateUser = User::where('email',$request['email'])->update(['forgot_otp'=>$forgot_otp]);

            $name = $check_user['first_name'].' '.$check_user['last_name'];
            $title = 'Hey '.$name.', forgot your password?';
            $data = ['title' => $title, 'email' => $email, 'name' => $name, 'password' => $forgot_otp];

            try {
                Mail::send('email.pass_reset', $data, function ($message) use ($data) {
                    $message->from('notification@zaecy.com', env('MAIL_FROM_NAME',"Zaecy"))->subject($data['title']);
                    $message->to($data['email']);
                });

                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'New Password Sent to Email Address', 'ResponseData' => null], 200);
            } catch (\Swift_TransportException $e) {
                \Log::debug($e);
                return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => null], 400);
            }
        }
	}


}
