<?php

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Controller;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\Payment;
use App\Models\ProductColours;
use App\Models\ProductImage;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;
use URL;
use Validator;
use Mail;
use File;

class OrderController extends Controller
{
    public function index(Request $request)
    {

        $rule = [
            'total_amount' => 'required',
            'address_name' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'product.*' => 'required',
        ];
        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = $request['user'];
        $user_id = $user['user_id'];
        $data = $request->all();
        $data['user_id'] = $user_id;
        $data['order_id'] = \BaseFunction::orderNumber();

        $order = new Orders();
        $order->fill($data);
        if ($order->save()) {
            $products = $request['product'];
            $total = 0;

            foreach ($products as $row) {
                $productDetails = Products::where('id', $row['product_id'])->first();

//                $productDetails->final_price = $productDetails->price;
                $productDetails->final_price = \BaseFunction::finalPrice($productDetails->price, $productDetails->discount_type, $productDetails->discount_value);
                $productData['order_id'] = $order->id;
                $productData['product_id'] = $productDetails->id;
                $productData['product_sku'] = $row['product_sku'];
                $productData['product_name'] = $productDetails->product_name;
                $productData['product_colour'] = $row['colour_id'];
                $productData['product_size'] = $row['size'];
                $productData['product_size_sku'] = $row['variant_sku_id'];
                $productData['qty'] = $row['qty'];
                $productData['price'] = $productDetails->final_price;
                $orderProduct = new OrderProducts();
                $orderProduct->fill($productData);
                $orderProduct->save();
                $total += $row['qty'] * $productDetails->final_price;


            }

            $paymentData['order_id'] = $order->id;
            $paymentData['payment_method'] = 'card';
            $paymentData['payment_id'] = '';
            $paymentData['payment_amount'] = $total;
            $paymentData['payment_status'] = 'pending';

            $payment = new Payment();
            $payment->fill($paymentData);
            $payment->save();
            if(!empty($request['discount_code'])){
                $total = $total - $request['discount_amount'];
            }
            $update = Orders::where('id', $order->id)->update(['total_amount' => $total, 'payment_id' => $payment->id]);

            /**
             * Create Payment Token
             */


            $session = \BaseFunction::createPaymentSession($order->order_id, $total);
            $url = URL::to('backend/payment-procced?payment_token='.encrypt($order->order_id).'&session_token='.encrypt($session->session->id));
            $data = array(
                'order_id' => $order->order_id,
                'session_id' => $session->session->id,
                'amount' => $total,
                'url'=>$url,
            );


            $check_user = User::where('id', $user_id)->first();

            $name = $check_user['first_name'].' '.$check_user['last_name'];
            $title = 'Thanks for shopping with us, '.$name.'!';
            $datas = ['title' => $title, 'email' => $check_user['email'], 'name' => $name];

            try {
                Mail::send('email.shopping', $datas, function ($message) use ($datas) {
                    $message->from('notification@zaecy.com', env('MAIL_FROM_NAME',"Zaecy"))->subject($datas['title']);
                    $message->to($datas['email']);
                    $message->to('punitkathiriya@gmail.com');
                });


            } catch (\Swift_TransportException $e) {
                \Log::debug($e);
            }


            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Data Found.', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => []], 400);
        }

    }

    public function orderList(Request $request)
    {
        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = Orders::where('user_id', $user_id)->paginate(10);

        foreach ($data as $row) {
            $products = OrderProducts::where('order_id', $row->id)->get();
            foreach ($products as $item) {
                $item->colour = ProductColours::where('product_id', $item->product_id)->where('id', $item->product_colour)->value('colour_name');
                $image = ProductImage::where('product_id', $item->product_id)->where('colour_id', $item->product_colour)->value('image');
                if (!empty($image)) {
                    $item->image = config('app.asset_url').'/storage/app/public/product/' . $image;
                } else {
                    $item->image = config('app.asset_url').'/storage/app/public/default/default.png';
                }
            }
            $row->product = $products;
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Data Found.', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => []], 400);
        }
    }

    public function orderDetails(Request $request)
    {
        $rule = [
            'order_id' => 'required'
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = $request['user'];
        $user_id = $user['user_id'];
//        dd($request['order_id']);
        $data = Orders::where('order_id', $request['order_id'])->where('user_id', $user_id)->first();

        $products = OrderProducts::where('order_id', $data->id)->get();
        foreach ($products as $item) {
            $item->colour = ProductColours::where('product_id', $item->product_id)->where('id', $item->product_colour)->value('colour_name');
            $image = ProductImage::where('product_id', $item->product_id)->where('colour_id', $item->product_colour)->value('image');
            if (!empty($image)) {
                $item->image = config('app.asset_url').'/storage/app/public/product/' . $image;
            } else {
                $item->image = config('app.asset_url').'/storage/app/public/default/default.png';
            }
        }
        $data->product = $products;
        $payment = Payment::where('order_id', $data->id)->first();
        $data->payment = $payment;


        if (!empty($data)) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Data Found.', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => []], 400);
        }
    }

    public function cancelOrder(Request $request)
    {
        $rule = [
            'order_id' => 'required'
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = Orders::where('id', $request['order_id'])->where('user_id', $user_id)->first();
        if (!empty($data)) {
            $update = Orders::where('id', $request['order_id'])->where('user_id', $user_id)->update(['order_status' => 'cancel']);
            if ($update) {
                $updatePayment = Payment::where('order_id',$request['order_id'])->update(['payment_status'=>'refunded']);
                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Order Has been cancelled', 'ResponseData' => []], 200);
            } else {
                return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => []], 400);
            }

        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something went wrong', 'ResponseData' => []], 400);
        }
    }
}
