<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\PageCategory;
use App\Models\ProductPageCategory;
use App\Models\UserWishList;
use Illuminate\Http\Request;
use App\Models\ProductImage;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\ProductColours;
use App\Models\Collection;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Colours;
use URL;
use Validator;
use Mail;
use File;

class ProductController extends Controller
{
    /**
     * Get Latest Product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('language_id', $request['language_id'])->where('status', 'active')->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }

            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }

    }

    /**
     * Popular product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function popularProduct(Request $request)
    {
        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('language_id', $request['language_id'])->where('status', 'active')->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Popular Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }

    }

    /**
     * Category wise Product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function categoryProduct(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'category_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('category_id', $request['category_id'])->where('language_id', $request['language_id'])->where('status', 'active')->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Categorywise Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }

    /**
     * Subcategory Wise Product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subcategoryProduct(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'subcategory_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('subcategory_id', $request['subcategory_id'])->where('language_id', $request['language_id'])->where('status', 'active')->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'SubCategorywise Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }

    /**
     * Collection wise Product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectionProduct(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'collection_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('collection_id', $request['collection_id'])->where('language_id', $request['language_id'])->where('status', 'active')->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Collectionwise Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }

    /**
     * Get Page Category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPageCategory(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'collection_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = PageCategory::where('language_id', $request['language_id'])->where('collection_id',$request['collection_id'])->where('status', 'active')->get();

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'PageCategory get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'No Data Found', 'ResponseData' => $data], 200);
        }

    }

    /**
     * Get Product page category wise
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductPageCategory(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'category_id' => 'required',
            'collection_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $getProducts = ProductPageCategory::where('page_category_id', $request['category_id'])->groupBy('product_id')->select('product_id')->get()->toArray();

        $data = Products::where('language_id', $request['language_id'])->where('collection_id',$request['collection_id'])->whereIn('id', $getProducts)->where('status', 'active')->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }

    /**
     * Product View
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productView(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'product_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('language_id', $request['language_id'])->where('product_id', $request['product_id'])->where('status', 'active')->first();
        if (empty($data)) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No Product Found.', 'ResponseData' => []], 400);
        }
        $productImage = ProductColours::where('product_id', $data->id)->get();
        foreach ($productImage as $item) {
            $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
            $item->size = ProductSize::where('product_id', $data->id)->where('colour_id', $item->id)->get();
            $item->product_image = ProductImage::where('product_id', $data->id)->where('colour_id', $item->id)->get();
            foreach ($item->product_image as $v) {
                $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
            }
        }
        $data->colour = $productImage;
        $video = ProductImage::where('product_id', $data->id)->where('type', 'video')->value('image');
        if (!empty($video)) {
            $data->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
        } else {
            $data->video = '';
        }

        $data->sizes = ProductSize::where('product_id', $data->id)->groupBy('size')->get();
        $data->collection_name = @$data->CollectionData->name;
        $data->category_name = @$data->CategoryData->name;
        $data->subcategory_name = @$data->SubcategoryData->name;
        $data->fit_name = @$data->FitData->name;
        $data->style_name = @$data->StyleData->name;
        $data->nack_style_name = @$data->NackStyleData->name;
        $data->sleeve_style_name = @$data->SleeveStyleData->name;
        $data->final_price = \BaseFunction::finalPrice($data->price, $data->discount_type, $data->discount_value);

        $similarProduct = Products::where('language_id', $request['language_id'])->whereIn('id', explode(',', $data->similar_products))->where('status', 'active')->get();
        foreach ($similarProduct as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }
        $data->similar_products = $similarProduct;

        $recommended_products = Products::where('language_id', $request['language_id'])->whereIn('id', explode(',', $data->recommended_products))->where('status', 'active')->get();
        foreach ($recommended_products as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }
        $data->recommended_products = $recommended_products;


        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Data Found.', 'ResponseData' => $data], 200);
    }

    /**
     * Add to Wishlist
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addWishlist(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'product_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = $request->all();
        $data['user_id'] = $user_id;

        $addWishList = new UserWishList();
        $addWishList->fill($data);
        if ($addWishList->save()) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Added into wishlist Successful', 'ResponseData' => $addWishList], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }

    }

    /**
     * Remove From Wishlist
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function removeWishlist(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'product_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        $user = $request['user'];
        $user_id = $user['user_id'];

        $remove = UserWishList::where('user_id', $user_id)->where('product_id', $request['product_id'])->where('language_id', $request['language_id'])->delete();
        if ($remove) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Removed into wishlist Successful', 'ResponseData' => []], 200);
        }

    }

    /**
     * MyWishlist Product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function myWishlist(Request $request)
    {
        $rule = [
            'language_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        $user = $request['user'];
        $user_id = $user['user_id'];

        $getProducts = UserWishList::where('user_id', $user_id)->where('language_id', $request['language_id'])->groupBy('product_id')->select('product_id')->get()->toArray();

        $data = Products::where('language_id', $request['language_id'])->whereIn('product_id', $getProducts)->where('status', 'active')->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Wishlist Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }

    /**
     * Search Products
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchProduct(Request $request)
    {
        $rule = [
            'language_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Products::where('language_id', $request['language_id'])->where('status', 'active');
        if ($request['search']) {
            $data = $data->where('product_name', 'LIKE', "%{$request['search']}%");
        }
        $data = $data->orderBy('id', 'DESC')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }
            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }

    /**
     * Get Colours
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getColours(Request $request)
    {

        $rule = [
            'language_id' => 'required'
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = ProductColours::leftjoin('products', 'products.id', '=', 'product_colours.product_id')
            ->where('products.language_id', $request['language_id'])
            ->select('product_colours.colour_name', 'product_colours.id')
            ->groupBy('product_colours.colour_name')->get();

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Colour get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }

    /**
     * Product Sorting
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sorting(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'sorting_type' => 'required',
            'subcategory_id' => 'required',
        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }
        $soting = $request['sorting_type'];

        $data = Products::where('language_id', $request['language_id'])->where('subcategory_id',$request['subcategory_id'])->where('status', 'active');
        if ($soting == 'what-new') {
            $data = $data->orderBy('id', 'DESC');
        }
        if ($soting == 'high-low') {
            $data = $data->orderBy('price', 'DESC');
        }

        if ($soting == 'low-high') {
            $data = $data->orderBy('price', 'ASC');
        }

        if ($soting == 'discount') {
            $data = $data->where('discount_value', '!=', null)->orderBy('id', 'DESC');
        }

        $data = $data->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }

            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }


    }

    public function filter(Request $request)
    {
        $rule = [
            'language_id' => 'required',
            'subcategory_id' => 'required',

        ];

        $message = [
            'language_id.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $price = $request['price'];
        $size = [];
        if (!empty($request['size'])) {

            $size = explode(',', $request['size']);
        }
        $colour = [];
        if (!empty($request['colour'])) {

            $colour = explode(',', $request['colour']);
        }
        $category = [];
        if (!empty($request['category'])) {

            $category = explode(',', $request['category']);
        }

        $data = Products::leftjoin('product_colours', 'product_colours.product_id', '=', 'products.id')
            ->leftjoin('product_sizes', 'product_sizes.product_id', '=', 'products.id')
            ->where('products.subcategory_id',$request['subcategory_id']);
        if (!empty($price)) {
            $data = $data->where('products.price', '<=', $price);
        }

        if (count($size) > 0) {
            $data = $data->whereIn('product_sizes.size', $size)->where('product_sizes.qty', '!=', 0);
        }

        if (count($colour) > 0) {
            $data = $data->whereIn('product_colours.id', $colour);
        }

        if (count($category) > 0) {
            $data = $data->whereIn('products.category_id', $category);
        }

        $data = $data->select('products.*')->distinct('products.id')->paginate(10);

        foreach ($data as $row) {
            $productImage = ProductColours::where('product_id', $row->id)->get();
            foreach ($productImage as $item) {
                $item->image = config('app.asset_url') . '/storage/app/public/colour/' . $item->colour_image;
                $item->size = ProductSize::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                $item->product_image = ProductImage::where('product_id', $row->id)->where('colour_id', $item->id)->get();
                foreach ($item->product_image as $v) {
                    $v->image = config('app.asset_url') . '/storage/app/public/product/' . $v->image;
                }
            }
            $image = ProductImage::where('product_id', $row->id)->where('type', 'image')->value('image');
            if (!empty($image)) {
                $row->image = config('app.asset_url') . '/storage/app/public/product/' . $image;
            } else {
                $row->image = config('app.asset_url') . '/storage/app/public/default/default.png';
            }
            $row->colour = $productImage;
            $video = ProductImage::where('product_id', $row->id)->where('type', 'video')->value('image');
            if (!empty($video)) {
                $row->video = config('app.asset_url') . '/storage/app/public/product/video/' . $video;
            } else {
                $row->video = '';
            }

            $row->sizes = ProductSize::where('product_id', $row->id)->groupBy('size')->get();
            $row->collection_name = @$row->CollectionData->name;
            $row->category_name = @$row->CategoryData->name;
            $row->subcategory_name = @$row->SubcategoryData->name;
            $row->fit_name = @$row->FitData->name;
            $row->style_name = @$row->StyleData->name;
            $row->nack_style_name = @$row->NackStyleData->name;
            $row->sleeve_style_name = @$row->SleeveStyleData->name;
            $row->final_price = \BaseFunction::finalPrice($row->price, $row->discount_type, $row->discount_value);
        }

        if (count($data) > 0) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }

    }
}
