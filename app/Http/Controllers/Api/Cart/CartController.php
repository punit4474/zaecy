<?php

namespace App\Http\Controllers\Api\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Discount;
use App\Models\Orders;
use App\Models\Products;
use App\Models\ProductImage;
use App\Models\ProductColours;
use Illuminate\Http\Request;
use URL;
use Validator;
use Mail;
use File;


class CartController extends Controller
{
    public function couponValidation(Request $request)
    {
        $rule = [
            'coupon_code' => 'required',
        ];

        $message = [
            'coupon_code.required' => 'Language is required',
        ];

        $validate = Validator::make($request->all(), $rule, $message);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $date = \Carbon\Carbon::now()->format('Y-m-d');
        $couponData = Discount::where('discount_code', $request['coupon_code'])->first();


        if (empty($couponData)) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Invalid Coupon', 'ResponseData' => []], 400);
        }
        $user = $request['user'];
        $user_id = $user['user_id'];

        $userCouponCodes = Orders::where('discount_code',$couponData['discount_code'])->where('order_status','!=','cancel')->count();
        if ($couponData['max_use_value'] > $userCouponCodes && $date > $couponData['discount_start_date'] && $date < $couponData['discount_end_date']) {
            $checkUser = Orders::where('user_id',$user_id)->where('discount_code',$couponData['discount_code'])->first();
            if(!empty($checkUser)){
              return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Coupon Already use by this user', 'ResponseData' => []], 400);  
            }
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Valid Coupon', 'ResponseData' => $couponData], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Invalid Coupon', 'ResponseData' => []], 400);
        }

    }

    public function addToCart(Request $request)
    {
        $rule = [
            'product_id' => 'required',
            'colour_id' => 'required',
            'product_sku' => 'required',
            'variant_sku_id' => 'required',
            'size' => 'required',
            'qty' => 'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = $request->all();

        $checkProduct = Cart::where('user_id', $user_id)
            ->where('product_id', $data['product_id'])
            ->where('colour_id', $data['colour_id'])
            ->where('variant_sku_id', $data['variant_sku_id'])
            ->first();

        if(!empty($checkProduct)){
            $newQty = (int)$checkProduct['qty'] + $data['qty'];
            $update = Cart::where('user_id', $user_id)
                ->where('product_id', $data['product_id'])
                ->where('colour_id', $data['colour_id'])
                ->where('variant_sku_id', $data['variant_sku_id'])
                ->update(['qty'=>$newQty]);
            if($update){
                $addCart = Cart::where('user_id', $user_id)
                    ->where('product_id', $data['product_id'])
                    ->where('colour_id', $data['colour_id'])
                    ->where('variant_sku_id', $data['variant_sku_id'])->first();

                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Added into Cart', 'ResponseData' => $addCart], 200);
            } else {
                return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
            }
        } else {
            $data['user_id'] = $user_id;

            $addCart = new Cart();
            $addCart->fill($data);
            if($addCart->save()){
                return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Added into Cart', 'ResponseData' => $addCart], 200);
            } else {
                return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
            }
        }

    }

    public function updateCart(Request $request){
        $rule = [
            'qty' => 'required',
            'cart_id'=>'required'
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data['qty'] = $request['qty'];

        $update = Cart::where('id',$request['cart_id'])->update($data);
        if($update){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Cart Data Updated', 'ResponseData' => []], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }
    }

    public function removeCart(Request $request){
        $rule = [
            'cart_id'=>'required'
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $remove = Cart::where('id',$request['cart_id'])->delete();

        if($remove){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Removed From Cart', 'ResponseData' => []], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }

    }

    public function getCartProduct(Request $request){
        $user = $request['user'];
        $user_id = $user['user_id'];

        $cart = Cart::where('user_id',$user_id)->get();

        foreach ($cart as $row){

            $productData = Products::where('language_id', $request['language_id'])->where('product_id', $row->product_sku)
                            ->where('status','active')->first();
            
            $row->product_name = @$productData->product_name;

            $colourData = ProductColours::where('product_id',$productData->id)->first();

            $row->colour_name = @$colourData->colour_name;
            
            $row->price = \BaseFunction::finalPrice(@$productData->price, @$productData->discount_type, @$productData->discount_value);
            
            $image = ProductImage::where('product_id',$productData->id)->where('colour_id',$colourData->id)->value('image');
            if(!empty($image)){
                $row->images = config('app.asset_url').'/storage/app/public/product/'.$image;
            } else {
                $row->images =  config('app.asset_url').'/storage/app/public/default/default.png';
            }
        }

        if(count($cart)>0){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Product Cart data', 'ResponseData' => $cart], 200);
        }else {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }

    }
}
