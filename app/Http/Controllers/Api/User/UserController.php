<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\Slider;
use URL;
use Validator;
use Mail;
use File;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = User::where('id',$user_id)->first();

        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'User Get Successful', 'ResponseData' => $data], 200);
    }

    public function editProfile(Request $request){
        $rule = [
            'name' => 'required',
            'email'=>'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = $request->all();
        $data = $request->except('user');

        if ($request->file('profile_pic')) {

            $oldimage = User::where('id', $user_id)->value('profile_pic');
            if (!empty($oldimage)) {
                File::delete('storage/app/public/user/' . $oldimage);
            }

            $file = $request->file('profile_pic');
            $filename = 'user-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/user', $filename);
            $data['profile_pic'] = $filename;
        }

        $update = User::where('id', $user_id)->update($data);
        if ($update) {
            $data = User::where('id', $user_id)->first();
            $data['profile_pic'] = config('app.asset_url').'/storage/app/public/user/' . $data['profile_pic'];

            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Update Success.', 'ResponseData' => $data], 200);
        }
    }

    public function getAllAddress(Request $request)
    {
        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = UserAddress::where('user_id',$user_id)->get();

        if(count($data) > 0){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'User Address Get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'No Data found', 'ResponseData' => []], 200);
        }
    }

    public function addAddress(Request $request)
    {
        $rule = [
            'address_type' => 'required',
            'address_line1'=>'required',
            'city'=>'required',
            'postal_code'=>'required',
            'address_name'=>'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = $request['user'];
        $user_id = $user['user_id'];
        $data = $request->all();
        $data['user_id'] = $user_id;

        $addAddress = new UserAddress();
        $addAddress->fill($data);
        if($addAddress->save()){
            if(!empty($request['default'])){
                $updateAddress = UserAddress::where('user_id',$user_id)->where('id','!=',$addAddress->id)->update(['default'=>null]);
            }
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Address Add Successful', 'ResponseData' => $addAddress], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }


    }

    public function editAddress(Request $request)
    {
        $rule = [
            'address_id'=>'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = UserAddress::where('id',$request['address_id'])->first();

        if(!empty($data)){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Address get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }
    }

    public function updateAddress(Request $request)
    {
        $rule = [
            'address_type' => 'required',
            'address_line1'=>'required',
            'city'=>'required',
            'postal_code'=>'required',
            'address_id'=>'required',
            'address_name'=>'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = $request->all();
        $data = $request->except('user','address_id');

        $update = UserAddress::where('id',$request['address_id'])->update($data);

        if($update){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Address updated Successful', 'ResponseData' => []], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }
    }

    public function removeAddress(Request $request)
    {
        $rule = [
            'address_id'=>'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = UserAddress::where('id',$request['address_id'])->delete();

        if($data){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Address removed Successful', 'ResponseData' => []], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }
    }

    public function defaultAddress(Request $request)
    {
        $rule = [
            'address_id'=>'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $user = $request['user'];
        $user_id = $user['user_id'];

        $data = UserAddress::where('user_id',$user_id)->update(['default'=>'']);
        $default = UserAddress::where('id',$request['address_id'])->update(['default'=>'yes']);

        if($default) {
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Address Default Set Successful', 'ResponseData' => []], 200);
        }else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Something Went Wrong', 'ResponseData' => []], 400);
        }

    }

    public function getSlider(Request $request)
    {
        $rule = [
            'type'=>'required',
            'language_id'=>'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'Validation Fails', 'ResponseData' => $validate->errors()->all()], 422);
        }

        $data = Slider::where('type',$request['type'])->where('language_id',$request['language_id']);

        if(!empty($request['collection_id'])){
            $data = $data->where('category_id',$request['collection_id']);
        } else {
            $data = $data->where('category_id',null);
        }
        $data = $data->where('status','active')->get();

        foreach($data as $row){
            $row['image'] = config('app.asset_url') . '/storage/app/public/slider/'.$row['image'];
        }

        if(count($data)> 0){
            return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Slider get Successful', 'ResponseData' => $data], 200);
        } else {
            return response()->json(['ResponseCode' => 0, 'ResponseText' => 'No data Found', 'ResponseData' => []], 200);
        }
    }
}
