<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\Payment;
use App\Models\ProductColours;
use App\Models\ProductImage;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\User;
use Illuminate\Http\Request;
use Mail;


class PaymentController extends Controller
{
    public function index(Request $request)
    {
        $order_id = decrypt($request['payment_token']);

        $data = \BaseFunction::paymentCheck($order_id);
        $orderData = Orders::where('order_id', $order_id)->first();
        if (!empty($orderData)) {
            if ($orderData['order_status'] == 'pending') {
                if ($data->result == 'SUCCESS') {
                    $trans = $data->transaction;
                    $count = count($trans);
                    $transactionId = $trans[$count - 1];

                    $update = Orders::where('id', $orderData['id'])->update(['order_status' => 'confirm']);
                    $payment = Payment::where('order_id', $orderData['id'])->update(['payment_status' => 'success', 'payment_details' => json_encode($data), 'payment_id' => $transactionId->authorizationResponse->transactionIdentifier]);
                    $cart = Cart::where('user_id', $orderData['user_id'])->delete();
                    $orderProduct = OrderProducts::where('order_id', $orderData['id'])->get();
                    foreach ($orderProduct as $rw) {
                        $qty = ProductSize::where('product_id', $rw->product_id)->where('colour_id', $rw->product_colour)->where('sku', $rw->product_size_sku)->value('qty');
                        $newQty = (int)$qty - (int)$rw->qty;
                        $getchild = Products::where('parent_id', $rw->product_id)->value('id');
                        $updateQTY = ProductSize::where('product_id', $rw->product_id)->where('colour_id', $rw->product_colour)->where('sku', $rw->product_size_sku)->update(['qty' => $newQty]);
                        $updateChildQTY = ProductSize::where('product_id', $getchild)->where('colour_id', $rw->product_colour)->where('sku', $rw->product_size_sku)->update(['qty' => $newQty]);
                    }

                    $check_user = User::where('id', $orderData['user_id'])->first();

                    $name = $check_user['first_name'] . ' ' . $check_user['last_name'];
                    $title = 'We got your order, ' . $name . '!';
                    $data = ['title' => $title, 'email' => $check_user['email'], 'name' => $name];

                    try {
                        Mail::send('email.confirm', $data, function ($message) use ($data) {
                            $message->from('notification@zaecy.com', env('MAIL_FROM_NAME', "Zaecy"))->subject($data['title']);
                            $message->to($data['email']);
                        });


                    } catch (\Swift_TransportException $e) {
                        \Log::debug($e);
                    }

                }
//                return redirect('https://zstage.zaecy.com?paymentStatus=DONE');
                return redirect('https://zstage.zaecy.com/order-tracking/' . $order_id);

            } else {
                dd('failed');
            }


        }

    }

    public function error(Request $request)
    {
        $order_id = decrypt($request['payment_token']);
        $orderData = Orders::where('order_id', $order_id)->first();
        if (!empty($orderData) && $orderData['order_status'] == 'pending') {
            $removeOrderProduct = OrderProducts::where('order_id', $orderData['id'])->delete();
            $payment = Payment::where('order_id', $orderData['id'])->delete();
            $order = Orders::where('id', $orderData['id'])->delete();
//            return redirect('https://zstage.zaecy.com?paymentStatus=CANCELLED');
            return redirect('https://zstage.zaecy.com/order-tracking/' . $order_id);

        }
    }

    public function cancel(Request $request)
    {

        $order_id = decrypt($request['payment_token']);
        $orderData = Orders::where('order_id', $order_id)->first();
        if (!empty($orderData) && $orderData['order_status'] == 'pending') {
            $removeOrderProduct = OrderProducts::where('order_id', $orderData['id'])->delete();
            $payment = Payment::where('order_id', $orderData['id'])->delete();
            $order = Orders::where('id', $orderData['id'])->delete();
            return redirect('https://zstage.zaecy.com/order-tracking/' . $order_id);
//            return redirect('https://zstage.zaecy.com?paymentStatus=CANCELLED');
        }
    }

    public function paymentProcced(Request $request)
    {
//        dd($request->all());
        $order_id = decrypt($request['payment_token']);
        $oid = $request['payment_token'];
        $session_id = decrypt($request['session_token']);
        $orderData = Orders::where('order_id', $order_id)->first();
        return view('payment', compact('oid', 'session_id', 'orderData'));
    }


}
