<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('order_id')->nullable();
            $table->string('total_amount')->nullable();
            $table->string('discount_code')->nullable();;
            $table->string('discount_amount')->nullable();;
            $table->string('payment_id')->nullable();;
            $table->enum('order_status',['pending','confirm','packing','ready_to_dispatch','onway','delivered','cancel','return'])->default('pending');;
            $table->string('airway_tracking_number')->nullable();
            $table->string('address_name')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('return_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
