<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone_number')->nullable();
            $table->string('social_type')->nullable();
            $table->string('social_id')->nullable();
            $table->enum('status',['active','inactive'])->default('active');
            $table->string('otp')->nullable();
            $table->enum('otp_verify',['yes','no'])->default('no');
            $table->text('device_token')->nullable();
            $table->text('device_id')->nullable();
            $table->string('device_type')->nullable();
            $table->bigInteger('user_role')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
