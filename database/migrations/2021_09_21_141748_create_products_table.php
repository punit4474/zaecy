<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('language_id');
            $table->bigInteger('collection_id')->nullable();
            $table->bigInteger('category_id')->nullable();
            $table->bigInteger('subcategory_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('product_name')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('gender')->nullable();
            $table->text('material_composition')->nullable();
            $table->string('material_type')->nullable();
            $table->string('country_region')->nullable();
            $table->string('model_fit')->nullable();
            $table->string('weight')->nullable();
            $table->string('fit')->nullable();
            $table->string('style')->nullable();
            $table->string('nack_style')->nullable();
            $table->string('sleeve_style')->nullable();
            $table->string('price')->nullable();
            $table->string('discount_type')->nullable();
            $table->string('discount_value')->nullable();
            $table->text('hashtag')->nullable();
            $table->enum('status',['active','draft','inactive','pending'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
