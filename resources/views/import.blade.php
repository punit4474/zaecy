
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/collections')}}">Collections</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Collections</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Create Collections</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Create Collections Form</h4>

                    <hr/>

                    {{Form::open(array('url'=>'import','method'=>'post','name'=>'create-collections','files'=>'true','class'=>'needs-validation','novalidate'))}}

                    <div class="row">

                        <div class="col-lg-4">

                            <div class="form-group mb-3">
                                <label for="validationCustom01">Image</label>
                                {{Form::file('image',array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'image'))}}
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                    </div>

                    {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('zaecy-admin/collections')}}" class="btn btn-danger" >Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>

