<html>
<head>
    <title>EXCHANGES - Zaecy</title>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/bootstrap.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/icons.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/app.min.css'}}" rel="stylesheet"
          type="text/css"/>
</head>
<body>
<div class="container">
    <h2>EXCHANGES</h2>
    <br/>
    <p><span style="font-weight: 400;">In order to provide you with a hassle-free shopping experience, you can return your items absolutely free for Store Credit. You can then use your store credit to purchase new items.</span>
    </p>
    <p><span style="font-weight: 400;">If you place an order and you are not satisfied with the products received, you can return your items free of charge. Please make sure the following return policy guidelines are met:</span>
    </p>
    <p><span style="font-weight: 400;">Items are packaged back in original packaging with labels and tags on.</span></p>
    <p><span style="font-weight: 400;">Items must be unworn &amp; unwashed (no distinct odors, blemishes, signs of wear, etc.)</span>
    </p>
    <p><span
            style="font-weight: 400;">We cannot accept returns on masks, underwear, or socks for hygiene reasons.</span>
    </p>
    <p><span style="font-weight: 400;">Please use a Standard Shipping Pouch or Flyer with a weight limit of 2 kg and size (40&times;30 cm) for returning the items.</span>
    </p>
    <p><span style="font-weight: 400;">Once the exchange request is received and accepted, we will send you a prepaid/free shipping label to return the items.</span>
    </p>
    <p><span style="font-weight: 400;">After the return items are received in the warehouse and pass the quality check, we will issue you a store credit to shop for another item of the same value from our web store.</span>
    </p>
    <p><span style="font-weight: 400;">You may either use the store credit coupon to order the size that suits you or you can order another item of your choice by paying the difference if any.</span>
    </p>
    <p><span style="font-weight: 400;">Store Credit is valid for 12 months from the date of issue.</span></p>
    <p><span style="font-weight: 400;">The items you will purchase with the store credit might be subject to shipping fees at the checkout depending on your order value.&nbsp;</span>
    </p>
    <p><span style="font-weight: 400;">All discounts 20% and above are final sales. If you have any size questions, you can check our size guide page by clicking here for men and clicking here for women. You can also reach out to our customer service team for guidance.</span>
    </p>
    <p><span style="font-weight: 400;">Only the first return label of any item is free, to get a 2nd replacement for the same order, an additional shipping fee will be charged.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>REFUNDS</strong></p>
    <p><span style="font-weight: 400;">You can return your items up to 14 days after the delivery date.</span></p>
    <p><span style="font-weight: 400;">We have tried our best to make our return process absolutely hassle-free and smooth for you. Once the return request is received and accepted, we will send you a discounted and trackable prepaid return shipping label to return the items.</span>
    </p>
    <p><span style="font-weight: 400;">To avoid any inconvenience of dropping off the return to the courier company, we will also send a courier person to your pickup address for return pickup. Just keep your return items and return shipping label ready.</span>
    </p>
    <p><span style="font-weight: 400;">Return shipping labels are free for the United Arab Emirates, USD 10 for the USA, EUR 15 for Europe, GBP 15 for the UK, SAR 25 for KSA, and USD 15 or equivalent for the rest of the world.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Refunds:</span></p>
    <p><span style="font-weight: 400;">If you are returning a product, we will refund you the price of the original order excluding the return shipping label fee (as detailed above) within 7-14 business days after we receive the product in a proper condition at our warehouse.</span>
    </p>
    <p><span
            style="font-weight: 400;">We reserve the right to return any item that does not comply with our return policy.</span>
    </p>
    <p><span style="font-weight: 400;">Cash-on-delivery orders are not eligible for refunds; a store credit of equal value will be reserved instead.</span>
    </p>
    <p><span style="font-weight: 400;">Except for Black Friday or Birthday Sale, all discounts 20% and above are final sales. If you have any size questions, you can check our size guide page by clicking here for men and clicking here for women. You can also reach out to our customer service team for guidance.</span>
    </p>
    <p><span style="font-weight: 400;">You can request for a refund to your original mode of payment by submitting your request to info@zaecy.com</span>
    </p>
    <p><span style="font-weight: 400;">We cannot accept returns on masks, underwear or socks for hygiene reasons.</span>
    </p>
</div>
</body>
</html>
