@extends('layouts.admin')
@section('title')
    Coupons
@endsection
@section('css')
    <!-- plugin css -->
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.css'}}" rel="stylesheet" type="text/css" />
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.css'}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right">
                <a href="{{URL::to('zaecy-admin/discount/create')}}" class="btn btn-primary text-white">+ Create Coupon</a>
            </nav>
            <h4 class="mb-1 mt-1">Coupons</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total Quantity Sold</span>
                            <h2 class="mb-0">{{number_format($totalOrder,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total Revenue</span>
                            <h2 class="mb-0">AED {{number_format($totalRevenue,1)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Orders Fulfilled</span>
                            <h2 class="mb-0">{{number_format($totalFulfilled,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Orders Pending Dispatch</span>
                            <h2 class="mb-0">{{number_format($totalUnFulfilled,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                {!! Session::get('message') !!}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Coupons</h4>
                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Coupon Code</th>
                            <th>Discount (Type)</th>
                            <th>Discount Value</th>
                            <th>Coupon Start Date</th>
                            <th>Coupon End Date</th>
                            <th>Max Use</th>
                            <th>Used Coupon</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->discount_code == '' ? '-': $row->discount_code}}</td>
                                <td>{{$row->discount_type == '' ? '-': $row->discount_type}}</td>
                                <td>{{$row->discount_value == '' ? '-': $row->discount_value}}</td>
                                <td>{{$row->discount_start_date == '' ? '-': $row->discount_start_date}}</td>
                                <td>{{$row->discount_end_date == '' ? '-': $row->discount_end_date}}</td>
                                <td>{{$row->max_use_value == '' ? '-': $row->max_use_value}}</td>
                                <td>{{$row->used == '' ? '-': $row->used}}</td>
                                <td>
                                    <a class="" href="{{URL::to('zaecy-admin/discount/'.$row->id.'/edit')}}"> <i class="uil-pen"></i></a>
                                    <a class="" href="{{URL::to('zaecy-admin/discount/'.$row->id.'/destroy')}}"><i class="uil-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- datatable js -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/jquery.dataTables.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.responsive.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.buttons.min.js'}}"></script>

@endsection
@section('js')
    <!-- Datatables init -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/datatables.init.js'}}"></script>

@endsection
