@extends('layouts.admin')
@section('title')
    Create Coupon
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/discount')}}">Coupon</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Coupon</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Create Coupon</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Create Coupon Form</h4>

                    <hr/>

                    {{Form::open(array('url'=>'zaecy-admin/discount','method'=>'post','name'=>'create-discount','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <div class="row">

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Name</label>
                                {{Form::text('name','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Name','required'))}}
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Coupon Code</label>
                                {{Form::text('discount_code','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Coupon Code','required'))}}
                                @error('discount_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Discount (Type)</label>
                                {{Form::select('discount_type',array(''=>'select Discount Type','amount'=>'Amount','percentage'=>'Percentage'),'',array('class'=>'form-control discount_type','id'=>'validationCustom04','required'))}}
                                @error('discount_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Discount Value</label>
                                {{Form::text('discount_value','',array('class'=>'form-control discount_value','id'=>'validationCustom04','placeholder'=>'Discount Value','required'))}}
                                @error('discount_value')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Coupon Start Date</label>
                                {{Form::date('discount_start_date','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Coupon Start Date'))}}
                                @error('discount_start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Coupon End Date</label>
                                {{Form::date('discount_end_date','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Coupon End Date'))}}
                                @error('discount_end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Max Use of Coupon</label>
                                {{Form::number('max_use_value','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Max Use of Coupon','min'=>'0'))}}
                                @error('max_use_value')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                    </div>

                    {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('zaecy-admin/discount')}}" class="btn btn-danger">Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/form-validation.init.js'}}"></script>
    <script>
        $(document).on('change','.discount_type',function (){
            var value = $(this).val();
            if(value == 'amount'){

            } else if(value == 'percentage'){

            }
        });
    </script>
@endsection

