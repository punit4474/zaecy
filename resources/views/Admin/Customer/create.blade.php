@extends('layouts.admin')
@section('title')
    Create Customer
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/customers')}}">Customers</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Customer</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Create Customer</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Create Customer Form</h4>

                    <hr/>

                    {{Form::open(array('url'=>'zaecy-admin/customers','method'=>'post','name'=>'create-Customer','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <h5>Customer Details</h5>
                    <hr/>
                    <div class="row">

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Name</label>
                                {{Form::text('name','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Name','required'))}}
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Email</label>
                                {{Form::text('email','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Email','required'))}}
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Phone Number</label>
                                {{Form::text('phone_number','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Phone Number','required'))}}
                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <h5>Customer Address</h5>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Address Name</label>
                                {{Form::text('address_name','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Address Name','required'))}}
                                @error('address_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Address1</label>
                                {{Form::text('address_line1','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Address 1','required'))}}
                                @error('address_line1')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Address2</label>
                                {{Form::text('address_line2','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Address 2'))}}
                                @error('address_line2')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">City</label>
                                {{Form::text('city','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'City','required'))}}
                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Postal Code</label>
                                {{Form::text('postal_code','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Postal Code','required'))}}
                                @error('postal_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Address Type</label>
                                {{Form::select('address_type',array(''=>'Select Address TYpe','home'=>'Home','office'=>'Office','other'=>'Other'),'',array('class'=>'form-control','id'=>'validationCustom03','required'))}}
                                @error('address_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>

                    {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('zaecy-admin/customers')}}" class="btn btn-danger">Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/form-validation.init.js'}}"></script>
    <script>
        $(document).on('change','.language_id',function (){
            var value = $(this).val();
            getCollection(value);
        });

        function getCollection(language_id){
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-collection')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Collection</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.collection_id').html(html);
                },
                error: function (error) {
                }
            });
        }
    </script>
@endsection

