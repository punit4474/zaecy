@extends('layouts.admin')
@section('title')
    Customers
@endsection
@section('css')
    <!-- plugin css -->
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.css'}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.css'}}"
          rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right">
                <a href="{{URL::to('zaecy-admin/customers/create')}}" class="btn btn-primary text-white">+ Create
                    Customer</a>
            </nav>
            <h4 class="mb-1 mt-1">Customers</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                {!! Session::get('message') !!}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-xl-2">

        </div>
        <div class="col-md-3 col-xl-2">

        </div>
        <div class="col-md-3 col-xl-2">

        </div>
        <div class="col-md-3 col-xl-2">

        </div>
        <div class="col-md-3 col-xl-2">
            <div class="form-group mb-3">
                {{Form::select('sorting',array(''=>'Select Sorting','last_update_new'=>'Last Update (Newest First)','last_update_old'=>'Last Update (Oldest First)','high'=>'Amount Spend (High to Low)',
                    'low'=>'Amount Spend (Low to High)','order_high'=>'Total Orders (High to Low)','order_low'=>'Total Orders (Low to High)','last_order_new'=>'Last Order Date (newest first)','last_order_old'=>'Last Order Date (Oldest First)',
                    'date_new_customer'=>'Date added as a customer (Newest First)','date_old_customer'=>'Date added as a customer (Oldest First)'),
                    $type,array('class'=>'form-control sorting','id'=>'validationCustom04','required'))}}
                @error('sorting')
                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                @enderror
            </div>

        </div>
        <div class="col-md-3 col-xl-2">
            <div class="form-group mb-3 ">
                <a href="javascript:void(0)" class="btn btn-primary filter">Filter</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Customers</h4>
                    <table id="basic-datatable1" class="table dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Orders</th>
                            <th>Order Units</th>
                            <th>REVENUE (AED)</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                            {{--                            <th>Action</th>--}}
                        </tr>
                        </thead>
                        <tbody class="tbl_body1">
                        <div style="display: none">{{$i=1}}</div>
                        @foreach($data as $row)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$row['name']}}</td>
                                <td>{{$row['orders']}}</td>
                                <td>{{$row['product_qty']}}</td>
                                <td>{{number_format($row['revenue'],1)}}</td>
                                <td>{{$row['email']}}</td>
                                <td>{{$row['phone_number']}}</td>
                                <td>{{@$row['address']['address_line1']}} {{@$row['address']['city']}}</td>

                            </tr>
                            <div style="display: none">{{$i++}}</div>
                        @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- datatable js -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/jquery.dataTables.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.responsive.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.buttons.min.js'}}"></script>

@endsection
@section('js')
    <!-- Datatables init -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/datatables.init.js'}}"></script>
    <script>
        $("#basic-datatable1").DataTable({
            language: {
                paginate: {
                    previous: "<i class='uil uil-angle-left'>",
                    next: "<i class='uil uil-angle-right'>"
                }
            }, drawCallback: function () {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
        var url = '{{URL::to('zaecy-admin/sub-admin')}}';
        $(document).on('click','.filter',function (){
            var value = $('.sorting').val();
            if(value != ''){
                window.location.href = url + '?filter='+value;
            }
        });
    </script>
@endsection
