<h4 class="header-title mt-0 mb-1">Customers</h4>
<table id="basic-datatable" class="table dt-responsive nowrap">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Orders</th>
        <th>REVENUE (AED)</th>
        {{--                            <th>Action</th>--}}
    </tr>
    </thead>
    <tbody class="tbl_body1">

    @foreach($data as $row)
        <tr>
            <td>{{$row['id']}}</td>
            <td>{{$row['name']}}</td>
            <td>{{$row['orders']}}</td>
            <td>{{number_format($row['revenue'],1)}}</td>
            {{--                                <td>--}}
            {{--                                    <a class="" href="{{URL::to('zaecy-admin/customers/'.$row->id.'/edit')}}"> <i class="uil-pen"></i></a>--}}
            {{--                                    <a class="" href="{{URL::to('zaecy-admin/customers/'.$row->id.'/destroy')}}"><i class="uil-trash-alt"></i></a>--}}
            {{--                                </td>--}}
        </tr>
    @endforeach

    </tbody>
</table>
