@extends('layouts.admin')
@section('title')
    Bulk Upload
@endsection
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/products')}}">Products</a></li>
                <li class="breadcrumb-item active" aria-current="page">Bulk Upload</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Bulk Upload</h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-1">Bulk Upload</h4>

                <hr/>

                {{Form::open(array('url'=>'zaecy-admin/product/import','method'=>'post','name'=>'create-products','files'=>'true','class'=>'needs-validation','novalidate'))}}

                <div class="row">

                    <div class="col-lg-4">

                        <div class="form-group mb-3">
                            <label for="validationCustom01">File</label>
                            {{Form::file('image',array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'image','required'))}}
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                </div>

                {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                <a href="{{URL::to('zaecy-admin/products')}}" class="btn btn-danger" >Cancel</a>
                {{Form::close()}}

            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
</div>

@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
@endsection
