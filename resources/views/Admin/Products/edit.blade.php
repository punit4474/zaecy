@extends('layouts.admin')
@section('title')
    Edit Product
@endsection
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <style>
        .img-delete {
            position: relative;
            width: 70px;
            height: 70px;
        }

        .img-delete a {
            position: absolute;
            top: 4px;
            right: 4px;
            background: red;
            color: #fff;
            width: 18px;
            height: 18px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 100%;
            font-size: 9px;
            font-weight: 900;
        }

        .img-delete img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/products')}}">Products</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Product</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Edit Product</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Edit Product Form</h4>

                    <hr/>

                    {{Form::open(array('url'=>'zaecy-admin/products/'.$data['id'],'method'=>'put','name'=>'edit-Product','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <h5>Product Organisation</h5>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Language</label>
                                {{Form::select('language_id',array(''=>'Select Language','1'=>'English','2'=>'Arabic'),$data['language_id'],array('class'=>'form-control language_id','id'=>'validationCustom04','required'))}}
                                @error('language_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Collection Name</label>
                                {{Form::select('collection_id',$collection,$data['collection_id'],array('class'=>'form-control collection_id','id'=>'validationCustom04','required'))}}
                                @error('collection_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Category Name</label>
                                {{Form::select('category_id',$category,$data['category_id'],array('class'=>'form-control category_id','id'=>'validationCustom04','required'))}}
                                @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Sub Category Name</label>
                                {{Form::select('subcategory_id',$subcategory,$data['subcategory_id'],array('class'=>'form-control subcategory_id','id'=>'validationCustom04','required'))}}
                                @error('subcategory_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <h5>Product Identification</h5>
                    <hr/>
                    <div class="row">

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Brand Name</label>
                                {{Form::text('brand_name',$data['brand_name'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Brand Name','required'))}}
                                @error('brand_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Product Name</label>
                                {{Form::text('product_name',$data['product_name'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Product Name','required'))}}
                                @error('product_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Product ID</label>
                                {{Form::text('product_id',$data['product_id'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Product ID','required'))}}
                                @error('product_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Gender</label>
                                {{Form::select('gender',array(''=>'Select Gender','male'=>'Male','female'=>'Female','other'=>'Others'),$data['gender'],array('class'=>'form-control','id'=>'validationCustom04'))}}
                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                       
                    </div>
                    <h5>Product Specifications</h5>
                    <hr/>
                    <div class="row">

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Material composition</label>
                                {{Form::text('material_composition',$data['material_composition'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Material composition','required'))}}
                                @error('material_composition')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Material type</label>
                                {{Form::text('material_type',$data['material_type'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Material type','required'))}}
                                @error('material_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Country of origin</label>
                                {{Form::text('country_region',$data['country_region'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Country of origin','required'))}}
                                @error('country_region')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                       
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Fabric Finish</label>
                                {{Form::text('fabric_finish',$data['fabric_finish'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Fabric Finish'))}}
                                @error('weight')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <h5>Style</h5>
                    <hr/>
                    <div class="row">

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Fit</label>
                                {{Form::select('fit',$fit,$data['fit'],array('class'=>'form-control fit','id'=>'validationCustom01'))}}
                                @error('fit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Style</label>
                                {{Form::select('style',$style,$data['style'],array('class'=>'form-control style','id'=>'validationCustom01'))}}
                                @error('style')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Neck Style</label>
                                {{Form::select('nack_style',$nackStyle,$data['nack_style'],array('class'=>'form-control nack_style','id'=>'validationCustom01'))}}
                                @error('nack_style')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Sleeve Type</label>
                                {{Form::select('sleeve_style',$sleeveStyle,$data['sleeve_style'],array('class'=>'form-control sleeve_style','id'=>'validationCustom01'))}}
                                @error('sleeve_style')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <h5>Colour Variants</h5>
                     <div style="display: none">{{$i = 0}}</div>
                           @if($i == '0')
                                    <div class="col-lg-3">
                                        <a href="javascript:void(0)" class="btn btn-primary add_new_colour"
                                           style="margin-top: 25px;">Add New +
                                        </a>
                                    </div>
                                @else
                                    <div class="col-lg-3">
                                        <a href="javascript:void(0)" class="btn btn-danger remove_colour"
                                           data-id="{{$row->id}}" style="margin-top: 25px;">Remove -
                                        </a>
                                    </div>
                                @endif
                    <hr/>
                    <div class="add_colors">


                        @foreach($productColour as $row)
                            <div class="row" @if($i != 0) style="margin-top: 20px" @endif>
                                <div class="col-lg-3">
                                    <div class="form-group mb-3">
                                        <label for="validationCustom01">Colour Name (Enter English name only*)</label>
                                        {{Form::text('colour_name[]',$row->colour_name,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Colour Name','required'))}}
                                        @error('colour_name')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group mb-3">
                                        <label for="validationCustom01">Image</label>
                                        {{Form::file('colour_image[]',array('class'=>'form-control','id'=>'validationCustom01'))}}
                                        @error('colour_image')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>
                                    @if(!empty($row->colour_image))
                                        <img
                                            src="{{config('app.asset_url').'/storage/app/public/colour/'.$row->colour_image}}"
                                            class="avatar rounded mr-3" alt="{{$row->colour_name}}">
                                    @endif
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group mb-3">
                                        <label for="validationCustom01">Product Images (Multiple Images) </label>
                                        {{Form::file('product_image['.$i.'][]',array('class'=>'form-control','id'=>'validationCustom01','multiple'))}}
                                        @error('product_image')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>
                                    @if(!empty($row->product_images))
                                        <div class="d-flex flex-wrap">
                                            @foreach($row->product_images as $item)
                                                <div class="m-2 img-delete">
                                                    <img
                                                        src="{{config('app.asset_url').'/storage/app/public/product/'.$item->image}}"
                                                        class="avatar rounded mr-3" alt="{{$row->colour_name}}">
                                                    <a href="{{URL::to('zaecy-admin/product/'.$data['id'].'/remove-image/'.$item->id)}}">X</a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                             @if($i != '0')
                                    <div class="col-lg-3">
                                        <a href="javascript:void(0)" class="btn btn-danger remove_colour"
                                           data-id="{{$row->id}}" style="margin-top: 25px;">Remove -
                                        </a>
                                    </div>
                                @else
                                <div class="col-lg-3"></div>
                                @endif
                                <div class="col-lg-2">
                                    <label for="validationCustom01">Size xs</label>
                                    {{Form::text('xs[]',$row->product_size[0]->qty,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Size XS Qty','required'))}}
                                    @error('colour_image')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-lg-2">
                                    <label for="validationCustom01">Size S</label>
                                    {{Form::text('s[]',$row->product_size[1]->qty,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Size S Qty','required'))}}
                                    @error('colour_image')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-lg-2">
                                    <label for="validationCustom01">Size M</label>
                                    {{Form::text('m[]',$row->product_size[2]->qty,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Size M Qty','required'))}}
                                    @error('m')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-lg-2">
                                    <label for="validationCustom01">Size L</label>
                                    {{Form::text('l[]',$row->product_size[3]->qty,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Size L Qty','required'))}}
                                    @error('colour_image')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-lg-2">
                                    <label for="validationCustom01">Size xl</label>
                                    {{Form::text('xl[]',$row->product_size[4]->qty,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Size XL Qty','required'))}}
                                    @error('xl')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>

                                <div class="col-lg-2">
                                    <label for="validationCustom01">Size xxl</label>
                                    {{Form::text('xxl[]',$row->product_size[5]->qty,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Size XXL Qty','required'))}}
                                    @error('xxl')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>

                                 <div class="col-lg-3">
                                    <div class="form-group mb-3">
                                <label for="validationCustom01">Product Quotation</label>
                                {{Form::text('product_quotation[]',$row->product_quotation,array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'Product Quotation','required'))}}
                                @error('product_quotation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Product Description</label>
                                {{Form::textarea('product_description[]',$row->product_description,array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'Product Description','required','rows'=>2))}}
                                @error('product_description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                         <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Model and fit</label>
                                {{Form::text('model_fit[]',$row->model_fit,array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Model and fit'))}}
                                @error('model_fit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                                <div style="display: none">{{$i++}}</div>
                                <input type="hidden" name="pro_id" class="pro_id" value="{{$i}}">
                                <input type="hidden" name="product_id[]" class="product_id" value="{{$row->id}}">
                            </div>

                        @endforeach
                    </div>
                    <h5>Product Pricing</h5>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Price</label>
                                {{Form::text('price',$data['price'],array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'Price','required'))}}
                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Discount (Type)</label>
                                {{Form::select('discount_type',array(''=>'select Discount Type','amount'=>'Amount','percentage'=>'Percentage'),$data['discount_type'],array('class'=>'form-control','id'=>'validationCustom04'))}}
                                @error('discount_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Discount Value</label>
                                {{Form::text('discount_value',$data['discount_value'],array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'Discount Value'))}}
                                @error('discount_value')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Hashtag</label>
                                {{Form::text('hashtag',$data['hashtag'],array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'Hashtag'))}}
                                @error('hashtag')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Page Category</label>
                                {{Form::select('page_category[]',$pageCategory,$selectedPageCategory,array('class'=>'form-control page_category','id'=>'validationCustom04','multiple'))}}
                                @error('page_category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Product Video</label>
                                {{Form::file('product_video',array('class'=>'form-control','id'=>'validationCustom01'))}}
                                @error('product_video')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            @if(!empty($data['video']))
                                <video width="320" height="240" class="avatar rounded mr-3" controls>
                                    <source
                                        src="{{config('app.asset_url').'/storage/app/public/product/video/'.$data['video']}}"
                                        type="video/mp4">
                                </video>
                            @endif
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Similar Product</label>
                                {{Form::select('similar_products[]',$simillarProduct,explode(',',$data['similar_products']),array('class'=>'form-control similar_products','id'=>'validationCustom05','multiple'))}}
                                @error('similar_products')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Recommended Products</label>
                                {{Form::select('recommended_products[]',$simillarProduct,explode(',',$data['recommended_products']),array('class'=>'form-control recommended_products','id'=>'validationCustom06','multiple'))}}
                                @error('recommended_products')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Product Status</label>
                                {{Form::select('status',[''=>'Select Status','active'=>'Active','draft'=>'Draft','inactive'=>'Inactive','pending'=>'Pending'],$data['status'],array('class'=>'form-control','id'=>'validationCustom06','required'))}}
                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                    </div>

                    {{Form::submit('Update',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('zaecy-admin/products')}}" class="btn btn-danger">Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script
        src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/form-validation.init.js'}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $('.page_category').select2();
        $('.similar_products').select2();
        $('.recommended_products').select2();

        $(document).on('change', '.language_id', function () {
            var value = $(this).val();
            getCollection(value);
            getFit(value);
            getStyle(value);
            getNackStyle(value);
            getSleeveType(value);
            getProduct(value);
        });

        $(document).on('change', '.collection_id', function () {
            var value = $(this).val();
            var language_id = $('.language_id').val();
            getCategory(language_id, value)
        });

        $(document).on('change', '.category_id', function () {
            var value = $(this).val();
            var language_id = $('.language_id').val();
            getSubCategory(language_id, value)
        });

        function getCollection(language_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-collection')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Collection</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.collection_id').html(html);
                },
                error: function (error) {
                }
            });
        }

        function getCategory(language_id, collection_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-category')}}",
                data: {
                    language_id: language_id,
                    collection_id: collection_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Category</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.category_id').html(html);
                },
                error: function (error) {
                }
            });
        }

        function getSubCategory(language_id, category_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-subcategory')}}",
                data: {
                    language_id: language_id,
                    category_id: category_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select SubCategory</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.subcategory_id').html(html);
                },
                error: function (error) {
                }
            });
        }

        function getFit(language_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-fit')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Fit</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.fit').html(html);
                },
                error: function (error) {
                }
            });
        }

        function getStyle(language_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-style')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Style</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.style').html(html);
                },
                error: function (error) {
                }
            });
        }

        function getNackStyle(language_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-nackstyle')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Nack Style</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.nack_style').html(html);
                },
                error: function (error) {
                }
            });
        }

        function getSleeveType(language_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-sleevestyle')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Sleeve Type</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.sleeve_style').html(html);
                },
                error: function (error) {
                }
            });
        }

        function getProduct(language_id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-product')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.product_id + "</option>";
                    });
                    $('.similar_products').html(html);
                    $('.recommended_products').html(html);
                    $('.similar_products').select2();
                    $('.recommended_products').select2();
                },
                error: function (error) {
                }
            });
        }

        $(document).on('click', '.add_new_colour', function () {
            var html = '';
            var id = $('.pro_id').val();
            var newId = parseInt(id) + 1;
            html += '<div class="row" style="margin-top: 20px">' +
                '<div class="col-lg-3">' +
                '<div class="form-group mb-3">' +
                '<label for="validationCustom01">Colour Name</label>' +
                '<input type="text" name="colour_name[]" class="form-control" id="validationCustom01" placeholder="Colour Name" required>' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-3">' +
                '<div class="form-group mb-3">' +
                '<label for="validationCustom01">Image</label>' +
                '<input type="file" name="colour_image[]" class="form-control" id="validationCustom01" required>' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-3">' +
                '<div class="form-group mb-3">' +
                '<label for="validationCustom01">Product Images (Multiple Images)</label>' +
                '<input type="file" name="product_image[' + newId + '][]" class="form-control" id="validationCustom01" multiple>' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-3">' +
                '<a class="btn btn-danger remove_colour" href="javascript:void(0)" data-id="" style="margin-top: 25px;">Remove - </a>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label for="validationCustom01">Size xs</label>' +
                '<input type="text" name="xs[]" class="form-control" id="validationCustom01" placeholder="Size XS Qty" required>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label for="validationCustom01">Size s</label>' +
                '<input type="text" name="s[]" class="form-control" id="validationCustom01" placeholder="Size S Qty" required>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label for="validationCustom01">Size M</label>' +
                '<input type="text" name="m[]" class="form-control" id="validationCustom01" placeholder="Size M Qty" required>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label for="validationCustom01">Size L</label>' +
                '<input type="text" name="l[]" class="form-control" id="validationCustom01" placeholder="Size L Qty" required>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label for="validationCustom01">Size XL</label>' +
                '<input type="text" name="xl[]" class="form-control" id="validationCustom01" placeholder="Size XL Qty" required>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label for="validationCustom01">Size XXL</label>' +
                '<input type="text" name="xxl[]" class="form-control" id="validationCustom01" placeholder="Size XXL Qty" required>' +
                '</div>'+
                        '<div class="col-lg-3">'+
                            '<div class="form-group mb-3">'+
                                '<label for="validationCustom01">Product Quotation</label>'+
                                '{{Form::text('product_quotation[]','',array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'Product Quotation','required'))}}'+
                                '@error('product_quotation')'+
                                '<span class="invalid-feedback" role="alert">'+
                                    '<strong>{{ $message }}</strong>'+
                                '</span>'+
                                '@enderror'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-3">'+
                            '<div class="form-group mb-3">'+
                                '<label for="validationCustom01">Product Description</label>'+
                                '{{Form::textarea('product_description[]','',array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'Product Description','required','rows'=>2))}}'+
                                '@error('product_description')'+
                                '<span class="invalid-feedback" role="alert">'+
                                    '<strong>{{ $message }}</strong>'+
                                '</span>'+
                               '@enderror'+
                            '</div>'+
                '</div>' +
                '<div class="col-lg-3">'+
                    '<div class="form-group mb-3">'+
                        '<label for="validationCustom01">Model and fit</label>'+
                        '{{Form::text('model_fit[]','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Model and fit'))}}'+
                        '@error('model_fit')'+
                        '<span class="invalid-feedback" role="alert">'+
                            '<strong>{{ $message }}</strong>'+
                        '</span>'+
                        '@enderror'+
                    '</div>'+
                '</div>'
                '</div>';

            $('.add_colors').append(html);
            $('.pro_id').val(newId);
        });

        $(document).on('click', '.remove_colour', function () {

            var ids = $(this).data('id');
            $(this).closest(".row").remove();
            console.log(ids);
            var id = $('.pro_id').val();
            var newId = id - 1;
            $('.pro_id').val(newId);
            console.log(id);
            console.log(newId);
            if (ids != '') {
                var product_id = '{{$data['id']}}';
                console.log(product_id);
                removeProduct(product_id, ids);
            }

        });

        function removeProduct(product_id, id) {
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/remove-product')}}",
                data: {
                    product_id: product_id,
                    colour_id: id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    if (response.status == 'success') {
                        return true;
                    }

                },
                error: function (error) {
                }
            });

        }

    </script>
@endsection

