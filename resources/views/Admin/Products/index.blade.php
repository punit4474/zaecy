@extends('layouts.admin')
@section('title')
    Products
@endsection
@section('css')
    <!-- plugin css -->
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.css'}}" rel="stylesheet" type="text/css" />
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.css'}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right">
                <a href="javascript:void(0)" class="btn btn-primary text-white remove_product">+ Remove  Products</a>
                <a href="{{URL::to('zaecy-admin/product/import')}}" class="btn btn-primary text-white">+ Import  Products</a>
                <a href="{{URL::to('zaecy-admin/products/create')}}" class="btn btn-primary text-white">+ Create  Products</a>
            </nav>
            <h4 class="mb-1 mt-1"> Products</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                {!! Session::get('message') !!}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1"> Products</h4>


                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Product ID</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Color</th>
                            <th>Language </th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data as $row)
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="{{$row->id}}" class="ids"></td>
                                <td>{{$row->id}}</td>
                                <td>{{@$row->product_id}}</td>
                                <td>{{$row->product_name}}</td>

                                <td>
                                        <img src="{{$row->image}}"
                                             class="avatar rounded mr-3" alt="{{$row->name}}">

                                </td>
                                <td>{{$row->color}}</td>
                                <td>{{$row->language_id == '1' ? 'English': 'Arabic'}}</td>

                                <td>

                                    <a href="javascript:void(0)"> <label
                                            class="badge badge-soft-{{$row->status == 'active' ? 'success' : 'danger'}}">{{$row->status}}</label>
                                    </a>


                                </td>

                                <td>
                                    <a class="" href="{{URL::to('zaecy-admin/products/'.$row->id.'/edit')}}"> <i class="uil-pen"></i></a>
                                    <a class="" href="{{URL::to('zaecy-admin/products/'.$row->id.'/destroy')}}"><i class="uil-trash-alt"></i></a>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- datatable js -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/jquery.dataTables.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.responsive.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.buttons.min.js'}}"></script>

@endsection
@section('js')
    <!-- Datatables init -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/datatables.init.js'}}"></script>
    <script type="text/javascript">
        $(document).on('click','.remove_product',function(){
            var array = [];
            $("input[type=checkbox]:checked").each(function(i){
                array.push($(this).val());
            });   

            if(array.length != 0){
                removeProduct(array);
            }

        });

        function removeProduct(product){
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/removeproducts')}}",
                data: {
                    product_id: product,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    if (response.status == 'success') {
                        location.reload();
                        return true;
                    }

                },
                error: function (error) {
                }
            });
        }
    </script>
@endsection
