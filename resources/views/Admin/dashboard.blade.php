@extends('layouts.admin')
@section('title')
    Dashboard
@endsection
@section('css')
@endsection
@section('content')

    <div class="row page-title align-items-center">
        <div class="col-sm-4 col-xl-6">
            <h4 class="mb-1 mt-0">Dashboard</h4>
        </div>

    </div>

    <!-- content -->
    <div class="row">
        <div class="col-md-6 col-xl-2">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Total Customers</span>
                            <h2 class="mb-0">{{number_format($customer,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-2">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Total Orders</span>
                            <h2 class="mb-0">{{number_format($totalTransaction,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-2">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Total Units Sold</span>
                            <h2 class="mb-0">{{number_format($totalUnitSold,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-2">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Average Basket Size</span>
                            <h2 class="mb-0">{{number_format($average,2)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-2">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Total Revenue (AED)</span>
                            <h2 class="mb-0">{{number_format($totalRevenue,1)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-xl-6">
            <div class="card">
                <div class="card-body">

                    <h5 class="card-title mt-0 mb-0 header-title">MOST RECENTLY SOLD</h5>
                    <p>This is your most recent sales for today's date.</p>
                    <div class="table-responsive mt-4">
                        <table class="table table-hover table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">DATE</th>
                                <th scope="col">TRANSACTIONS</th>
                                <th scope="col">UNITS</th>
                                <th scope="col">REVENUE (AED)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mostRecentSold as $row)
                                <tr>
                                    <td>#{{$row->order_id}}</td>
                                    <td>{{\Carbon\Carbon::parse($row->created_at)->format('d M,Y')}}</td>
                                    <td>{{$row->transaction}}</td>
                                    <td>{{$row->unit}}</td>
                                    <td>{{number_format($row->total_amount,1)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div>
        <div class="col-sm-12 col-xl-6">
            <div class="card">
                <div class="card-body">

                    <h5 class="card-title mt-0 mb-0 header-title">YOUR TOP PRODUCTS</h5>
                    <p>Sales performance revenue based on top products</p>
                    <div class="table-responsive mt-4">
                        <table class="table table-hover table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Revenue (AED)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <div style="display: none">{{$i = 1}}</div>
                            @foreach($topProducts as $row)
                                @if($i == 6)
                                    @break
                                @endif
                                <tr>
                                    <td>#{{$i}}</td>
                                    <td>{{@$row['product_name']}}</td>
                                    <td>{{@$row['category']}}</td>
                                    <td>{{number_format($row['price'] * $row['product_count'],1)}}</td>
                                </tr>
                                <div style="display: none">{{$i++}}</div>

                            @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div>
    </div>



@endsection
@section('js')
@endsection
