@extends('layouts.admin')
@section('title')
    Analytics
@endsection
@section('css')
    <!-- plugin css -->
    <link
        href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.css'}}"
        rel="stylesheet" type="text/css"/>
    <link
        href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.css'}}"
        rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <style>
        .apexcharts-menu-icon {
            display: none;
        }

    </style>
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">

            <h4 class="mb-1 mt-1">Analytic</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 mb-4">
            <div id="reportrange" class="pull-left"
                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span>{{$start}} - {{$end}}</span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('message'))
                {!! Session::get('message') !!}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body pb-0">
                    <h5 class="card-title mb-0 header-title">Total Sales</h5>
                    <h2 class="mb-0">AED {{number_format($totalAmount,2)}}</h2>
                    <div id="total_sales" class="apex-charts mt-3" dir="ltr"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body pb-0">

                    <h5 class="card-title mb-0 header-title">Total Orders</h5>
                    <h2 class="mb-0">{{number_format($totalOrderValue,0)}}</h2>
                    <div id="total_orders" class="apex-charts mt-3" dir="ltr"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body pb-0">

                    <h5 class="card-title mb-0 header-title">Average Order Value</h5>
                    <h2 class="mb-0">AED {{number_format($totalAverageSaleValue,2)}}</h2>
                    <div id="average_order_value" class="apex-charts mt-3" dir="ltr"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body pb-0">

                    <h5 class="card-title mb-0 header-title">Average Basket Size</h5>
                    <h2 class="mb-0">{{number_format($totalAverageBasketValue,1)}}</h2>
                    <div id="average_basket_size" class="apex-charts mt-3" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('plugin')
    <!-- datatable js -->
    <script
        src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/jquery.dataTables.min.js'}}"></script>
    <script
        src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.js'}}"></script>
    <script
        src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.responsive.min.js'}}"></script>
    <script
        src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.js'}}"></script>
    <script
        src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.buttons.min.js'}}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


@endsection
@section('js')
    <!-- Datatables init -->
    {{--    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/datatables.init.js')}}"></script>--}}
    <script
        src="{{URL::to(config('app.asset_url').'/storage/app/public/Adminassets/libs/apexcharts/apexcharts.min.js')}}"></script>
    <script>
        $(function () {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                var firstDate = start.format('YYYY-MM-D');
                var lastDate = end.format('YYYY-MM-D');

                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                redirectData(firstDate,lastDate);
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            function redirectData(firstDate,lastDate){

                var urls = "{{URL::to('zaecy-admin/analytics/')}}";
                var value = '?start='+firstDate+'&last='+lastDate;
                console.log(value);
                window.location.href = urls + value;
            }
            // cb(start, end);



        });
    </script>
    <script>
        function ChartData(data, label, id) {


            var e = new Date,
                r = {
                    chart: {height: 296, type: "area"},
                    dataLabels: {enabled: !1},
                    stroke: {curve: "smooth", width: 4},
                    series: [{name: "Revenue", data: data}],
                    zoom: {enabled: !1},
                    legend: {show: !1},
                    colors: ["#43d39e"],
                    xaxis: {
                        type: "string",
                        categories: label,
                        tooltip: {
                            enabled: !1
                        },
                        axisBorder: {
                            show: !1
                        }, labels: {}
                    },
                    yaxis: {
                        labels: {
                            formatter: function (t) {
                                return t + " AED"
                            }
                        }
                    },
                    fill: {
                        type: "gradient",
                        gradient: {
                            type: "vertical",
                            shadeIntensity: 1,
                            inverseColors: !1,
                            opacityFrom: .45,
                            opacityTo: .05,
                            stops: [45, 100]
                        }
                    }
                };
            new ApexCharts(document.querySelector(id), r).render();
        }

        function ChartOrderData(data, label, id) {


            var e = new Date,
                r = {
                    chart: {height: 296, type: "area"},
                    dataLabels: {enabled: !1},
                    stroke: {curve: "smooth", width: 4},
                    series: [{name: "Order Count", data: data}],
                    zoom: {enabled: !1},
                    legend: {show: !1},
                    colors: ["#43d39e"],
                    xaxis: {
                        type: "string",
                        categories: label,
                        tooltip: {
                            enabled: !1
                        },
                        axisBorder: {
                            show: !1
                        }, labels: {}
                    },
                    yaxis: {},
                    fill: {
                        type: "gradient",
                        gradient: {
                            type: "vertical",
                            shadeIntensity: 1,
                            inverseColors: !1,
                            opacityFrom: .45,
                            opacityTo: .05,
                            stops: [45, 100]
                        }
                    }
                };
            new ApexCharts(document.querySelector(id), r).render();
        }

        var saleMonth = <?php  echo json_encode($totalSale)?>;
        var salelabel = [];
        var saledata = [];
        $.each(saleMonth, function (i, row) {
            salelabel.push(row.date);
            saledata.push(row.total);
        });
        ChartData(saledata, salelabel, '#total_sales');

        var orderMonth = <?php  echo json_encode($totalOrder)?>;
        var orderlabel = [];
        var orderdata = [];
        $.each(orderMonth, function (i, row) {
            orderlabel.push(row.date);
            orderdata.push(row.total);
        });
        ChartOrderData(orderdata, orderlabel, '#total_orders');

        var totalAverageSaleMonth = <?php  echo json_encode($totalAverageSale)?>;
        var totalAverageSalelabel = [];
        var totalAverageSaledata = [];
        $.each(totalAverageSaleMonth, function (i, row) {
            totalAverageSalelabel.push(row.date);
            totalAverageSaledata.push(row.total);
        });
        ChartData(totalAverageSaledata, totalAverageSalelabel, '#average_order_value');


        var averageBasket = <?php  echo json_encode($averageBasket)?>;
        var averageBasketlabel = [];
        var averageBasketdata = [];
        $.each(averageBasket, function (i, row) {
            averageBasketlabel.push(row.date);
            averageBasketdata.push(row.total);
        });
        ChartOrderData(averageBasketdata, averageBasketlabel, '#average_basket_size');

    </script>
@endsection
