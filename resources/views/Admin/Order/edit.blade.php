@extends('layouts.admin')
@section('title')
    View Order
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title mt-2 d-print-none">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('/zaecy-admin')}}">Zaecy</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/orders')}}">Orders</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Order</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Order</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- Logo & title -->
                    <div class="clearfix">
                        <div class="float-sm-right">
                            <img src="{{config('app.asset_url').'/storage/app/public/Adminassets/images/logo.svg'}}" alt=""
                                 height="48"/>
                            <h4 class="m-0 d-inline align-middle">Zaecy</h4>
                            <address class="pl-2 mt-2">
                                795 Folsom Ave, Suite 600<br>
                                San Francisco, CA 94107<br>
                                <abbr title="Phone">P:</abbr> (123) 456-7890
                            </address>
                        </div>
                        <div class="float-sm-left">
                            <h4 class="m-0 d-print-none">Order</h4>
                            <dl class="row mb-2 mt-3">
                                <dt class="col-sm-3 font-weight-normal">Order No.:</dt>
                                <dd class="col-sm-9 font-weight-normal">#{{$data['order_id']}}</dd>

                                <dt class="col-sm-3 font-weight-normal">Order Date :</dt>
                                <dd class="col-sm-9 font-weight-normal">{{\Carbon\Carbon::parse($data['created_at'])->format('M d,Y')}}</dd>

                            </dl>
                        </div>

                    </div>

                    <div class="row mt-4">
                        <div class="col-md-6">
                            <h6 class="font-weight-normal">Order For:</h6>
                            <h6 class="font-size-16">{{$data['address_name']}}</h6>
                            <address>
                                {{$data['address1']}}<br>
                                {{$data['address2']}}, {{$data['city']}}, {{$data['postal_code']}}<br>
                                <abbr title="Phone">P:</abbr> {{@$data['userData']['phone_number']}}
                            </address>
                        </div> <!-- end col -->

                        <div class="col-md-6">
                            <div class="text-md-right">
                                <h6 class="font-weight-normal">Total</h6>
                                <h2>{{number_format($data['total_amount'],1)}} AED</h2>
                            </div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->

                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table mt-4 table-centered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item Image</th>
                                        <th>Item</th>
                                        <th style="width: 10%">Quantity</th>
                                        <th style="width: 10%">Price</th>
                                        <th style="width: 10%" class="text-right">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <div style="display: none">{{$i=1}}{{$stotal = 0}}</div>
                                    @foreach($data->product as $row)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td><img src="{{$row->image}}" class="avatar rounded mr-3"></td>
                                            <td>
                                                <h5 class="font-size-16 mt-0 mb-2">{{$row->product_name}} ({{$row->product_size_sku}})</h5>
                                                <p class="text-muted mb-0">{{$row->colour}} - {{$row->product_size}}</p>
                                            </td>
                                            <td>{{$row->qty}}</td>
                                            <td>{{$row->price}}</td>
                                            <td class="text-right">{{number_format($row->qty*$row->price,1)}}</td>
                                        </tr>

                                        <div style="display: none">{{$i++}}{{$stotal += $row->qty*$row->price}}</div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div> <!-- end table-responsive -->
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="clearfix pt-5">
                                <h6 class="text-muted">Notes:</h6>

                                <small class="text-muted">
                                    All accounts are to be paid within 7 days from receipt of
                                    Order. To be paid by cheque or credit card or direct payment
                                    online. If account is not paid within 7 days the credits details
                                    supplied as confirmation of work undertaken will be charged the
                                    agreed quoted fee noted above.
                                </small>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-sm-6">
                            <div class="float-right mt-4">
                                <p><span class="font-weight-medium">Sub-total:</span> <span
                                        class="float-right">{{number_format($stotal,1)}} AED</span></p>
                                <p><span class="font-weight-medium">Discount:</span> <span
                                        class="float-right"> &nbsp;&nbsp;&nbsp; {{number_format($data['discount_amount'],1)}} AED</span></p>
                                <h3>{{number_format($data['total_amount'],1)}} AED</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->
                    {{Form::open(array('url'=>'zaecy-admin/orders/'.$data['id'],'method'=>'put','name'=>'update-order','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="clearfix pt-5" style="width: 50%">
                                <h6 class="text-muted">Airway Tracking Details:</h6>
                                {{Form::text('airway_tracking_number',$data['airway_tracking_number'],array('class'=>"form-control"))}}
                            </div>
                        </div> <!-- end col -->
                        <div class="col-sm-6">
                            <div class="float-right mt-4" style="width: 50%">
                                <h6 class="text-muted">Order Status:</h6>
                                {{Form::select('order_status',[''=>'Select Status','pending'=>'Pending','confirm'=>'Confirm','packing'=>'Packing','ready_to_dispatch'=>'Ready to Dispatch','onway'=>'On way','delivered'=>'Delivered','cancel'=>'Cancel','return'=>"Return"],$data['order_status'],array('class'=>'form-control','id'=>'validationCustom06','required'))}}
                                @error('order_status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>
                            <div class="clearfix"></div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->
                    <div class="mt-5 mb-1">
                        <div class="text-right d-print-none">
                            <a href="javascript:window.print()" class="btn btn-primary"><i
                                    class="uil uil-print mr-1"></i> Print</a>
                            {{Form::submit('Update',array('class'=>'btn btn-info'))}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/form-validation.init.js'}}"></script>

@endsection

