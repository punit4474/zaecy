@extends('layouts.admin')
@section('title')
    Create Order
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/orders')}}">Orders</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Order</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Create Order</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Create Order Form</h4>

                    <hr/>

                    {{Form::open(array('url'=>'zaecy-admin/orders','method'=>'post','name'=>'create-Order','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <h5>Customer Details</h5>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Customer</label>
                                {{Form::select('user_id',$customer,'',array('class'=>'form-control','id'=>'validationCustom04','required'))}}
                                @error('user_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <h5>Product Details</h5>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Product Name</label>
                                {{Form::select('product_id',$product,'',array('class'=>'form-control','id'=>'validationCustom04','required'))}}
                                @error('product_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Colour</label>
                                {{Form::select('colour_id',[],'',array('class'=>'form-control colour_id','id'=>'validationCustom04','required'))}}
                                @error('colour_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Size</label>
                                {{Form::select('size',[],'',array('class'=>'form-control size','id'=>'validationCustom04','required'))}}
                                @error('size')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Qty</label>
                                {{Form::text('qty','',array('class'=>'form-control qty','id'=>'validationCustom04','required'))}}
                                @error('qty')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <a href="javascript:void(0)" class="btn btn-primary add_product" style="margin-top: 25px;">Add New +
                            </a>
                        </div>
                    </div>

                    {{Form::submit('Create',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('zaecy-admin/orders')}}" class="btn btn-danger" >Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/form-validation.init.js'}}"></script>

@endsection

