@extends('layouts.admin')
@section('title')
    Orders
@endsection
@section('css')
    <!-- plugin css -->
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.css'}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.css'}}"
          rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
{{--            <nav aria-label="breadcrumb" class="float-right">--}}
{{--                <a href="{{URL::to('zaecy-admin/orders/create')}}" class="btn btn-primary text-white">+ Create Order</a>--}}
{{--            </nav>--}}
            <h4 class="mb-1 mt-1"> Orders</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total Orders</span>
                            <h2 class="mb-0">{{number_format($totalOrder,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total Revenue</span>
                            <h2 class="mb-0">AED {{number_format($totalRevenue,1)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Orders Fulfilled</span>
                            <h2 class="mb-0">{{number_format($totalFulfilled,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span
                                class="text-muted text-uppercase font-size-12 font-weight-bold">Orders Unfulfilled</span>
                            <h2 class="mb-0">{{number_format($totalUnFulfilled,0)}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                {!! Session::get('message') !!}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-xl-2">
        </div>
        <div class="col-md-3 col-xl-2">

        </div>
        <div class="col-md-3 col-xl-2">

        </div>
        <div class="col-md-3 col-xl-2">


        </div>
        <div class="col-md-3 col-xl-2">
            <div class="form-group mb-3">
                {{Form::select('sorting',array(''=>'Select Sorting','order_asc'=>'Order Number (Ascending)','order_desc'=>'Order Number (Descending)','high'=>'Total Price (High to Low)','low'=>'Total Price (Low to High)'),$type,array('class'=>'form-control sorting','id'=>'validationCustom04','required'))}}
                @error('sorting')
                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                @enderror
            </div>

        </div>
        <div class="col-md-3 col-xl-2">
            <div class="form-group mb-3 ">
                <a href="javascript:void(0)" class="btn btn-primary filter">Apply Filter</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1"> Orders</h4>
                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Date</th>
                            <th>Customer Name</th>
                            <th>Payment Status</th>
                            <th>Fullfillment Status</th>
                            <th>Order Units</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data as $row)
                            <tr>
                                <td>{{$row['order_id']}}</td>
                                <td>{{\Carbon\Carbon::parse($row['created_at'])->format('M d,Y')}}</td>
                                <td>{{@$row['name']}}</td>
                                <td>{{@$row['payment_status']}}</td>
                                <td>{{$row['order_status']}}</td>
                                <td>{{$row['qty']}}</td>
                                <td>AED {{@number_format($row['total_amount'],1)}}</td>
                                <td>
                                    <a class="" href="{{URL::to('zaecy-admin/orders/'.$row['id'].'/edit')}}"> <i
                                            class="uil-pen"></i></a>


                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- datatable js -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/jquery.dataTables.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.responsive.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.js'}}"></script>
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/datatables/dataTables.buttons.min.js'}}"></script>

@endsection
@section('js')
    <!-- Datatables init -->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/datatables.init.js'}}"></script>
    <script>
        var url = '{{URL::to('zaecy-admin/orders')}}';
        $(document).on('click','.filter',function (){
            var value = $('.sorting').val();
            if(value != ''){
                window.location.href = url + '?filter='+value;
            }
        });
    </script>
@endsection
