@extends('layouts.admin')
@section('title')
    Edit Collections
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/colours')}}">Collections</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Collection</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Edit Collections</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Edit Collections Form</h4>

                    <hr/>

                  {{Form::open(array('url'=>'zaecy-admin/colours/'.$data['id'],'method'=>'put','name'=>'edit-colours','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Language</label>
                                {{Form::select('language_id',array(''=>'Select Language','1'=>'English','2'=>'Arabic'),$data['language_id'],array('class'=>'form-control','id'=>'validationCustom04','required'))}}
                                @error('language_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Name</label>
                                {{Form::text('name',$data['name'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Name','required'))}}
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Colour Code</label>
                                {{Form::color('colour_code',$data['colour_code'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Colour Code','required'))}}
                                @error('colour_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Description</label>
                                {{Form::textarea('description',$data['description'],array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Discription','rows'=>2))}}
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>

                    {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('zaecy-admin/colours')}}" class="btn btn-danger" >Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/form-validation.init.js'}}"></script>

@endsection

