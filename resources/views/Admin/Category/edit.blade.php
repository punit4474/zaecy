@extends('layouts.admin')
@section('title')
    Edit Category
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('zaecy-admin/category')}}">Category</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Category</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Edit Category</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Edit Category Form</h4>

                    <hr/>

                  {{Form::open(array('url'=>'zaecy-admin/category/'.$data['id'],'method'=>'put','name'=>'edit-Category','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Language</label>
                                {{Form::select('language_id',array(''=>'Select Language','1'=>'English','2'=>'Arabic'),$data['language_id'],array('class'=>'form-control language_id','id'=>'validationCustom04','required'))}}
                                @error('language_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Collection</label>
                                {{Form::select('collection_id',$collection,$data['collection_id'],array('class'=>'form-control collection_id','id'=>'validationCustom04','required'))}}
                                @error('collection_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Name</label>
                                {{Form::text('name',$data['name'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Name','required'))}}
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>



                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Discription</label>
                                {{Form::textarea('discription',$data['discription'],array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Discription','rows'=>2))}}
                                @error('discription')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                   <label for="validationCustom01">Image</label>
                                {{Form::file('image',array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'image'))}}
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            @if(!empty($data['image']))
                                <img src="{{config('app.asset_url').'/storage/app/public/category/'.$data['image']}}"
                                                 class="avatar rounded mr-3" alt="{{$data['image']}}">
                            @endif
                        </div>


                    </div>

                    {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('zaecy-admin/category')}}" class="btn btn-danger" >Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js'}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{config('app.asset_url').'/storage/app/public/Adminassets/js/pages/form-validation.init.js'}}"></script>
    <script>
        $(document).on('change','.language_id',function (){
            var value = $(this).val();
            getCollection(value);
        });

        function getCollection(language_id){
            $.ajax({
                type: 'POST',
                url: "{{URL::to('zaecy-admin/products/get-collection')}}",
                data: {
                    language_id: language_id,
                    '_token': '{{csrf_token()}}',
                },
                success: function (response) {
                    var html = "<option>Select Collection</option>";
                    $.each(response.data, function (i, data) {
                        html += "<option value=" + data.id + ">" + data.name + "</option>";
                    });
                    $('.collection_id').html(html);
                },
                error: function (error) {
                }
            });
        }
    </script>
@endsection

