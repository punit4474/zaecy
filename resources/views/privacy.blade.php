<html>
<head>
    <title>Privacy &amp; Cookie Policy - Zaecy</title>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/bootstrap.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/icons.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/app.min.css'}}" rel="stylesheet"
          type="text/css"/>
</head>
<body>
<div class="container">
    <h2>Privacy &amp; Cookie Policy</h2>

    <p>&nbsp;</p>
    <p><strong>Zaecy Privacy Policy and Cookie Policy</strong></p>
    <p><span style="font-weight: 400;">This privacy policy describes the personal data collected or generated (processed) when you use Zaecy&rsquo;s websites (&ldquo;Sites&rdquo;) and mobile applications (&ldquo;Apps&rdquo;). It also explains how your personal data is used, shared and protected, what choices you have relating to your personal data and how you can contact us.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>WHO is Responsible for the Processing of Your Personal Data?</strong></p>
    <p><span style="font-weight: 400;">The Zaecy entity responsible for the processing of your personal data will depend on how you interact with Zaecy&rsquo;s Sites and Apps and where you are located in the world. The relevant Zaecy entity are referred to as &ldquo;Zaecy&rdquo;, &ldquo;our&rdquo;, &ldquo;we&rdquo; or &ldquo;us&rdquo; in this privacy policy.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Please review our List of Local Entities for the name of the Zaecy entity responsible and the appropriate contact information.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>What Personal Data Do We Collect and WHEN?</strong></p>
    <p><span style="font-weight: 400;">We ask you for certain personal data to provide you with the products or services you request. For example, when you make purchases, contact our consumer services, request to receive communications, create an account, participate in our events or contests, or use our Sites or Apps.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">This personal data includes your:</span></p>
    <p><span style="font-weight: 400;">contact details including name, email, telephone number and shipping, billing address;</span>
    </p>
    <p><span
            style="font-weight: 400;">login and account information, including screen name, password and unique user ID;</span>
    </p>
    <p><span
            style="font-weight: 400;">personal details including gender, hometown, date of birth and purchase history;</span>
    </p>
    <p><span style="font-weight: 400;">payment or credit card information;</span></p>
    <p><span style="font-weight: 400;">images, photos and videos;</span></p>
    <p><span style="font-weight: 400;">data on physical characteristics, including weight, height, and body measurements (such as estimated stride and shoe/foot measurements or apparel size);</span>
    </p>
    <p><span style="font-weight: 400;">fitness activity data provided by you or generated through our Sites or Apps (time, duration, distance, location, calorie count, pace/stride); or</span>
    </p>
    <p><span style="font-weight: 400;">personal preferences including your wish list as well as marketing and cookie preferences.</span>
    </p>
    <p><span style="font-weight: 400;">We collect additional personal data from you to enable particular features within our Sites and Apps.&nbsp; For example, we request access to your phone&rsquo;s location data to log your run route, your contacts to allow you to interact with your friends, your calendar to schedule a training plan or your social network credentials to post content from an App to a social network.&nbsp; This personal data includes your:</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">movement data from your device&rsquo;s accelerometer;</span></p>
    <p><span style="font-weight: 400;">photos, audio, contacts and calendar information;</span></p>
    <p><span style="font-weight: 400;">sensor data, including heart rate and (GPS) location data; or</span></p>
    <p><span style="font-weight: 400;">social network information, including credentials and any information from your public posts about Zaecy or your communications with us.</span>
    </p>
    <p><span style="font-weight: 400;">When interacting with our Sites and Apps, certain data is automatically collected from your device or web browser. More information about these practices is included in the &ldquo;Cookies and Pixel Tags&rdquo; section of this privacy policy below.&nbsp; This data includes:</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Device IDs, call state, network access, storage information and battery information; and</span>
    </p>
    <p><span style="font-weight: 400;">Cookies, IP addresses, referrer headers, data identifying your web browser and version, and web beacons and tags.</span>
    </p>
    <p><span style="font-weight: 400;">KIDS</span></p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">We comply with local laws and do not allow children to register on our Sites or Apps when they are under the legal age limit of the country in which they reside. We will ask for parental consent for children participating in Zaecy experiences and events.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>TOOLS to Manage What Personal Data We Collect</strong></p>
    <p><span style="font-weight: 400;">When using our Apps and Sites, we also provide in-time notice or obtain consent for certain practices. For example, we will obtain consent to use your location or send push notifications.&nbsp; We may obtain this consent through the Apps or Sites or using the standard permissions available on your device.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">In many cases, your web browser or mobile device platform will provide additional tools to allow you to control when your device collects or shares particular categories of personal data.&nbsp; For example, your mobile device or web browser may offer tools to allow you to manage cookie usage or location sharing.&nbsp; We encourage you to familiarize yourself with and use the tools available on your devices.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>WHY and HOW Do We Use Your Personal Data?</strong></p>
    <p><span style="font-weight: 400;">We use your personal data in the following ways:</span></p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">To Provide the Features of the Sites, Apps, and Services You Request</span></p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">When you use our Sites and Apps, we will use your personal data to provide the requested product or service.&nbsp; For example, if you make a purchase on Zaecy.com or participate in an event or promotion, we will use the contact information you give us to communicate with you about the purchase, event or promotion. If you contact our consumer services, we will use information about you, such as delivery or payment information, or the product you have purchased to help you resolve a problem or question.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">If you use our Apps to track your fitness activity or physical characteristics, we will collect this personal data and store it so that you can review it in the App.&nbsp; Your fitness activity data may include data you enter about your activity or data collected by your device during your activity such as location data and movement data.&nbsp; We may use this activity data to calculate further information about your activity, such as distance run, or calories burned, so that the calculated information can be provided to you as part of the functionality of the App.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">In many cases, to use particular features within our Sites and Apps&nbsp; you may need to provide Zaecy with additional data or additional consent to use particular data in a certain way.&nbsp; For example, to use the heart rate tracking features of our Apps, you may need to connect to a heart rate monitoring device. Similarly, to share content on social media, you may be required to provide your social media account credentials to sign in.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>To Communicate Information about our Products, Services, Events and for Other Promotional
            Purposes</strong>
    </p>
    <p><span style="font-weight: 400;">When you consent, we will send you marketing communications and news concerning Zaecy&rsquo;s products, services, events and other promotions.&nbsp; You can opt-out at any time after you have given your consent.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">If you are an existing customer of Zaecy (for example, if you have placed an order with us), we may use the contact details you provided to send you marketing communications about similar Zaecy products or services where permitted by applicable law (unless you have opted-out). In other cases, we ask for your consent to send you marketing information. We may use the information that you provide to us as well as information from other Zaecy products or services - such as your use of Zaecy&rsquo;s Sites and Apps, your visits to or purchases made in Zaecy stores, your participation in Zaecy events and contests - to personalize communications on products and services that may be interesting for you.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>To Operate, Improve and Maintain our Business, Products and Services</strong></p>
    <p><span style="font-weight: 400;">We use the personal data you provide to us to operate our business.&nbsp; For example, when you make a purchase, we use that information for accounting, audits and other internal functions.&nbsp; We may use personal data about how you use our products and services to enhance your user experience and to help us diagnose technical and service problems and administer our Sites and Apps.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>To Protect Our or Others' Rights, Property or Safety</strong></p>
    <p><span style="font-weight: 400;">We may also use your personal data about how you use our Sites and Apps to prevent or detect fraud, abuse, illegal use, violations of our Terms of Use, and to comply with court orders, governmental requests or applicable law.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>For General Research and Analysis Purposes</strong></p>
    <p><span style="font-weight: 400;">We use data about how our visitors use our Sites, Apps and services to understand customer behavior or preferences.&nbsp; For example, we may use information about how visitors to Zaecy.com search for and find products to better understand the best ways to organize and present product offerings in our storefront.&nbsp;&nbsp;</span>
    </p>
    <p><br/><br/><br/></p>
    <p><strong>Other Purposes</strong></p>
    <p><span style="font-weight: 400;">We may also use your personal data in other ways and will provide specific notice at the time of collection and obtain your consent where necessary.</span>
    </p>
    <p><br/><br/><br/><br/><br/></p>
    <p><strong>Legal Grounds</strong></p>
    <p><span style="font-weight: 400;">To process your personal data, we rely on certain legal grounds, depending on how you interact with our Sites and Apps.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">When you purchase Zaecy products from our Sites and Apps, we need your personal data to fulfill our contract with you. For example, we need your payment and contact details to deliver your order.</span>
    </p>
    <p><span style="font-weight: 400;">When you use our Apps, we rely on your consent for processing and for certain limited purposes to fulfill our contract with you (for example, for in-App purchases).&nbsp;</span>
    </p>
    <p><span style="font-weight: 400;">We also rely on other legal grounds, such as our legitimate interests as a business, to comply with a legal obligation, or to protect your vital interests.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>SHARING of Your Personal Data</strong></p>
    <p>&nbsp;</p>
    <p><strong>Zaecy&rsquo;s Sharing</strong></p>
    <p><span style="font-weight: 400;">Zaecy shares your personal data with:</span></p>
    <p>&nbsp;</p>
    <p><span
            style="font-weight: 400;">Zaecy entities for the purposes and under the conditions outlined above.&nbsp;</span>
    </p>
    <p><span style="font-weight: 400;">Third party service providers processing personal data on Zaecy&rsquo;s behalf, for example to process credit cards and payments, shipping and deliveries, host, manage and service our data, distribute emails, research and analysis, manage brand and product promotions as well as administering certain services and features.&nbsp; When using third party service providers we enter into agreements that require them to implement appropriate technical and organizational measures to protect your personal data.&nbsp;</span>
    </p>
    <p><span style="font-weight: 400;">Other third parties to the extent necessary to: (i) comply with a government request, a court order or applicable law; (ii) prevent illegal uses of our Sites and Apps or violations of our Sites&rsquo; and the Apps&rsquo; Terms of Use and our policies; (iii) defend ourselves against third party claims; and (iv) assist in fraud prevention or investigation (e.g., counterfeiting).&nbsp;</span>
    </p>
    <p><span style="font-weight: 400;">to any other third party where you have provided your consent.</span></p>
    <p><span style="font-weight: 400;">We may also transfer personal data we have about you in the event we sell or transfer all or a portion of our business or assets (including in the event of a reorganization, spin-off, dissolution or liquidation).</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>Your Sharing</strong></p>
    <p><span style="font-weight: 400;">When you use certain social features on our Sites or Apps, you can create a public profile that may include information such as your screen name, profile picture and hometown.&nbsp; You can also share content with your friends or the public, including information about your Zaecy activity.&nbsp; We encourage you to use the tools we provide for managing Zaecy&rsquo;s social sharing to control what information you make available through Zaecy&rsquo;s social features.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>PROTECTION and MANAGEMENT of Your Personal Data</strong></p>
    <p>&nbsp;</p>
    <p><strong>Encryption &amp; Security</strong><span style="font-weight: 400;">&nbsp;</span></p>
    <p><span style="font-weight: 400;">We use a variety of technical and organizational security measures, including encryption and authentication tools, to maintain the safety of your personal data.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>International Transfers of your Personal Data</strong></p>
    <p><span style="font-weight: 400;">The personal data we collect (or process) in the context of our Sites and Apps will be stored in the USA and other countries. Some of the data recipients with whom Zaecy shares your personal data may be located in countries other than the country in which your personal data originally was collected. The laws in those countries may not provide the same level of data protection compared to the country in which you initially provided your data.&nbsp; Nevertheless, when we transfer your personal data to recipients in other countries, including the USA, we will protect that personal data as described in this privacy policy and in compliance with applicable law.We take measures to comply with applicable legal requirements for the transfer of personal data.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>Retention of your Personal Data</strong><span style="font-weight: 400;">&nbsp;</span></p>
    <p><span style="font-weight: 400;">Your personal information will be retained for as long as is necessary to carry out the purposes set out in this privacy policy (unless a longer retention period is required by applicable law). In general, this means that we will keep your personal data for as long as you keep your Zaecy account. For personal data related to product purchases, we retain this longer to comply with legal obligations (such as tax and sales laws and for warranty purposes). Click here to learn more.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>YOUR RIGHTS Relating to Your Personal Data</strong></p>
    <p><span style="font-weight: 400;">You have the right to request: (i) access to your personal data; (ii) an electronic copy of your personal data (portability); (iii) correction of your personal data if it is incomplete or inaccurate; or (iv) deletion or restriction of your personal data in certain circumstances provided by applicable law. These rights are not absolute. Where we have obtained your consent for the processing of your personal data, you have the right to withdraw your consent at any time.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">If you would like to request a copy of your personal data or exercise any of your other rights, please use our Zaecy Privacy Webform.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>Opting Out of Direct Marketing</strong></p>
    <p><span style="font-weight: 400;">If you have a Zaecy account, you can opt-out of receiving Zaecy&rsquo;s marketing communications by modifying your preferences in the "view or change my profile" section of our Sites. You can also opt-out by modifying your email or SMS subscriptions by clicking on the unsubscribe link or following the opt-out instructions included in the message. Alternatively, you can contact us using the contact details in the &ldquo;Question and Feedback&rdquo; section below.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>COOKIES and Pixel Tags</strong></p>
    <p><span style="font-weight: 400;">Zaecy collects information, which may include personal data, from your browser when you use our Sites. We use a variety of methods, such as cookies and pixel tags to collect this information, which may include your (i) IP-address; (ii) unique cookie identifier, cookie information and information on whether your device has software to access certain features; (iii) unique device identifier and device type; (iv) domain, browser type and language, (v) operating system and system settings; (vi) country and time zone; (vii) previously visited websites; (viii) information about your interaction with our Sites such as click behavior, purchases and indicated preferences; and (ix) access times and referring URLs.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Third parties may also collect information via Sites through cookies, third party plug-ins and widgets. These third parties collect data directly from your web browser and the processing of this data is subject to their own privacy policies.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">We use cookies and pixel tags to track our customers&rsquo; usage of the Sites and to understand our customers&rsquo; preferences (such as country and language choices). This enables us to provide services to our customers and improve their online experience. We also use cookies and pixel tags to obtain aggregate data about site traffic and site interaction, to identify trends and obtain statistics so that we can improve our Sites. There are generally three categories of cookies used on our Sites:</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Functional: These cookies are required for basic site functionality and are therefore always enabled. These include cookies that allow you to be remembered as you explore our Sites within a single session or, if you request, from session to session. They help make the shopping cart and checkout process possible as well as assist in security issues and conforming to regulations.</span>
    </p>
    <p><span style="font-weight: 400;">Performance: These cookies allow us to improve our Sites&rsquo; functionality by tracking usage. In some cases, these cookies improve the speed with which we can process your request and allow us to remember site preferences you have selected. Refusing these cookies may result in poorly-tailored recommendations and slow site performance.</span>
    </p>
    <p><span style="font-weight: 400;">Social media and Advertising: Social media cookies offer the possibility to connect you to your social networks and share content from our Sites through social media.&nbsp; Advertising cookies (of third parties) collect information to help better tailor advertising to your interests, both within and beyond our Sites. In some cases, these cookies involve the processing of your personal data. Refusing these cookies may result in seeing advertising that is not as relevant to you or you not being able to link effectively with Facebook, Twitter, or other social networks and/or not allowing you to share content on social media.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">You can always change your preference by visiting the "Cookie Settings" at the bottom of each page of our Sites.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">For a comprehensive and up-to-date summary of every third-party accessing your web browser (through Zaecy Sites or otherwise), we recommend installing a web browser plugin built for this purpose.&nbsp; You can also choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings on each browser and device that you use. Each browser is a little different, so look at your browser Help menu to learn the correct way to modify your cookies. If you turn cookies off, you may not have access to many features that make our Sites and Apps more efficient and some of our services will not function properly.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>USING Zaecy Sites and Apps with Third-Party Products and Services</strong></p>
    <p><span style="font-weight: 400;">Our Sites and Apps allow you to interact with a wide variety of other digital products and services.&nbsp; For example, our Sites and Apps can integrate with third-party devices for activity tracking, social networks, music streaming services and other digital services.&nbsp;</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">If you choose to connect your Zaecy account with a third-party device or account, your privacy rights on third-party platforms will be governed by their respective policies.&nbsp; For example, if you choose to share your Zaecy activity on third-party social media platforms, the policies of those platforms govern the data that resides there.&nbsp;</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Our Sites and Apps may provide links to other (third-party) websites and apps for your convenience or information. Linked sites and apps have their own privacy notices or policies, which we strongly encourage you to review. To the extent any linked websites or apps are not owned or controlled by us, we are not responsible for their content, any use of the websites or apps, or the privacy practices of the websites or apps.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>CHANGES to Our Privacy Policy</strong></p>
    <p><span style="font-weight: 400;">Applicable law and our practices change over time. If we decide to update our privacy policy, we will post the changes on our Sites and Apps. If we materially change the way in which we process your personal data, we will provide you with prior notice, or where legally required, request your consent prior to implementing such changes. We strongly encourage you to read our privacy policy and keep yourself informed of our practices.&nbsp; This privacy policy was last modified in May 2019.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>QUESTIONS and Feedback</strong></p>
    <p><span style="font-weight: 400;">We welcome questions, comments, and concerns about our privacy policy and privacy practices. If you wish to provide feedback or if you have questions or concerns or wish to exercise your rights related to your personal data, please contact the Zaecy Privacy Office by email at info@Zaecy.com.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">If you contact us with a privacy complaint it will be assessed with the aim of resolving the issue in a timely and effective manner.</span>
    </p>
</div>
</body>
</html>
