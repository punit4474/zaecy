<div class="media user-profile mt-2 mb-2">
    @if(file_exists(storage_path('app/public/user/'.Auth::user()->profile_pic)) && Auth::user()->profile_pic != '')

        <img src="{{config('app.asset_url').'/storage/app/public/user/'.Auth::user()->profile_pic}}"
             class="avatar-sm rounded-circle mr-2" alt="Shreyu"/>
    @else
        <img src="{{config('app.asset_url').'/storage/app/public/Adminassets/images/users/avatar-7.jpg'}}"
             class="avatar-sm rounded-circle mr-2" alt="Shreyu"/>
    @endif


    @if(file_exists(storage_path('app/public/user/'.Auth::user()->profile_pic)) && Auth::user()->profile_pic != '')
        <img src="{{config('app.asset_url').'/storage/app/public/user/'.Auth::user()->profile_pic}}"
             class="avatar-xs rounded-circle mr-2" alt="Shreyu"/>
    @else
        <img src="{{config('app.asset_url').'/storage/app/public/Adminassets/images/users/avatar-7.jpg'}}"
             class="avatar-xs rounded-circle mr-2" alt="Shreyu"/>
    @endif


    <div class="media-body">
        <h6 class="pro-user-name mt-0 mb-0">{{Auth::user()->name}}</h6>
        <span class="pro-user-desc">Administrator</span>
    </div>
    <div class="dropdown align-self-center profile-dropdown-menu">
        <a class="dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
           aria-expanded="false">
            <span data-feather="chevron-down"></span>
        </a>
        <div class="dropdown-menu profile-dropdown">

            <div class="dropdown-divider"></div>

            <a href="{{URL::to('zaecy-admin/logout')}}" class="dropdown-item notify-item">
                <i data-feather="log-out" class="icon-dual icon-xs mr-2"></i>
                <span>Logout</span>
            </a>
        </div>
    </div>
</div>
<div class="sidebar-content">
    <!--- Sidemenu -->
    <div id="sidebar-menu" class="slimscroll-menu">
        <ul class="metismenu" id="menu-bar">
            <li class="menu-title">Navigation</li>

            <li>
                <a href="{{URL::to('zaecy-admin')}}">
                    <i data-feather="home"></i>
                    <span> Dashboard </span>
                </a>
            </li>

            <li>
                <a href="{{URL::to('zaecy-admin/collections')}}">
                    <i data-feather="box"></i>
                    <span> Collections </span>
                </a>
            </li>
            <li>

                <a href="javascript: void(0);" aria-expanded="false" class="mm-collapsed">
                    <i data-feather="list"></i>
                    <span> Category </span>
                    <span class="menu-arrow"></span>
                </a>

                <ul class="nav-second-level mm-collapse" aria-expanded="false" style="">
                    <li>
                        <a href="{{URL::to('zaecy-admin/category')}}">Product Category</a>
                    </li>
                    <li>
                        <a href="{{URL::to('zaecy-admin/page-category')}}">Page Category</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('zaecy-admin/subcategory')}}">
                    <i data-feather="layers"></i>
                    <span> SubCategory </span>
                </a>
            </li>
        <!-- <li>
                <a href="{{URL::to('zaecy-admin/colours')}}">
                    <i data-feather="codesandbox"></i>
                    <span> Colours </span>
                </a>
            </li> -->
            <li>
                <a href="javascript: void(0);" aria-expanded="false" class="mm-collapsed">
                    <i data-feather="server"></i>
                    <span> Style </span>
                    <span class="menu-arrow"></span>
                </a>

                <ul class="nav-second-level mm-collapse" aria-expanded="false" style="">
                    <li>
                        <a href="{{URL::to('zaecy-admin/fits')}}">Fit</a>
                    </li>
                    <li>
                        <a href="{{URL::to('zaecy-admin/style')}}">Style</a>
                    </li>
                    <li>
                        <a href="{{URL::to('zaecy-admin/nack-style')}}">Nack Style</a>
                    </li>
                    <li>
                        <a href="{{URL::to('zaecy-admin/sleeve-type')}}">Sleeve Type</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('zaecy-admin/products')}}">
                    <i data-feather="package"></i>
                    <span> Products </span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('zaecy-admin/discount')}}">
                    <i data-feather="percent"></i>
                    <span> Coupons </span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('zaecy-admin/analytics')}}">
                    <i data-feather="activity"></i>
                    <span> Analytics </span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('zaecy-admin/orders')}}">
                    <i data-feather="shopping-cart"></i>
                    <span> Orders </span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('zaecy-admin/customers')}}">
                    <i data-feather="user"></i>
                    <span> Customers </span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('zaecy-admin/slider')}}">
                    <i data-feather="airplay"></i>
                    <span> Slider </span>
                </a>
            </li>

        </ul>
    </div>
    <!-- End Sidebar -->

    <div class="clearfix"></div>
</div>
