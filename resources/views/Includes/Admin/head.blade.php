<head>
    <meta charset="utf-8"/>
    <title>@yield('title') - Zaecy</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href=""/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content="Gurufit"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
    <meta name="theme-color" content="#fdfbf3">
    <meta name="msapplication-navbutton-color" content="#fdfbf3">
    <meta name="apple-mobile-web-app-status-bar-style" content="#fdfbf3">
    <link rel="apple-touch-icon" sizes="57x57" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/57x57.png'}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/60x60.png'}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/72x72.png'}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/76x76.png'}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/114x114.png'}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/120x120.png'}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/144x144.png'}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/152x152.png'}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/180x180.png'}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/32x32.png'}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/192x192.png'}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{config('app.asset_url').'/storage/app/public/Frontassets/media/favs/16x16.png'}}">

    <!-- App css -->
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/bootstrap.min.css'}}" rel="stylesheet" type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/icons.min.css'}}" rel="stylesheet" type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/app.min.css'}}" rel="stylesheet" type="text/css"/>
    @yield('css')
</head>
