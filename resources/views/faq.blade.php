<html>
<head>
    <title>FAQ - Zaecy</title>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/bootstrap.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/icons.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/app.min.css'}}" rel="stylesheet"
          type="text/css"/>
</head>
<body>
<div class="container">
    <h2>FAQ</h2>
    <ol>
        <li><strong> How Do I Shop?</strong></li>
        <li><span style="font-weight: 400;"> How do I view what&rsquo;s in my shopping cart?</span></li>
    </ol>
    <p><span style="font-weight: 400;">To view the contents of your cart, click on the &ldquo;View Cart&rdquo; icon in the upper-right corner of your computer screen.</span>
    </p>
    <p><span style="font-weight: 400;">Once you click on this icon, you can easily change the quantity you want to purchase of a particular item in your cart by updating the quantity listed and then clicking the &ldquo;Update cart&rdquo; link.</span>
    </p>
    <p><span style="font-weight: 400;">You can also delete any item in your cart by clicking the &ldquo;Remove&rdquo; checkbox to the left of that item and then updating your cart with the &ldquo;Update cart&rdquo; link.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> How do I add items to my cart?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Choose the product from the category page and you will be taken to the product page, choose the color, size, and quantity and then click on &ldquo;ADD TO CART&rdquo; and the product will be added in the cart.</span>
    </p>
    <p><span style="font-weight: 400;">If you wish to checkout, click on the cart button on the right-hand corner and proceed to checkout or you can continue shopping.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> How do I remove items from my cart?</span></li>
    </ol>
    <p><span style="font-weight: 400;">You can delete any item in your cart by clicking the &ldquo;Remove&rdquo; checkbox to the left of that item and then updating your cart with the &ldquo;Update cart&rdquo; link.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> How do I change the quantity of a particular item in my cart?</span></li>
    </ol>
    <p><span style="font-weight: 400;">First, click on the &ldquo;View Cart&rdquo; link in the upper-right corner of your computer screen. This will allow you to view all items currently in your cart, as well as the quantities that you have chosen for each item.</span>
    </p>
    <p><span style="font-weight: 400;">To change the quantity of an item in your cart, move your cursor to the box that appears under the &ldquo;Qty&rdquo; header for this item and types in the quantity desired, then click the &ldquo;Update cart&rdquo; link.</span>
    </p>
    <p><span style="font-weight: 400;">Once you do this, the quantity and associated amount will automatically change and reflect the correct amounts for both the quantity and the cost.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> What if I order something and it is out of stock?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Unfortunately, due to popular demand, some items do sell out quickly online &ndash; if you see something online that you want to have, be sure to snap it up quickly! If you do happen to miss out, contact us at info@zaecy.com and we will let you know if the item will come available again.</span>
    </p>
    <p><span style="font-weight: 400;">Once we get a shipment of new stock and it becomes available for purchase, you will be sent an email informing you of the available new stock.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> Where can I find sizing information?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Please go through our size guide</span></p>
    <p>&nbsp;</p>
    <ol start="2">
        <li><strong> Delivery Info</strong></li>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> What are the shipping times and charges?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Please check all the up-to-date shipping and delivery information on our Shipping and Delivery page.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> What time can I expect my order delivery?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Delivery times are usually between 7 am to 9 pm, local time. However, it can vary according to the country and the courier service.</span>
    </p>
    <p><span style="font-weight: 400;">Once you have received the tracking number and shipping information. You can call the respective courier company to schedule delivery at your preferred time during business hours on the weekdays.&nbsp;</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span
                style="font-weight: 400;"> What if no one is home during delivery? Do I need to sign for receiving?</span>
        </li>
    </ol>
    <p><span style="font-weight: 400;">We understand that you may not be available to receive your package. A signature is not always required for the parcel. If you have paid online, our delivery team can deliver your parcel to anyone available at the address. If the order is Cash on Delivery(CoD), please instruct the receiving person to hand the CoD amount to the delivery guy.</span>
    </p>
    <p><span style="font-weight: 400;">You can always track your package with the tracking code from the courier company. If you cannot track your parcel then email us at info@zaecy.com, and we will track down your parcel.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> Do you ship in my country?</span></li>
    </ol>
    <p><span style="font-weight: 400;">We deliver orders worldwide in over 200 countries.</span></p>
    <p><span style="font-weight: 400;">Please check all the up-to-date shipping and delivery information on our Shipping and Delivery page.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> Will, I will be charged customs and import duties?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Unfortunately, Zaecy cannot control any customs charges or import duties that are applied to your package by your local customs.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> Do you deliver to PO Box, BFPO, and APO Addresses?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Unfortunately, we don&rsquo;t deliver on PO Box, BFPO, and APO addresses. You will need to provide us with full residential or commercial addresses.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> What shipping provider do you use?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Our shipping partner is Aramex &amp; DHL.</span></p>
    <p>&nbsp;</p>
    <ol start="3">
        <li><span style="font-weight: 400;"> My Order Info</span></li>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> Where is my order?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Once your order is placed and processed, you would receive an email confirmation which includes all the relevant details regarding your order. Please know the shipping times for your order.</span>
    </p>
    <p><span style="font-weight: 400;">If your order did not arrive within the shipping time, then email us at info@zaecy.com, or go to our website and message chat support.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> Can I track my order?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Yes, you can track your order once the order has been dispatched from the Zaecy facility. As soon as the order is dispatched from our warehouse, you will receive a tracking number and the courier website address to track your order in real time.</span>
    </p>
    <p><span style="font-weight: 400;">If you didn&rsquo;t not receive the tracking number after 2 days of placing the order, get the tracking number by either emailing us at </span><a
            href="mailto:info@zaecy.com"><span style="font-weight: 400;">info@zaecy.com</span></a><span
            style="font-weight: 400;"> or getting hold of customer chat support at our website.</span></p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> I entered the wrong item/shipping address, what should I do?</span></li>
    </ol>
    <p><span style="font-weight: 400;">If you have entered the wrong item/size, or even a wrong shipping address in your checkout, and the order is already placed, contact our staff within 60 minutes at info@zaecy.com, with all your relevant details so that our team can promptly resolve the issue. If, for whatever reason, you have failed to notify our team within 60 minutes of placing your order: Incorrect address: Still notify us, we will do our best to change the delivery address if the order hasn&rsquo;t been delivered already. Or contact the address where you have sent the address. Only they will be able to return the package to you. Incorrect item: You will have to wait till you receive your items and then return it to us.</span>
    </p>
    <p><span
            style="font-weight: 400;">In both cases, we are not responsible for the delivery charges if you incurred any.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> Why was my order cancelled?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Our items sell out extremely fast due to such high demand and on occasion, an item you have ordered can suddenly become unavailable. These cases are extremely rare but can happen from time to time.</span>
    </p>
    <p><span style="font-weight: 400;">If the item you order does sell out, we will contact you as soon as possible to confirm the cancellation of your order. If your order contains multiple items, we will dispatch the rest of your order and refund the out of stock item.</span>
    </p>
    <p>&nbsp;</p>
    <ol start="4">
        <li><span style="font-weight: 400;"> Account Info</span></li>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> I have forgotten my password. What can I do?</span></li>
    </ol>
    <p><span style="font-weight: 400;">If you have forgotten your password, then click on the &lsquo;forgot password&rsquo; button on the login screen. You will be asked to enter your email address on the next page.</span>
    </p>
    <p><span style="font-weight: 400;">Once you enter it and hit send, you will receive an email to change your password. Open the email, and click on the link to change password. Another page will open where you can type in your new password.</span>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li><span style="font-weight: 400;"> I cannot login to my account. What can I do?</span></li>
    </ol>
    <p><span style="font-weight: 400;">Email us at info@zaecy.com if the login page gives you an error other than &lsquo;incorrect password&rsquo; or &lsquo;incorrect email&rsquo;.</span>
    </p>
    <p><span style="font-weight: 400;">&nbsp;</span></p>
</div>
</body>
</html>
