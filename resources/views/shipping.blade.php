<html>
<head>
    <title>Shipping and Delivery - Zaecy</title>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/bootstrap.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/icons.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/app.min.css'}}" rel="stylesheet"
          type="text/css"/>
</head>
<body>
<div class="container">
    <h2>Shipping and Delivery</h2>
    <br/>
    <p><strong>COVID-19 UPDATE</strong><span style="font-weight: 400;">&nbsp;&nbsp;</span></p>
    <p><span style="font-weight: 400;">You can still shop at ZAECY without any hesitation as we are shipping your orders daily around the world,&nbsp;</span>
    </p>
    <p><span style="font-weight: 400;">The safety &amp; well-being of our employees, and the customers we serve, is our top priority. Our Customer Success &amp; Logistics teams are on top of the disruptions caused by Covid-19 and although we are doing our best to make sure it doesn&rsquo;t affect the order processing times, you might still experience slight delays due to flight operations, lockdown or situations beyond our control.</span>
    </p>
    <p><span style="font-weight: 400;">In case you notice a delay in delivery, please contact us at info@zaecy.com and one of our team members will immediately look into it for you.</span>
    </p>
    <p><span style="font-weight: 400;">Take Care &amp; Be Safe!</span></p>
    <p><span style="font-weight: 400;">&nbsp;</span></p>
    <p><strong>TRANSPARENT SHIPPING POLICY</strong></p>
    <p><span style="font-weight: 400;">We ship worldwide. Please allow 1-2 business days for orders to be processed. Tracking information is automatically sent to the email address you provide at checkout.</span>
    </p>
    <p><span style="font-weight: 400;">If you do not receive the tracking information within 1-2 business days, please check your spam folder.</span>
    </p>
    <p><span style="font-weight: 400;">Orders are processed and shipped 6 days a week (Monday &ndash; Saturday), except on UAE &amp; International Public Holidays.</span>
    </p>
    <p><span style="font-weight: 400;">You can track your order with your courier by simply logging onto their website and typing in your tracking number.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>PAYMENT METHODS</strong></p>
    <p><span style="font-weight: 400;">CREDIT / DEBIT CARDS: We accept all VISA, MasterCard, American Express, Diners &amp; Discover credit and debit cards.</span>
    </p>
    <p><span style="font-weight: 400;">All your card payments are processed through military-grade encrypted and PCI compliant payment processors like 2Checkout, BlueSnap, and PayTabs. We do not store any of your credit card information.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">PAYPAL: PayPal allows you to make payments using a variety of methods including PayPal Cash or PayPal Cash Plus account balance, a bank account, PayPal Credit, debit or credit cards, and rewards balance.</span>
    </p>
    <p><span style="font-weight: 400;">You can choose any of the payment methods in your account with PayPal as your preferred payment method in the Payments section of your account settings. If you select a preferred payment method, it will be shown as the preferred payment method when you make a purchase online.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">CASH ON DELIVERY: This payment method is only available in United Arab Emirates, Saudi Arabia, Oman, Bahrain, Kuwait, and Jordan.</span>
    </p>
    <p><span style="font-weight: 400;">The cash on Delivery payment method is free in UAE. but since the COD service is costly for us to provide outside of UAE, cash payment orders outside of UAE will be charged a COD Fee as follows:</span>
    </p>
    <p><span style="font-weight: 400;">Saudi Arabia: SAR___</span></p>
    <p><span style="font-weight: 400;">Qatar: QAR____</span></p>
    <p><span style="font-weight: 400;">Kuwait: KWD____</span></p>
    <p><span style="font-weight: 400;">Oman: OMR___</span></p>
    <p><span style="font-weight: 400;">Bahrain: BHD___</span></p>
    <p><span
            style="font-weight: 400;">Please have the exact amount available for the courier, it will save you time.</span>
    </p>
    <p><span
            style="font-weight: 400;">Select your preferred payment method on the checkout page to complete your order.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">CUSTOM DUTIES &amp; TAXES</span></p>
    <p><span style="font-weight: 400;">Customers from UAE, US, UK, Europe, KSA, and Canada will not be subject to any customs duties &amp; taxes.</span>
    </p>
    <p><span style="font-weight: 400;">As all orders are shipped directly from the UAE, you may be susceptible to customs fees in countries not listed above.&nbsp;</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">ZAECY will NOT deal or provide any services or products to any OFAC (Office of Foreign Assets Control) sanctioned countries in accordance with the law of UAE.</span>
    </p>
    <p><span style="font-weight: 400;">Multiple shipments/delivery may result in multiple postings to the cardholder&rsquo;s monthly statement.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">xxx</span></p>
    <p><span style="font-weight: 400;">Happy Shopping!</span></p>
</div>
</body>
</html>
