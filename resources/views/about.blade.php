<html>
<head>
    <title>About us - Zaecy</title>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/bootstrap.min.css'}}" rel="stylesheet" type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/icons.min.css'}}" rel="stylesheet" type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/app.min.css'}}" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">
    <h2>About Zaecy</h2>
    <p><span style="font-weight: 400;">Zaecy is a &lsquo;Made to Move&rsquo; clothing brand based in Jordan. The brand was started with the purpose of promoting a healthy, happy lifestyle, particularly through yoga. Yoga is the ancient Indian tradition of gaining complete control over the mind and body through physical postures (asana), breathing techniques (pranayama), and spiritual practices. Originating from the Sanskrit word &lsquo;yuj&rsquo; meaning &lsquo;connectivity&rsquo;, yoga is a deeply spiritual way of life that connects you physically, mentally, and emotionally to nature and the universe. It is the most natural and holistic way of living and studies have demonstrated its many benefits like stress relief, improved heart health, regulated sleep cycle and many more.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">People&rsquo;s lives all over the world have become increasingly high-stress and sedentary over the years. During these stressful times, Zaecy strives to act as a gentle reminder to take some time out for yourself and get some exercise. The clothes we design are made for moderate to high-intensity physical activity. We believe in a delicate balance of comfort and cutting-edge fashion and strive to give our customers nothing but the very best.&nbsp;</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Designing clothes is an art as well as a science. And at Zaecy, we take the science aspect very seriously. All our products are made with materials handpicked to provide comfort and ease. Superior fabric finishing techniques ensure hygiene and durability. Our wide range of designs, colors, and patterns is loved by customers of all ages alike.&nbsp;</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Sustainability is one of the core tenets Zaecy abides by. Our collection themes reflect our deep connection with Mother Nature and the universe, and we believe in giving back as much as we take. We try to use eco-friendly raw materials and energy-efficient manufacturing processes as much as possible.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">For those who want to be healthy but never seem to have the time, who are constantly plagued by very busy schedules, Zaecy will act as the constant, gentle reminder to get up and get moving. Instead of focusing on &lsquo;better performance&rsquo; or &lsquo;stronger agility&rsquo; like other brands, we simply strive to give each person a chance to be fit and healthy at their own pace.</span>
    </p>
    <p><br/><br/></p>
    <p><strong>Brand Story</strong></p>
    <p><span style="font-weight: 400;">Zaecy started with the idea to promote the idea of a healthy lifestyle among the masses. We aim to extend a helping hand for people who strive to achieve a fit body and a wholesome mind though the comfort of our quality products and post-sales services.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Our name 'Zaecy' (pronounced 'Zay-chee') is an amalgamation of &lsquo;Zaci&rsquo; and &lsquo;Shachi&rsquo;. Zaci is the god of fatherhood in African tradition while Shachi is the queen of the gods in Hinduism, as well as another name for the Shatavari plant which is used in Ayurvedic medicine for its anti-oxidant and anti-ageing properties. The name &lsquo;Zaecy&rsquo; thus connotes the harmonious union of masculine and feminine energy. It signifies grace, compassion, and healing along with skill, might, and strength.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">The logo depicts a central design theme. The corners of the &lsquo;Z&rsquo; appear to be pointing towards a direction, which is the path to a fit, healthy lifestyle. The tick marks forming the Z are about making the right decisions regarding your health and keeping your mind and body in the right condition.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">The color scheme of blue and green is pleasant to look at and brings a sense of calm to the mind, along with hinting at our deep connection with nature. It also reminds one of the peacock, the national bird of India, thus capturing the authentic Indian heart and soul of the brand.</span>
    </p>
    <p>&nbsp;</p>
    <p><strong>Mission</strong></p>
    <p><span style="font-weight: 400;">Our tagline, "Made to Move" says it all. Here at Zaecy, we aim to promote a healthy lifestyle. The average person needs at least 30 minutes of exercise. Unfortunately, due to our hectic personal and professional schedules, most of us can barely manage a fraction of that. Or we simply aren't motivated enough.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">In today's times, the importance of physical as well as mental health is being realised like never before. A healthy mind resides in a healthy body and now there is more emphasis on the role of regular exercise as a stress buster and immunity builder. Doctors and medical professionals are recommending a fitness regime not just for physical health but to relieve depression and anxiety as well.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">At Zaecy, we aspire to create smart, attractive, and comfortable clothing that you look forward to working out in. The lightweight, stretchy, breathable fabric is easy to move around in. Superior moisture control and wicking techniques with anti-microbial properties keep you safe from sweat and germs. Soil release finish makes the product easy to wash while abrasion resistance ensures long-lasting durability.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Our products come in a wide variety of colors and aesthetic patterns to uplift your mood and help you feel confident about yourself every day. We strive to bring back the spark in your exercise routine and give you some much-needed motivation to achieve your goals.</span>
    </p>
    <p><br/><br/></p>
    <p><strong>#MadeToMove</strong></p>
    <p><span style="font-weight: 400;">We are an all-inclusive brand that understands that people of all sizes, shapes, ages, genders, skin tones are &lsquo;made to move&rsquo;. We believe that every human is unique and we aim to celebrate this diversity by creating designs tailor-made to suit the needs of all body and skin types.</span>
    </p>
    <p><br/><br/></p>
    <p><strong>Philosophy</strong></p>
    <p><span style="font-weight: 400;">Zaecy emphasises on a strong connection with the earth and the universe. We believe in drawing strength and positive vibrations from nature to accomplish our targets. Our designs draw inspiration from elements of nature (soil, water, wood, metal, fire) and celestial bodies (sun, moon, Mercury, Venus, Mars). We try to encourage people to respect and honour the marvels of creation all around us and draw inspiration and inner strength from the same.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">Zaecy believes in the tenet, &lsquo;Vasudhaiva Kutumbakam&rsquo;, meaning &ldquo;the world is a family&rdquo;. A large global network of healthy and happy individuals living in harmony with the universe is our dream for the future.</span>
    </p>
    <p><br/><br/></p>
    <p><strong>Vision</strong></p>
    <p><span style="font-weight: 400;">&lsquo;Vasudhaiva Kutumbakam&rsquo; (&ldquo;the world is a family&rdquo;) is Zaecy&rsquo;s motto. Our vision is to create a global community of individuals leading fit, healthy, and happy lives in harmony with nature. We are currently working to expand our brand all over the Middle East, with Jordan as our operations base and we aspire to become the No. 1 brand worldwide that motivates you to adopt and maintain a healthy lifestyle.</span>
    </p>
    <p>&nbsp;</p>
    <p><span style="font-weight: 400;">We further aim to inculcate diverse design styles from all over the world, making us a truly global brand. We look forward to reaching customers in every corner of the globe and spreading our agenda of holistic, harmonious, and healthy living.</span>
    </p>
</div>
</body>
</html>
