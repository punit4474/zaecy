<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
<div style="max-width: 375px;background: rgb(165 165 165 / 10%);padding: 15px;margin: 20px auto;">
    <a href="#" style="max-width: 100px;display: block;width: 100%;margin: 0 auto;">
        <img style="width: 100%;height: 100%;object-fit: contain;"
             src="https://zstage.zaecy.com/backend/storage/app/public/zaecy_logo.png" alt="">
    </a>
    <p style="font-size: 16px;font-weight: 500;">{{$name}}, your order {{$order_id}} has been dispatched. You&#39;ll get it in just a
        few more days. Thanks for shopping with us :)</p>
    <p style="font-size: 16px;font-weight: 500;">&quot;Movement is the song of the body&quot;</p>
    <p>Team Zaecy</p>
    <p style="margin: 20px 0 0 0;padding: 15px 0 5px 0;text-align: center; border-top: 1px solid #0002;">Copyright ©
        Zaecy rights reserved.</p>
</div>
</body>

</html>
