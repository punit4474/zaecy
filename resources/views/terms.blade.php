<html>
<head>
    <title>TERMS &amp; CONDITIONS - Zaecy</title>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/bootstrap.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/icons.min.css'}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{config('app.asset_url').'/storage/app/public/Adminassets/css/app.min.css'}}" rel="stylesheet"
          type="text/css"/>
</head>
<body>
<div class="container">
    <h2>TERMS &amp; CONDITIONS</h2>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">USE OF THE WEBSITE</span></p>
<p><span style="font-weight: 400;">Welcome to the Zaecy website (&ldquo;zaecy.com&rdquo;). By entering our Website, you agree to these terms and conditions (the &ldquo;Terms&rdquo;). Please review these carefully prior to making any purchases. By using this Website you agree to these Terms.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">In order to be able to make purchases through the Website, you will be requested to register. The information sought will allow Zaecy to handle your transaction efficiently and effectively. </span><span
        style="font-weight: 400;">Any purchase, dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of the UAE</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">As payment will be made online/cash on delivery (COD), it is important that you enter all the requested information accurately. This will ensure that there is no delay in the dispatch of your order. The information provided will be treated in the strictest confidence. We do not share any customer data to any third parties and all customer data is held on a secure server. For more details about Privacy at Zaecy, please read our Privacy Policy.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">By making an offer to purchase merchandise, you authorize Zaecy to perform credit checks and to transmit or to obtain information (including any updated information) about you to or from third parties, including but not limited to your payment card number or credit reports (including credit reports for your spouse if you reside in a community property jurisdiction), to authenticate your identity, to validate your payment card, to obtain an initial payment card I.D.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">You agree not to impersonate any other person or entity or to use a false name or a name that you are not authorized to use.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">Zaecy may use the information you provide to conduct suitable anti-fraud reviews, the agency involved may keep a record of that information.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">We reserve the right to restrict multiple quantities of a specific item from being shipped to any one customer or postal address.</span>
</p>
<p><span style="font-weight: 400;">In case your personal information changes, please notify us by contacting Customer Care on the following email address________________.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">Zaecy may modify these Terms at our sole discretion. If you do not agree to the changes to these Terms, you should stop using this Website. By using this Website after any such change(s), you agree to comply with the Terms as changed. Any changes made after you have placed an order with us, will not affect that order, unless we are required to make the change by law.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">SHOPPING AT ZAECY</span></p>
<p><span style="font-weight: 400;">When you make a purchase through the Website, you are offering to buy Product(s) for the price stated, subject to these Terms. All orders are subject to acceptance and availability. Zaecy </span><span
        style="font-weight: 400;">Will not provide any services or products to any Office of Foreign Assets Control (OFAC) sanctioned countries in accordance with UAE law</span>
</p>
<p><br/><br/><br/></p>
<p><span style="font-weight: 400;">PAYMENT</span></p>
<p><span style="font-weight: 400;">Zaecy offers customers various payment methods including cash on delivery, payment through credit card, debit card, and net banking.&nbsp;</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">RETURNS AND EXCHANGES</span></p>
<p><span style="font-weight: 400;">If a customer is not happy with their purchase they may obtain an exchange within 5 calendar days of receipt of the goods. </span><span
        style="font-weight: 400;">Refunds will be done on the original mode of payment used</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">CONTENT</span></p>
<p><span style="font-weight: 400;">Zaecy &ldquo;Content&rdquo; is defined as any graphics, photographs, including all image rights, sounds, music, video, audio, or text on this Website. Zaecy tries to ensure that the information on this Website is accurate and complete. Zaecy does not promise that Zaecy Content is accurate or error-free. Zaecy does not promise that the functional aspects of the Website or Zaecy Content will be error-free or that this Website, Zaecy Content, or the server that makes it available are free of viruses or other harmful components. We always recommend that all users of the Internet ensure they have up-to-date virus checking software installed.</span>
</p>
<p><span style="font-weight: 400;">By posting on any of our social media channels you agree to be solely responsible for the content of all information you contribute. You also grant Zaecy the right to use any content you provide for its own purposes including republication in any form or media. Comments may be moderated and may take up to 72 hours to be displayed but Zaecy does not commit to checking all content and will not be liable for third-party posts. If you have a complaint about any posts please email____________.</span>
</p>
<p><span style="font-weight: 400;">Zaecy reserves the right at its sole discretion not to publish or to remove any comment including those that it believes may be unlawful, defamatory, racist or libelous, incite hatred or violence, detrimental to people, institutions, religions or to people&rsquo;s privacy, which may cause harm to minors, is detrimental to the trademarks, patents, and copyrighted content contains personal data, improperly uses the medium for promoting and advertising businesses. This site is available to the public; information you consider confidential should not be posted to this site.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">How do we use your information?</span></p>
<p><span style="font-weight: 400;">We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</span>
</p>
<p><span style="font-weight: 400;">To quickly process your transactions.</span></p>
<p><span style="font-weight: 400;">To send periodic emails regarding your order or other products and services.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">GENERAL</span></p>
<p><span style="font-weight: 400;">You acknowledge and agree that these Terms, together with Zaecy Privacy Policy, constitute the complete and exclusive agreement between us concerning your use of the Website and any purchase by you of any Products from Zaecy, and supersede and govern all prior proposals, agreements, or other communications.</span>
</p>
<p><span style="font-weight: 400;">If any provision of these Terms is held to be illegal, invalid, or unenforceable in whole or in part, then these Terms shall continue to be valid as to the other provisions and the remainder of the affected provision. Any waiver of any of the provisions of these Terms by Zaecy shall not be deemed a waiver of any subsequent breach or default and shall in no way affect the other provisions of these Terms.</span>
</p>
<p><span style="font-weight: 400;">Nothing contained in these Terms shall be construed as creating any agency, partnership, or other forms of joint enterprise between us.</span>
</p>
<p><span style="font-weight: 400;">Zaecy reserves the right to change these Terms at any time and we encourage you to revisit these Terms periodically to ensure that you are at all times fully aware of our Terms. Any changes are effective immediately upon posting to the Website. Your continued use of the Website constitutes your agreement to all such Terms.</span>
</p>
<p><span style="font-weight: 400;">Zaecy reserves the right to refuse to supply Products to any person if they are in breach of any of the Terms of Use specified.</span>
</p>
<p><span style="font-weight: 400;">If you have any questions regarding these Terms, please send an email to support@Zaecy.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">GOVERNING LAW</span></p>
<p><span style="font-weight: 400;">Your use of this Website and any purchase by you of any Product from Zaecy shall be governed by U.A.E law and you hereby submit to the exclusive jurisdiction of the U.A.E. courts. Please check back frequently to see any updates or changes to our Privacy Policy.</span>
</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">MISCELLANEOUS</span></p>
<p><span style="font-weight: 400;">Any dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of UAE.</span>
</p>
<p><span style="font-weight: 400;">United Arab of Emirates is our country of domicile.</span></p>
<p><span style="font-weight: 400;">Minors under the age of 18 shall are prohibited to register as a User of this website and are not allowed to transact or use the website.</span>
</p>
<p><span style="font-weight: 400;">If you make a payment for our products or services on our website, the details you are asked to submit will be provided directly to our payment provider via a secured connection.</span>
</p>
<p><span style="font-weight: 400;">The cardholder must retain a copy of transaction records and Merchant policies and rules.</span>
</p>
<p><span style="font-weight: 400;">We accept payments online using Visa and MasterCard credit/debit card in AED (or any other agreed currency).</span>
</p>
</div>
</body>
</html>
