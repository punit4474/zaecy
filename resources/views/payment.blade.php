<html>
<head>
    <script src="https://rakbankpay.gateway.mastercard.com/checkout/version/61/checkout.js"
            data-error="{{URL::to('payment-error?payment_token='.$oid)}}"
            data-cancel="{{URL::to('payment-cancel?payment_token='.$oid)}}"
            data-complete="{{URL::to('payment-callback?payment_token='.$oid.'&sessionNo='.$session_id)}}"></script>

    <script type="text/javascript">
        function errorCallback(error) {
            console.log(JSON.stringify(error));
        }

        function cancelCallback() {
            alert('Payment cancelled Done');
            console.log('Payment cancelled');
        }

        function completeCallback(resultIndicator, sessionVersion) {
            //handle payment completion
            console.log(resultIndicator);
            console.log(sessionVersion);
        }

        Checkout.configure({
            merchant: 'TESTZAECYFZLLCR',
            order: {
                amount: {{$orderData['total_amount']}},
                currency: 'AED',
                description: 'Ordered goods',
                id: '{{$orderData['order_id']}}'
            },
            session: {
                id: '{{$session_id}}'
            },
            billing: {
                address: {
                    street: '{{$orderData['address1']}}',
                    city: '{{$orderData['city']}}',
                    postcodeZip: '{{$orderData['postal_code']}}',
                    stateProvince: 'UAE',
                    country: 'ARE'
                }
            },
            interaction: {
                operation: 'PURCHASE',
                merchant: {
                    name: 'Zaecy Store',
                    address: {
                        line1: '200 Sample St',
                        line2: '1234 Example Town'
                    }
                }
            }
        });
        Checkout.showLightbox();
    </script>
</head>
<body>
<!--<input type="button" value="Pay with Lightbox" onclick="Checkout.showLightbox();"/>-->
<!--<input type="button" value="Pay with Payment Page" onclick="Checkout.showPaymentPage();"/>-->
</body>
</html>
