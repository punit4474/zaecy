<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'Api', 'prefix' => 'v1'], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('register', 'Auth\AuthController@register');
    Route::post('verify-otp', 'Auth\AuthController@verifyOTP');
    Route::post('forgot-password', 'Auth\AuthController@forgotPassword');
    Route::post('forgot-password-email', 'Auth\AuthController@forgotPasswordEmail');
    Route::post('set-password', 'Auth\AuthController@setNewPassword');

    Route::group(['middleware' => 'ApiAuth'], function () {
        Route::post('change-password', 'Auth\AuthController@changePassword');
        Route::get('logout', 'Auth\AuthController@logout');

        Route::get('get-user-profile', 'User\UserController@index');
        Route::post('update-user-profile', 'User\UserController@editProfile');
        Route::get('get-user-address', 'User\UserController@getAllAddress');
        Route::post('add-user-address', 'User\UserController@addAddress');
        Route::post('edit-user-address', 'User\UserController@editAddress');
        Route::post('update-user-address', 'User\UserController@updateAddress');
        Route::post('remove-user-address', 'User\UserController@removeAddress');
        Route::post('make-default-address', 'User\UserController@defaultAddress');

        Route::post('add-wishlist', 'Product\ProductController@addWishlist');
        Route::post('remove-wishlist', 'Product\ProductController@removeWishlist');
        Route::post('get-wishlist', 'Product\ProductController@myWishlist');
        Route::post('add-cart', 'Cart\CartController@addToCart');
        Route::post('update-cart', 'Cart\CartController@updateCart');
        Route::post('remove-cart', 'Cart\CartController@removeCart');
        Route::post('cart-data', 'Cart\CartController@getCartProduct');
        Route::post('checkout', 'Order\OrderController@index');
        Route::get('get-orders', 'Order\OrderController@orderList');
        Route::post('order-details', 'Order\OrderController@orderDetails');
        Route::post('cancel-order', 'Order\OrderController@cancelOrder');

    });

    Route::post('get-slider', 'User\UserController@getSlider');

    Route::post('get-collection', 'Category\CategoryController@getCollection');
    Route::post('get-category', 'Category\CategoryController@getCategory');
    Route::post('get-subcategory', 'Category\CategoryController@getSubcategory');
    Route::post('get-all-collection', 'Category\CategoryController@getAllCollection');
    Route::post('get-all-header-collection', 'Category\CategoryController@getHeaderCollection');
    Route::post('get-subcategory-collection', 'Category\CategoryController@getSubcategoryCollection');

    Route::post('subscribe-newletter', 'Category\CategoryController@newsLetter');
    Route::post('latest-product', 'Product\ProductController@index');
    Route::post('popular-product', 'Product\ProductController@popularProduct');
    Route::post('cateogrywise-product', 'Product\ProductController@categoryProduct');
    Route::post('subcategorywise-product', 'Product\ProductController@subcategoryProduct');
    Route::post('collectionwise-product', 'Product\ProductController@collectionProduct');
    Route::post('get-page-category', 'Product\ProductController@getPageCategory');
    Route::post('get-product-page-category', 'Product\ProductController@getProductPageCategory');
    Route::post('get-product-details', 'Product\ProductController@productView');
    Route::post('search-product', 'Product\ProductController@searchProduct');
    Route::post('get-colours','Product\ProductController@getColours');
    Route::post('product-sorting','Product\ProductController@sorting');
    Route::post('product-filter','Product\ProductController@filter');
    Route::post('coupon-validation','Cart\CartController@couponValidation');

});

/**
 * UnUsed Routes
 */
Route::group(['namespace' => 'Api\Admin', 'prefix' => 'v1/admin/'], function () {
    Route::post('login', 'Auth\AuthController@login');

    Route::group(['middleware' => 'ApiAuth'], function () {
        Route::post('dashboard', 'DashboardController@index');

        Route::post('get-collection', 'Collection\CollectionController@index');
        Route::post('create-collection', 'Collection\CollectionController@create');
        Route::post('edit-collection', 'Collection\CollectionController@edit');
        Route::post('update-collection', 'Collection\CollectionController@update');
        Route::post('remove-collection', 'Collection\CollectionController@destroy');

        Route::post('get-category', 'Category\CategoryController@index');
        Route::post('create-category', 'Category\CategoryController@create');
        Route::post('edit-category', 'Category\CategoryController@edit');
        Route::post('update-category', 'Category\CategoryController@update');
        Route::post('remove-category', 'Category\CategoryController@destroy');

        Route::post('get-sub-category', 'Subcategory\SubCategoryController@index');
        Route::post('create-sub-category', 'Subcategory\SubCategoryController@create');
        Route::post('edit-sub-category', 'Subcategory\SubCategoryController@edit');
        Route::post('update-sub-category', 'Subcategory\SubCategoryController@update');
        Route::post('remove-sub-category', 'Subcategory\SubCategoryController@destroy');


        Route::post('get-colours', 'Colours\ColoursController@index');
        Route::post('create-colours', 'Colours\ColoursController@create');
        Route::post('edit-colours', 'Colours\ColoursController@edit');
        Route::post('update-colours', 'Colours\ColoursController@update');
        Route::post('remove-colours', 'Colours\ColoursController@destroy');

        Route::post('get-products', 'Product\ProductController@index');
        Route::post('get-collection-data', 'Product\ProductController@getCollection');
        Route::post('get-category-data', 'Product\ProductController@getCategory');
        Route::post('get-subcategory-data', 'Product\ProductController@getSubcategory');
        Route::post('create-products', 'Product\ProductController@create');
        Route::post('edit-products', 'Product\ProductController@edit');
        Route::post('update-products', 'Product\ProductController@update');
        Route::post('remove-products', 'Product\ProductController@destroy');

    });

});

Route::group(['namespace' =>'Api\Delivery','prefix'=>'v1/delievry-partner'],function (){
    Route::get('product-sync','ProductController@index');
    Route::get('order-sync','OrderController@index');
    Route::post('awb-number-tracking','OrderController@awbTracking');
    Route::get('cancel-order','OrderController@cancelOrder');
    Route::get('return-order','OrderController@returnOrder');
    Route::post('stock-update','OrderController@stockUpdate');
});
