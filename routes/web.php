<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/faq',function (){
    return view('faq');
});

Route::get('/about-us',function (){
    return view('about');
});

Route::get('/privacy-policy',function (){
    return view('privacy');
});

Route::get('/return-policy',function (){
    return view('return');
});

Route::get('/shipping-policy',function (){
    return view('shipping');
});
Route::get('/terms-and-conditions',function (){
    return view('terms');
});
Route::get('/user-terms',function (){
    return view('use');
});

Route::get('payment-callback','PaymentController@index');
Route::get('payment-error','PaymentController@error');
Route::get('payment-cancel','PaymentController@cancel');
Route::get('payment-procced','PaymentController@paymentProcced');

Route::group(['prefix' => 'zaecy-admin', 'namespace' => 'Admin'], function () {

    Route::get('login', function () {

        if (Auth::check() && Auth::user()->hasRole('admin')) {
            return redirect('zaecy-admin/');
        } else {
            return view('Admin.Auth.login');
        }

    });

    Route::post('login', 'AuthController@doLogin');

    Route::group(['middleware' => 'AdminAuth'], function () {
        Route::get('/', 'DashboardController@index');
        Route::get('logout', 'AuthController@logout');

        Route::resource('collections', 'CollectionController');
        Route::get('collections/{id}/destroy', 'CollectionController@destroy');
        Route::get('collections/{id}/status', 'CollectionController@changeStatus');

        Route::resource('category', 'CategoryController');
        Route::get('category/{id}/destroy', 'CategoryController@destroy');
        Route::get('category/{id}/status', 'CategoryController@changeStatus');

        Route::resource('subcategory', 'SubCategoryController');
        Route::get('subcategory/{id}/destroy', 'SubCategoryController@destroy');
        Route::get('subcategory/{id}/status', 'SubCategoryController@changeStatus');
        Route::post('subcategory/get-category', 'SubCategoryController@getCategory');

        Route::resource('colours', 'ColourController');
        Route::get('colours/{id}/destroy', 'ColourController@destroy');
        Route::get('colours/{id}/status', 'ColourController@changeStatus');

        Route::resource('fits', 'FitController');
        Route::get('fits/{id}/destroy', 'FitController@destroy');
        Route::get('fits/{id}/status', 'FitController@changeStatus');

        Route::resource('style', 'StyleController');
        Route::get('style/{id}/destroy', 'StyleController@destroy');
        Route::get('style/{id}/status', 'StyleController@changeStatus');

        Route::resource('nack-style', 'NackStyleController');
        Route::get('nack-style/{id}/destroy', 'NackStyleController@destroy');
        Route::get('nack-style/{id}/status', 'NackStyleController@changeStatus');

        Route::resource('sleeve-type', 'SleeveTypeController');
        Route::get('sleeve-type/{id}/destroy', 'SleeveTypeController@destroy');
        Route::get('sleeve-type/{id}/status', 'SleeveTypeController@changeStatus');

        Route::resource('products', 'ProductController');
        Route::get('products/{id}/destroy', 'ProductController@destroy');
        Route::get('products/{id}/status', 'ProductController@changeStatus');
        Route::post('products/get-collection', 'ProductController@getCollection');
        Route::post('products/get-category', 'ProductController@getCategory');
        Route::post('products/get-subcategory', 'ProductController@getSubcategory');
        Route::post('products/get-fit', 'ProductController@getFit');
        Route::post('products/get-style', 'ProductController@getStyle');
        Route::post('products/get-nackstyle', 'ProductController@getNackStyle');
        Route::post('products/get-sleevestyle', 'ProductController@getSleeveStyle');
        Route::post('products/get-product', 'ProductController@getProduct');
        Route::post('products/remove-product', 'ProductController@removeProduct');
        Route::post('products/removeproducts', 'ProductController@removeMultipleProducts');
        Route::get('product/import','ProductController@import');
        Route::post('product/import','ProductController@importData');
        Route::get('product/{id}/remove-image/{image_id}','ProductController@removeProductImage');

        Route::resource('slider', 'SliderController');
        Route::get('slider/{id}/destroy', 'SliderController@destroy');
        Route::get('slider/{id}/status', 'SliderController@changeStatus');

        Route::resource('discount', 'DiscountController');
        Route::get('discount/{id}/destroy', 'DiscountController@destroy');

        Route::resource('customers', 'CustomerController');
        Route::get('customers/{id}/destroy', 'CustomerController@destroy');
        Route::get('customers/{id}/status', 'CustomerController@statusChange');
        Route::post('customers/filter', 'CustomerController@filterData');

        Route::resource('user', 'UserController');
        Route::get('user/{id}/destroy', 'UserController@destroy');
        Route::get('user/{id}/status', 'UserController@statusChange');

        Route::resource('page-category', 'PageCategoryController');
        Route::get('page-category/{id}/destroy', 'PageCategoryController@destroy');
        Route::get('page-category/{id}/status', 'PageCategoryController@changeStatus');

        Route::resource('orders', 'OrderController');
        Route::get('orders/{id}/destroy', 'OrderController@destroy');
        Route::get('orders/{id}/status', 'OrderController@changeStatus');

        Route::resource('analytics', 'AnalyticsController');

    });
});
